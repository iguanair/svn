#!/usr/bin/python -tt
from __future__ import with_statement
import warnings
import traceback
import sys
import os
import socket
import time
import lircInterface
import decoders
import struct
import threading

import iguanaIR

from support import *
from binner import computeBins_mixed as computeBins

def printUsage(msg = None):
    usage = "Usage: " + sys.argv[0] + " [OPTION]..." + """

-c
--config : Specify a lirc configuration file to define remotes.

-h
--help : Print this usage message.

-l
--log-file : Specify a log to receive all messages.

-1
--one : Stop after finding one code.

-q
--quiet : Decrease verbosity.

-r
--raw : Show the raw signals as they come in.

-v
--verbose : Increase verbosity.
"""

    if msg != None:
        message(LOG_FATAL, msg + usage)
    message(LOG_ALWAYS, usage)
    dieCleanly(LOG_ALWAYS)

def checkCodeSent(code):
    if len(code['raw']) > 2:
        code['name'] = 'received'
        (code['binned'], code['error']) = computeBins(code['raw'])
        if decoders.decode(code):
            return code['value']
    return False

def sendSignal(remotes, remote, button):
    remote = remotes[remote]
    bits = (remote['pre_data'] << remote['bits']) + remote['codes'][button]
    bitCount = remote['bits'] + remote['pre_data_bits']

    encoder = decoders.findEncoder(remote['flags'])
    if encoder is None:
        message(LOG_FATAL, "Failed to find an encoder for %s %s\n",
                remote['name'], button)

    signals = encoder.encode(bitCount, bits, remote)
    data = lircInterface.packSignalsForSend(signals)

    # checking the signal
    sentBits = checkCodeSent({ 'raw' : struct.unpack('I' * (len(data) / 4),
                                                     data) })
    if bits != sentBits:
        message(LOG_FATAL, "The generated code could not be recognized (%x != %x)\n" % (bits, sentBits))

    message(LOG_INFO, "Transmitting the signal.\n")
    conn = iguanaIR.connect('0')
    req = iguanaIR.createRequest(iguanaIR.IG_DEV_SEND, data)
    iguanaIR.writeRequest(req, conn)

    print remote, button

def deviceTransaction(type, data = ''):
    retval = False
    req = iguanaIR.createRequest(type, data)
    if not iguanaIR.writeRequest(req, conn):
        print 'Failed to write packet.'
    else:
        resp = iguanaIR.readResponse(conn, 10000)
        if iguanaIR.responseIsError(resp):
            print 'Error response.'
        else:
            retval = True
    return retval

def collectSignals(state):
    done = False

    while not state['done']:
        resp = iguanaIR.readResponse(conn, 100)
        if resp is None:
            continue
        elif iguanaIR.responseIsError(resp):
            print 'Error response.'
        else:
            more = iguanaIR.removeData(resp)
            more = struct.unpack('I' * (len(more) / 4), more)

            # lock the list while we add things too it
            with state['lock']:
                # pop "next" from the existing list
                try:
                    next = int(state['data'].pop(-1))
                except IndexError:
                    next = iguanaIR.IG_PULSE_MASK

                for pulse in more:
                    # if we changed states output the old value
                    if pulse & iguanaIR.IG_PULSE_MASK < 200:
                        message(LOG_WARN, "Transitions appear too short....\n")
                    elif pulse & iguanaIR.IG_PULSE_BIT & next == 0:
                        state['data'].append(next)
                        state['dataReady'].notify()
                        state['stats']['saw'] += 1
                        next = 0

                    if next == 0:
                        next = pulse
                    # cut off when a space exceeds the maxPause
                    elif next & ~iguanaIR.IG_PULSE_BIT <= 1000000:
                        next += pulse & iguanaIR.IG_PULSE_MASK

                # put next back onto the end of the list
                state['data'].append(next)

                # discard data older than 1 second
                totalTime = 0
                for x in range(len(state['data']) - 1, -1, -1):
                    totalTime += state['data'][x] & iguanaIR.IG_PULSE_MASK
                    if totalTime > 1000000:
                        state['stats']['junk'] += x
                        del state['data'][:x]
                        break

def checkCode(code, mapping):
    if len(code['raw']) > 2:
        code['name'] = 'received'
        (code['binned'], code['error']) = computeBins(code['raw'])
        if decoders.decode(code):
            if mapping:
                if code['value'] in mapping:
                    (remote, button) = mapping[code['value']]
                    lircInterface.sendToClients(code, remote, button)
                    state['stats']['used'] += len(code['raw']) + 1
            else:
                decoders.printCode(code)
            # returning true for any decoded code
            return True
        else:
            message(LOG_INFO, 'unrecognized code: %s\n' % code['binned'])
    return False

def handleTransmissions(state):
    with state['lock']:
        while not state['done']:
            # use a wait condition (paired with the lock)
            # to watch for incoming data
            state['dataReady'].wait()

            # initially there's no code data and start with a pulse
            code = { 'raw' : [] }
            longestPulse = 0
            isSpace = True
            lastDecoded = 0

            # work through the content of the raw signals
            for x in range(len(state['data'])):
                signal = state['data'][x]
                isSpace = (signal & iguanaIR.IG_PULSE_BIT) == 0
                signal &= iguanaIR.IG_PULSE_MASK

                if isSpace:
                    # add all short spaces to the signal
                    if signal <= longestPulse:
                        code['raw'].append(signal)
                    # detect gaps
                    elif longestPulse > 0:
                        # for raw data just print it to the terminal
                        if state['raw']:
                            for i in range(len(code['raw'])):
                                if i % 2 == 0:
                                    message(LOG_NORMAL, "pulse ")
                                else:
                                    message(LOG_NORMAL, "space ")
                                message(LOG_NORMAL, "%d\n" % code['raw'][i])
                            code = { 'raw' : [] }
                        # attempt to decode the signals we collected
                        elif checkCode(code, state['mapping']):
                            lastDecoded = x
                            code = { 'raw' : [] }

                        # stop after one signal if that was requested
                        if not code['raw']:
                            if state['onlyOne']:
                                state['done'] = True
                            elif state['raw']:
                                # show the user the detected gap
                                message(LOG_NORMAL, "----GAP----\n")
                                longestPulse = 0
                else:
                    # append all pulses and record the longest
                    code['raw'].append(signal)
                    if signal > longestPulse:
                        longestPulse = signal
                isSpace = not isSpace

            # throw away all signals before a recognized one
            state['data'] = state['data'][lastDecoded:]

if __name__ == "__main__":
    device = '0'
    remotes = {}
    printRaw = False
    onlyOne = False

    index = 1
    while index < len(sys.argv):
        arg = sys.argv[index]
        if arg == "-c" or arg == "--config":
            index += 1
            remotes.update(lircInterface.parseConfiguration(sys.argv[index]))
        elif arg == "-h" or arg == "--help":
            printUsage()
        elif arg == "-l" or arg == "--log-file":
            index += 1
            logFile = sys.argv[index]
            if logFile == "-":
                logFile = None
        elif arg == "-1" or arg == "--one":
            onlyOne = True
        elif arg == "-q" or arg == "--quiet":
            if currentLevel > LOG_FATAL:
                changeLevel(-1)
        elif arg == "-r" or arg == '--raw':
            printRaw = True
        elif arg == "-v" or arg == "--verbose":
            changeLevel(1)
        else:
            printUsage("Unknown argument: " + arg + "\n")
        index += 1

    # open the log file if specified
    if logFile != None:
        sys.log = open(logFile, "a", 1)
        logFile = "-"

    codeMapping = None
    if remotes:
        # construct a code mapping based on the remotes defined in lirc
        # configuration files.
        codeMapping = {}
        for remote in remotes:
            remote = remotes[remote]
            for code in remote['codes']:
                value = remote['codes'][code]
                if 'pre_data' in remote:
                    value += remote['pre_data'] << remote['bits']
                codeMapping[value] = (remote, code)
        # listen for incoming client connections
        lircInterface.listenForClients(lambda x,y: sendSignal(remotes, x, y))

    # connect to device 0
    conn = iguanaIR.connect(device)
    # turn the receiver on
    deviceTransaction(iguanaIR.IG_DEV_RECVON)

    # create the state shared between threads
    state = {
        'lock'  : threading.Lock(),
        'done'  : False,
        'data'  : [],
        'stats' : {
            'saw'  : 0,
            'used' : 0,
            'junk' : 0
        },
        'mapping' : codeMapping,
        'onlyOne' : onlyOne,
        'raw' : printRaw
    }
    state['dataReady'] = threading.Condition(state['lock'])

    # start a decoder thread that will be signaled as codes come in
    decoder = threading.Thread(target = handleTransmissions,
                               kwargs = { 'state' : state })
    decoder.start()

    # try and decipher any signals as they come in (ctrl-c breaks out)
    try:
        collectSignals(state)
    except KeyboardInterrupt:
        pass
    finally:
        lircInterface.stopListening()

        state['done'] = True
        with state['lock']:
            state['dataReady'].notify()
        decoder.join()

    # print a couple stats
    message(LOG_INFO, ('unused signal count: %s' % (len(state['data']) - 1)) + ' = %(saw)s - %(used)s - %(junk)s\n' % state['stats'])
