import math

def histogram(values):
    hist = {}
    for value in values:
        if value in hist:
            hist[value] += 1
        else:
            hist[value] = 1
    return hist

def weightedAverage(hist, start, end):
    total = 0.0
    count = 0

    for pos in hist:
        if pos > start and pos < end:
            total += hist[pos] * pos
            count += hist[pos]

    if total == 0:
        return None
    return total / count

def findPeaks(hist):
    peaks = []

    # find the start and stop of each range
    starts = []
    stops = []
    for key in hist:
        # fake weighting by adding an appropriate number
        for x in range(hist[key]):
            starts.append(key / 1.1)
            stops.append(key / 0.9)

    # we'll need to go through them in order
    starts.sort()
    stops.sort()

    count = 0
    prevCount = 0
    prevValue = 0
    start = 0
    total = 0
    while len(stops) > 0:
        if len(starts) > 0 and \
           starts[0] < stops[0]:
            count += 1
            value = starts[0]
            starts[0:1] = []
        else:
            count -= 1
            value = stops[0]
            stops[0:1] = []

        if prevCount > 1 and count <= 1:
            peak = weightedAverage(hist, start, value)
#            print 'range: %s - %s => %s' % (start, value, peak)
            if peak is not None:
                peaks.append(peak)

        if prevCount <= 1 and count > 1:
            start = value
        prevCount = count
        prevValue = value

    # there is a chance that we'l find no peaks, so return the avg
    if not peaks:
        total = 0
        for key in hist:
            total += key
        peaks = [ total / len(hist) ]

    return peaks

def makePeakMapping(codes):
    hist = histogram(codes)
    peaks = findPeaks(hist)

    # map each peak to a small integer multiple of the smallest signal
    peaks.sort()
    peakMapping = {}
    for x in range(len(peaks)):
        peakMapping[peaks[x]] = int(round(peaks[x] / peaks[0]))

    return peakMapping

def mapValue(mapping, value):
    closest = None
    distance = 1000000
    for point in mapping:
        if closest is None or \
           abs(point - value) < distance:
            distance = abs(point - value)
            closest = point
    return (closest, distance)

def divideSignals(codes):
    divided = [[], []]

    for x in range(len(codes)):
        divided[x % 2].append(codes[x])

    return divided

def mapSignals(codes):
    peakMapping = makePeakMapping(codes)

    # create a binned representation of the signal
    binned = []
    sumOfSquares = 0
    for signal in codes:
        (closest, distance) = mapValue(peakMapping, signal)
        sumOfSquares += closest * closest
        binned.append(peakMapping[closest])

    return (binned, math.sqrt(sumOfSquares) / len(codes))

def combineSignals(pulses, spaces):
    codes = []
    while spaces:
        codes.append(pulses[0])
        pulses[0:1] = []
        codes.append(spaces[0])
        spaces[0:1] = []
    codes.append(pulses[0])

    return codes

def makeMappings(signals):
    mappings = []
    for x in range(len(signals)):
        mappings.append(makePeakMapping(signals[x]))
    return mappings

def computeBins_combined(codes):
    return mapSignals(codes)

def computeBins_separate(codes):
    divided = divideSignals(codes)
    total = 0
    for x in range(len(divided)):
        (divided[x], error) = mapSignals(divided[x])
        total += error
    return (combineSignals(*divided), total / len(divided))

def computeBins_mixed(codes):
    divided = divideSignals(codes)
    divided.append(codes)
    return computeBins_fromResults(divided, codes)


def computeBins_fromResults(results, codes):
    mappings = makeMappings(results)
    return computeBins_fromMappings(mappings, codes)

def computeBins_fromMappings(mappings, codes):
    # create a binned representation of the signal
    binned = []
    sumOfSquares = 0
    for x in range(len(codes)):
        value = 0
        shortest = None

        for y in range(3):
            (closest, distance) = mapValue(mappings[y], codes[x])

            # prefer the bins from our own kind over the other type
            # and over the combined
            if y == 2:
                distance *= 6
            elif x % 2 != y % 2:
                distance *= 3

            if shortest is None or distance < shortest:
                value = mappings[y][closest]
                shortest = distance

        if shortest is not None:
            sumOfSquares += shortest * shortest
            binned.append(value)
        else:
            raise 'DEATH!!!'

    return (binned, math.sqrt(sumOfSquares) / len(codes))


#computeBins = computeBins_combined
#computeBins = computeBins_separate
computeBins = computeBins_mixed
