from support import *

def checkForHeader(headers, code):
    for header in headers:
        for x in range(len(header)):
            if x >= len(code) or header[x] != code[x]:
                break
        else:
            return header
    return None

# decode signals that start with the gap
class decoder:
    name = 'DecoderBaseClass'
    header = None

    def decodeReset(self):
        pass

    def decodeStep(self, pos):
        return 'decodeStep no implemented.'

    def decodeDone(self, remainder = None):
        return None

    def __decode__(self, header, code):
        # prepare to decode a signal
        self.retval = ''
        self.code = code
        self.decodeReset()

        for self.pos in range(len(header), len(self.code)):
            msg = self.decodeStep(self.pos)
            if msg is not None:
                break
        return msg

    def decode(self, code):
        msg = None
        self.pos = 0 # TODO: make this report the correct position

        # check for the header, then body
        header = checkForHeader(self.headers, code)
        if header is None:
            msg = 'Bad header.'
        else:
            msg = self.__decode__(header, code)

        # if no errors so far call done
        if msg is None or msg == 'Done?':
            msg = self.decodeDone(code[self.pos:])

        if 'retval' in dir(self) and \
           '1' * len(self.retval) == self.retval:
            msg = 'no zeros found in RC5 signal'

        if msg is not None:
            self.error = 'decode(%s): %s (%d at %d)\n' % (self.name, msg,
                                                          code[self.pos],
                                                          self.pos)
            return None

        return self.retval

    def encode(self, bits, data):
        return None

class Group:
  def __init__(self, l, size):
    self.size = size
    self.l = l

  def __getitem__(self, group):
    idx = group * self.size
    if idx >= len(self.l): 
      raise IndexError("Out of range")
    return self.l[idx:idx + self.size]

class RC5(decoder):
    """
Decodes signals such as the following:

RC5:
1  1 1  1 1  1 1  1 1  1 1  1 1  1 1  1 1  1 1  1 1  1 1  1 1  1 1
p  s p  p s  p s  p s  s p  p s  s p  p s  p s  s p  s p  p s  p s
    1    0    0    0    1    0    1    0    0    1    1    0    0

2 2 2 1 1 1 1 1 1 1 1 1 1 2 2 1 1 2 2
p  p s  s p  p s  p s  p s  p s  p s  p s  s p  p s  p s  s p  p
    0    1    0    0    0    0    0    0    1    0    0    1

"""
    name = 'RC5'
    headers = [[]]
    leader = ''
    mapping = { 'sp' : '1',
                'ps' : '0' }

    def __decode__(self, header, code):
        msg = None
        text = self.leader
        for self.pos in range(len(header), len(code)):
            if code[self.pos] != 1 and code[self.pos] != 2:
                msg =  'Not in range !(1 <= %d <= 2).' % code[self.pos]
                break
            else:
                text += 'ps'[self.pos % 2] * code[self.pos]
        else:
            self.retval = ''
            count = 0
            for pair in Group(text[1:],2):
                if pair == 'p':
                    pass
                elif pair == 's':
                    msg = 'Illegal space at %s in %s' % (count * 2 + 1, text)
                    break
                elif pair[0] == pair[1]:
                    msg = 'Illegal back-to-back %s at %s in %s' % (pair, count * 2 + 1, text)
                    break
                else:
                    self.retval += self.mapping[pair]

                count += 1
        return msg

class RC6(RC5):
    """
Decodes signals such as the following:
2 2 1 1 1 1 1 2 1 2 2 
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 1 1 1 1 1 1 2 2 2
2

Breaks into 3 parts:
pp  ss  ps  ps  ps  sp  ss  pp
sp  sp  sp  sp  sp  sp  sp  sp  sp  sp  sp  sp  ps  ps  ps  ps  sp  ps  sp
p

Very much derived from RC5
"""
    name = 'RC6'
    headers = [[1, 2, 1, 1, 1, 1, 1, 2, 1, 2, 1],
               [2, 2, 1, 1, 1, 1, 1, 2, 1, 2, 2]]
    leader = 'p'
    mapping = { 'sp' : '0',
                'ps' : '1' }

class SpaceEncoded(decoder):
    name = 'SPACE_ENC'
    headers = [
        [17, 8, 1],
        [17, 4, 1],
        [14, 4, 1],
        [14, 3, 1],
        [13, 4, 1],
        [ 8, 8, 1],
        [ 8, 4, 1],
        [ 7, 5, 1],
        [ 5, 2, 1],
        [ 4, 4, 1],
        [ 1, 3, 1],
        [ 1, 4, 1],
        [ 1, 5, 1],
    ]

    def decodeStep(self, pos):
        if pos % 2 == 1:
            if self.code[pos] == 1:
                self.retval += '0'
            # NOTE: >= 2 was == 3
            elif self.code[pos] >= 2:
                self.retval += '1'
            else:
                return 'Bad space length.'
        elif self.code[pos] != 1:
            return 'Done?'
        return None

    def decodeDone(self, remainder = None):
        if remainder == [1] or \
           checkForHeader([self.code[:3]], remainder):
            return None
        return 'Bad pulse length.'

    def encode(self, bits, data, values):
        signals = values['header'][:]
        for pos in range(bits):
            if (data >> (bits - pos - 1)) & 0x1:
                signals.extend(values['one'])
            else:
                signals.extend(values['zero'])
        signals.append(values['zero'][0])
        return signals

def decode(code):
#[1, 3, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 3, 1, 3, 1, 3, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 1, 3, 1, 3, 1, 3, 1, 3, 1, 1, 1, 3, 1]
#print code['binned']
    for decoder in ( RC5(), RC6(), SpaceEncoded() ):
        result = decoder.decode(code['binned'])
        if not result:
            message(LOG_DEBUG, '%s: %s' % (code['name'], decoder.error))
        elif 'decoder' in code:
            message(LOG_WARN,
                    "Multiple decoders (%s, %s) work for '%s'\n" % (code['decoder'].name, decoder.name, code['name']))
        else:
            code['decoder'] = decoder
            code['value'] = int(result, 2)
            printCode(code, LOG_INFO)
    return 'decoder' in code

def printCode(code, level = LOG_NORMAL):
    message(level,
            '%s: 0x%X (%s w/ error: %0.2f)\n' % (code['name'],
                                                 code['value'],
                                                 code['decoder'].name,
                                                 code['error']))

def findEncoder(name):
    for encoder in ( SpaceEncoded(), ):
        if encoder.name == name:
            return encoder
    return None

"""
List of things supported by WinLirc.

# RC5
The remote uses the RC5 protocol.

# RC6
The remote uses the RC6 protocol.

# RCMM
The remote uses the RC-MM protocol (transmitting not supported).

# SHIFT_ENC
Obsolete flag, now a synonym for RC5. The position of the pulse (before or after the space) determines whether the bit is a one or a zero.

# SPACE_ENC
A one and a zero can be distinguished by the length of the spaces.

# REVERSE
Reverses the bit order of the pre_data, the post_data and the codes (e.g., 0x123 becomes 0xC48). If this flag is present, the least significant bit is sent first.

# NO_HEAD_REP
The header is not sent when a signal (the button is held down) is repeated even though there is no special repeat code.

# NO_FOOT_REP
The foot is not sent when a signal is repeated (the button is held down) even though there is no special repeat code.

# CONST_LENGTH
The total signal length is always constant. The gap length now represents the length of the entire signal, and the actual gap at the end of the signal is adjusted accordingly.

# RAW_CODES
The codes are in raw format.

# REPEAT_HEADER
Send the header when the signal is repeated even though the remote has a special repeat code.

# SPECIAL_TRANSMITTER
Use the transmitter type specified for this remote. (Not supported by LIRC). 

"""
