from support import *
import decoders
import binner

computeBins = binner.computeBins_mixed
def doNotSplit():
    computeBins = binner.computeBins_combined

def completelySplit():
    computeBins = binner.computeBins_separate

def sortOnValue(a, b, offset):
    retval = cmp(len(a['raw']), len(b['raw']))
    if retval == 0 and offset < len(a['raw']):
        if abs(a['raw'][offset] - b['raw'][offset]) > 100:
            retval = cmp(a['raw'][offset], b['raw'][offset])
    return retval

def displayCodes(codes, encoding, skipKnown = True):
    y = -2
    count = 1
    while y <= 0 or count > 0:
        count = 0
        line = ''
        for code in codes:
            if 'decoder' in code:
                continue

            next = None

            if y == -2:
                format = '%%%ds ' % code['width']
                next = format % (code['name'])
            elif y == -1:
                next = '-' * (code['width'] + 1)
            else:
                if y < len(code[encoding]):
                    format = '%%%dd ' % code['width']
                    next = format % code[encoding][y]
                    count += 1
                else:
                    next = ' ' * (code['width'] + 1)

            if len(line + next) < maxWidth:
                line += next
            else:
                break
        y += 1
        if line:
            message(LOG_NORMAL, '%s\n' % line)

def decodeCode(decoders, code):
    for decoder in decoders:
        result = decoder.decode(code['binned'])
        if result is None:
            message(LOG_INFO, '%s: %s' % (code['name'], decoder.error))
        else:
            if 'decoder' in code:
                message(LOG_WARN,
                        "Multiple decoders work for %s\n" % code['name'])
            else:
                code['decoder'] = decoder
                code['value'] = result
                message(LOG_NORMAL, '%s (error: %0.2f) <= %s (%s)\n' % (result, code['error'], code['name'], decoder.name))

    return 'decoder' in code

def identifyCodes(codes, source):
    totalCount = 0
    totalCodes = 0

    # sort the codes by length (and values) to quickly separate remotes
    for x in range(20, -1, -1):
        codes.sort(lambda a, b: sortOnValue(a, b, x))

    decoderList = {
        decoders.RC5()          : [[], []],
        decoders.RC6()          : [[], []],
        decoders.SpaceEncoded() : [[], []]
    }
    for code in codes:
        (code['binned'], code['error']) = computeBins(code['raw'])
        if decodeCode(decoderList, code):
            if combineSuccesses:
                separate = binner.divideSignals(code['raw'])
                for x in range(len(separate)):
                    decoderList[code['decoder']][x].extend(separate[x])
        # random noise works... SAD ... not anymore
        elif tryNoise:
            # use some random noise to perturb the signal and try some more
            raw = code['raw']
            for y in range(0):
                code['raw'] = []
                for x in range(len(raw)):
                    # linear and geometric noise, or one, or the other
                    code['raw'].append(raw[x] + random.randint(-100, 100))
                    code['raw'].append(int(raw[x] * (1 + random.randint(-10, 10) / 100.0)))
                    code['raw'].append(int(raw[x] * (1 + random.randint(-10, 10) / 100.0)) + random.randint(-100, 100))
                (code['binned'],
                 code['error']) = computeBins(code['raw'])
                if decodeCode(decoderList, code):
                    break
            code['raw'] = raw

    # see which decoders were actually used and pre-compute their mappings
    usedDecoders = {}
    for decoder in decoderList:
        if len(decoderList[decoder][0]) > 0:
            decoderList[decoder].append([])
            for x in range(2):
                decoderList[decoder][-1].extend(decoderList[decoder][x])
            usedDecoders[decoder] = binner.makeMappings(decoderList[decoder])

    # create bins based on previously recognized codes
    lastDitch = [[],[],[]]
    for code in codes:
        if 'decoder' not in code:
            for decoder in usedDecoders:
                (binned, error) = (code['binned'], code['error'])
                (code['binned'], code['error']) = binner.computeBins_fromMappings(usedDecoders[decoder], code['raw'])

                # only keep the new binning if it is better than the old
                if not decodeCode([decoder], code) and \
                   error < code['error']:
                    (code['binned'], code['error']) = (binned, error)

        # combine the codes of all failures in a last-ditch effort
        if combineFailures:
            if 'decoder' not in code:
                separate = binner.divideSignals(code['raw'])
                for x in range(len(separate)):
                    lastDitch[x].extend(separate[x])
                    lastDitch[-1].extend(separate[x])

    if combineFailures:
        for code in codes:
            if 'decoder' not in code:
                (binned, error) = (code['binned'], code['error'])
                (code['binned'],
                 code['error']) = binner.computeBins_fromResults(lastDitch,
                                                                 code['raw'])

                # only keep the new binning if it is better than the old
                if not decodeCode(decoderList, code) and \
                   error < code['error'] and False:
                    (code['binned'], code['error']) = (binned, error)

    count = 0
    for code in codes:
        if 'decoder' not in code:
            message(LOG_NORMAL, "No decoder recognized %s\n" % code['name'])
        else:
            count += 1

    # print the unrecognized ones so the user can take a look
    displayCodes(codes, 'raw')
    displayCodes(codes, 'binned')

    print 'Identified %d / %d from %s\n' % (count, len(codes), source)
    totalCount += count
    totalCodes += len(codes)
    return (totalCount, totalCodes)
