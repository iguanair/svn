#!/usr/bin/python -tt
from __future__ import with_statement
import warnings
import traceback
import sys
import os
import struct

import iguanaIR

from binner import computeBins_mixed as computeBins
from support import *
import lircInterface
import decoders

def printUsage(msg = None):
    usage = "Usage: " + sys.argv[0] + " [OPTION]... config remote button" + """

-h
--help : Print this usage message.

-l
--log-file : Specify a log to receive all messages.

-q
--quiet : Decrease verbosity.

-v
--verbose : Increase verbosity.
"""

    if msg != None:
        message(LOG_FATAL, msg + usage)
    message(LOG_ALWAYS, usage)
    dieCleanly(LOG_ALWAYS)

def checkCode(code):
    if len(code['raw']) > 2:
        code['name'] = 'received'
        (code['binned'], code['error']) = computeBins(code['raw'])
        if decoders.decode(code):
            return code['value']
    return False

def sendSignal(remote, button):
    bits = (remote['pre_data'] << remote['bits']) + remote['codes'][button]
    bitCount = remote['bits'] + remote['pre_data_bits']

    encoder = decoders.findEncoder(remote['flags'].replace('|CONST_LENGTH',''))
    if encoder is None:
        message(LOG_FATAL, "Failed to find an encoder for %s %s\n" % (
                remote['name'], button))

    signals = encoder.encode(bitCount, bits, remote)
    data = lircInterface.packSignalsForSend(signals)

    # checking the signal
    sentBits = checkCode({ 'raw' : struct.unpack('I' * (len(data) / 4),
                                                 data) })
    if bits != sentBits:
        message(LOG_FATAL, "The generated code could not be recognized.\n")

    message(LOG_INFO, "Transmitting the signal.\n")
    conn = iguanaIR.connect('0')
    req = iguanaIR.createRequest(iguanaIR.IG_DEV_SEND, data)
    return iguanaIR.writeRequest(req, conn)

if __name__ == "__main__":
    configFile = None
    remoteName = None
    buttonName = None

    index = 1
    while index < len(sys.argv):
        arg = sys.argv[index]
        if arg == "-h" or arg == "--help":
            printUsage()
        elif arg == "-l" or arg == "--log-file":
            index += 1
            logFile = sys.argv[index]
            if logFile == "-":
                logFile = None
        elif arg == "-q" or arg == "--quiet":
            if currentLevel > LOG_FATAL:
                changeLevel(-1)
        elif arg == "-v" or arg == "--verbose":
            changeLevel(1)
        elif configFile is None:
            configFile = sys.argv[index]
        elif remoteName is None:
            remoteName = sys.argv[index]
        elif buttonName is None:
            buttonName = sys.argv[index]
        else:
            printUsage("Unknown argument: " + arg + "\n")
        index += 1

    # open the log file if specified
    if logFile != None:
        sys.log = open(logFile, "a", 1)
        logFile = "-"

    if buttonName is None:
        printUsage('config, remote, and button are required.\n')

    remotes = lircInterface.parseConfiguration(configFile)
    sendSignal(remotes[remoteName], buttonName)
