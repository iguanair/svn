import math

def std(array):

    average=float(sum(array))/float(len(array))
    dev=float(0)
    for i in array:
        dev+=(i-average)**2
    dev=math.sqrt(dev/len(array))
    return (average, dev)


def DiffArray(array):
    newarray = [0]
    for x in range(1,len(array)):
        newarray.append(array[x]-array[x-1])
            
    return newarray



def RMS(array):
    dev=float(0)
    for x in array:
        dev+=x**2
    dev=math.sqrt(dev/len(array))
    return dev
