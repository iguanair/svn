from PyQt4 import QtCore, QtGui

class RemoteDetailsDlg(QtGui.QDialog):
    def __init__(self, item):
        QtGui.QDialog.__init__(self)
        self.item = item

    def show(self):
        for name in self.item.remote:
            if name != 'codes':
                eval('self.ui.%sEdit.setText(str(self.item.remote["%s"]))' % (name, name))

        QtGui.QDialog.show(self)
