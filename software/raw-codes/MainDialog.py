from PyQt4 import QtCore, QtGui

import lircInterface

import RemoteTree

class MainDialog(QtGui.QDialog):
    def __init__(self):
        QtGui.QDialog.__init__(self)
        self.remotes = {}

    def loadConfig(self):
        fileName = QtGui.QFileDialog.getOpenFileName(self,
                                                     'Load configuration file',
                                                     '/etc/lircd.conf')
        if fileName:
            newRemotes = lircInterface.parseConfiguration(fileName)
            self.remotes.update(newRemotes)
            self.ui.codesWidget.parent = self
            self.populateTree()

    def saveConfig(self):
        fileName = QtGui.QFileDialog.getSaveFileName(self,
                                                     'Save configuration file',
                                                     'untitled.conf')
        if fileName:
            lircInterface.writeConfiguration(self.remotes, fileName)

    # construct and display tree structure from the loaded remotes
    def populateTree(self):
        tree = {}
        for name in self.remotes:
            remote = self.remotes[name]

            prefix = 0
            if 'pre_data' in remote:
                prefix = remote['pre_data'] << remote['pre_data_bits']

            tree[name] = []
            for code in remote['codes']:
                tree[name].append((code,
                                   '0x%x' % (prefix + remote['codes'][code])))
        keys = tree.keys()
        keys.sort()

        # record expanded branches before clearing and repopulating
        expanded = []
        for x in range(self.ui.codesWidget.topLevelItemCount()):
            item = self.ui.codesWidget.topLevelItem(x)
            if item.isExpanded():
                expanded.append(str(item.text(0)))

        self.ui.codesWidget.clear()
        for topLevel in keys:
            # add the remote entry and all it's codes
            top = RemoteTree.RemoteTreeItem(QtCore.QStringList(topLevel))
            self.ui.codesWidget.addTopLevelItem(top)
            for item in tree[topLevel]:
                item = RemoteTree.RemoteTreeItem(top, QtCore.QStringList(item))
                item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)

            # configuring the top level item only works after it's populated
            top.remote = self.remotes[topLevel]
            top.setFirstColumnSpanned(True)
            top.setExpanded(topLevel in expanded)
            top.setFlags(top.flags() | QtCore.Qt.ItemIsEditable)
        self.ui.codesWidget.resizeColumnToContents(0)

    def deleteItem(self, remoteName, codeName = None):
        if codeName:
            del self.remotes[remoteName]['codes'][codeName]
        else:
            del self.remotes[remoteName]
        self.populateTree()

    def setCodeValue(self, remoteName, codeName, codeValue):
        self.remotes[remoteName]['codes'][codeName] = codeValue

    def renameCode(self, remoteName, codeName, newCodeName):
        self.remotes[remoteName]['codes'][newCodeName] = \
            self.remotes[remoteName]['codes'][codeName]
        del self.remotes[remoteName]['codes'][codeName]
        
    def renameRemote(self, oldName, newName):
        self.remotes[newName] = self.remotes[oldName]
        del self.remotes[oldName]

    def mergeRemotes(self, oldName, newName):
        # TODO: do some actual work to merge them if they are compatible.
        print 'merge %s in %s' % (oldName, newName)
