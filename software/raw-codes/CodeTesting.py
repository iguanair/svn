#!/usr/bin/python -tt

import iguanaIR
import struct
#import threading
import subprocess
import time

# detect the version before we try to write it
def deviceTransaction(device,type, data = ''):
    retval = False
    req = iguanaIR.createRequest(type, data)
    if not iguanaIR.writeRequest(req, device):
        print 'Failed to write packet.'
    else:
        resp = iguanaIR.readResponse(device, 10000)
        if resp is None:
            print 'Repsonse is None (odd)'
        elif iguanaIR.responseIsError(resp):
            print 'Error response.'
        elif type == iguanaIR.IG_DEV_GETVERSION:
            data = iguanaIR.removeData(resp)
            retval = ord(data[0]) + (ord(data[1])*100)
        else:
      
            retval = iguanaIR.removeData(resp)
            #retval = True
    return retval


##make DEV_RECVON for not have raw codes read as I instead of B
##"I" * (len(data) / 4)
def checkReceiver(sender,receiver,sendtype="irsend", remote="TESTREMOTE", button="pause"):
    deviceTransaction(receiver,iguanaIR.IG_DEV_RECVON)

    irsend=subprocess.Popen(("irsend", "SEND_ONCE",remote,button))
    irsend.wait()
    if irsend.returncode != 0 :
        print "Error With IRSEND"
        return False
    

    count = 0
    type = -1
    current = 0
    record = False
    spaces = []
    pulses = []
    while True:
        try:
            packet = iguanaIR.readResponse(receiver, 1000)
            #print packet
        except KeyboardInterrupt:
            break
        data = iguanaIR.removeData(packet)
        templength = len(data)/4
        for signal in struct.unpack('I' * templength , data):            
            #type is a pulse or space
            typeprev = type
            type = signal & iguanaIR.IG_PULSE_BIT
#            print type
            #if type is not same as previous one
#            print "Signal is ", signal

            if typeprev != type:
#                print 'New Pulses / Space detected ', record, type
                
                if type == 0 and typeprev != -1:
#                    print "Received New Space... previous must be pulse... start recording"
                    
                    if record == True:
                        pulses.append(current)
                    record=True
                    current = signal & iguanaIR.IG_PULSE_MASK
#                    print "So the signal data is", signal
#                    print "Current from new space start is ",current

                elif (type == iguanaIR.IG_PULSE_BIT ):
#                    print "Received New Pulse" 
                    if record == True:
                        spaces.append(current)
#                    print "Got Pulse, Record Previous Space, with length ",current
#                    print "Got pulse, record space"
                    current = signal & iguanaIR.IG_PULSE_MASK
                    if current == 0:
                        print "what's going on, space"
#                else:
#                    print "Error, not a space, not a pulse"
#                current = 0

#            if signal == iguanaIR.IG_RAWSPACE_BIT:
#                current += 1024
            else:
#                print "Appending from ", current
                current += signal & iguanaIR.IG_PULSE_MASK
#                print "To ", current

        if (current > 1000000 ) & (record != False) :
           print 'Captured the signal'
           break

        if current > 10000000 :
           print "Timed out on signal... hmmm"
           break
#        print 'Current count is ', current
#        print 'Type is ', type    

    deviceTransaction(receiver,iguanaIR.IG_DEV_RECVOFF)
#    print 'Summary'
#    print len(pulses)
#    print len(spaces)

 #   print "Adsfasdf"
#    print pulses[0]
#    print spaces[0]

    return (pulses, spaces)

   
    return True


# connect to the device and check the version
sender = iguanaIR.connect('main')
receiver = iguanaIR.connect('tester')
version = deviceTransaction(sender,iguanaIR.IG_DEV_GETVERSION)
print 'Sending Device Version is', version
version = deviceTransaction(receiver,iguanaIR.IG_DEV_GETVERSION)
print 'Receiving Device Version is', version

time.sleep(2)
print "First Attempt"
(p1, s1 ) = checkReceiver(sender,receiver)
#time.sleep(2)
print "Second Attempt"
#time.sleep(1)
(p2, s2 ) = checkReceiver(sender,receiver)

#print "First Pulse is ", p1[0], p2[0]
#print "First Space is ", s1[0], s2[0]

print len(p1), len(p2), len(s1), len(s2)

if len(p1) == len(p2) & len(p1) == len(s1) & len(p1) == len(s2):
    print "Arrays are right length!"


    i=0
    for code in p1 :
        if abs(p1[i] - p2[i]) > 0:
            print "Pulse Error at line ", i ," ", p1[i], " vs ",p2[i], "   Diff: ",p1[i]-p2[i]

        if abs(s1[i] - s2[i]) > 0:
            print "Space Erorr at line ", i , " ", s1[i], " vs ",s2[i], "   Diff: ",s1[i]-s2[i]
        i=i+1


else:
    print "Arrays not right -- aborting"






