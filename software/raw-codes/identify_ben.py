from support import *
import decoders
import binner
import math
import BenFunction

computeBins = binner.computeBins_mixed
def doNotSplit():
    computeBins = binner.computeBins_combined

def completelySplit():
    computeBins = binner.computeBins_separate

def sortOnValue(a, b, offset):
    retval = cmp(len(a['raw']), len(b['raw']))
    if retval == 0 and offset < len(a['raw']):
        if abs(a['raw'][offset] - b['raw'][offset]) > 100:
            retval = cmp(a['raw'][offset], b['raw'][offset])
    return retval

def displayCodes(codes, encoding, skipKnown = True):
    y = -2
    count = 1
    while y <= 0 or count > 0:
        count = 0
        line = ''
        for code in codes:
            if 'decoder' in code:
                continue

            next = None

            if y == -2:
                format = '%%%ds ' % code['width']
                next = format % (code['name'])
            elif y == -1:
                next = '-' * (code['width'] + 1)
            else:
                if y < len(code[encoding]):
                    format = '%%%dd ' % code['width']
                    next = format % code[encoding][y]
                    count += 1
                else:
                    next = ' ' * (code['width'] + 1)

            if len(line + next) < maxWidth:
                line += next
            else:
                break
        y += 1
        if line:
            message(LOG_NORMAL, '%s\n' % line)

def decodeCode(decoders, code):
    for decoder in decoders:
        result = decoder.decode(code['binned'])
        if result is None:
            message(LOG_INFO, '%s: %s' % (code['name'], decoder.error))
        else:
            if 'decoder' in code:
                message(LOG_WARN,
                        "Multiple decoders work for %s\n" % code['name'])
            else:
                code['decoder'] = decoder
                code['value'] = result
                message(LOG_NORMAL, '%s (error: %0.2f) <= %s (%s)\n' % (result, code['error'], code['name'], decoder.name))

    return 'decoder' in code

def BenID(codes):
    print '## Ben\'s Attempt Starts Here ##'
    for remote in codes:
        print 'Remote %s' % (remote['name'])
        SpaceQuality=[]
        RC5Quality=[]
    # Divide array into pulse and space array since they have different statistics
        [pulsesorig, spacesorig] = binner.divideSignals(remote['raw'])
    #Drop first pulse since that is the "code starting" pulse and doesn't say what kind of encoding it is.

        pulses=pulsesorig[:]
        spaces=spacesorig[:]
        pulses.pop(0)
        spaces.pop(0)


#        print pulses
#        print spaces
        spaces.sort()
        pulses.sort()
#        print pulses
#        print spaces

        (ave, std) = BenFunction.std(pulses)
# Test 1: If space encoded, the standard deviation should be well less than one pulse
# at 38kHz, which is 26uS. I'll step threshhold of 10 as a start
        SpaceQuality.append(std)
        
        if std < 10:
#            print 'Test 1: Space Encoded (%s)' % (std)
            t1=1
        else:
#            print 'Test 1: Not Spaced Encoded (%s)' % (std)
            t1=2


# Test 2: If it is RC5, we should see only two pulse sizes --so 
# diff array should be [0 0 0 0 0 100 0 0  0 0 0 0 ]
# where 100 is the change in size of the pulses. 
# Let's find where that spit is and seperate the array there

#        print '###'
#        print pulses
        pulsedif=BenFunction.DiffArray(pulses)

        m = max(pulsedif)
        p = pulsedif.index(m)
        
        lows=pulses[:p]
        highs=pulses[p:]

        (laverage,lspread) = BenFunction.std(lows)
        (haverage,hspread) = BenFunction.std(highs)
#        print laverage, haverage
#        print lspread, hspread
#        print pulsesorig
        

#        RC5Quality.append(lspread)
#        RC5Quality.append(hspread)
        RC5Quality.append(2*BenFunction.RMS([lspread,hspread]))

        #print 7/(math.log(haverage/laverage))
#        print (haverage/laverage)
#        RC5Quality.append(7/(math.log(haverage/laverage)))
#        RC5Quality.append(100*10**(-((haverage/laverage)-1)**2))
        RC5Quality.append(100*(math.log(7/float(15)*haverage/laverage-.2))**4)
#7/(math.log(haverage/laverage)))

        #RC5Quality.append(((2*laverage-haverage)**2)/1000)




        pulsedif.sort(reverse=True)
#        print pulsedif
        d1=pulsedif[0] - pulsedif[1]
        d2=pulsedif[1] - pulsedif[2]
#        print d1, pulsedif[0]

        SpaceQuality.append(pulsedif[0]/math.sqrt(len(pulses)*2))
#        if ( d1 < 50 ) and ( pulsedif[0] < 75 ):
#            print 'Test 2: Space Encoded'
#            t2=1

#        elif ( d1 > 300 ) and ( d1 < 2000) and (d2 < 100 ):
 #           print 'Test 2: RC5 / RC6'
 #           t2=2
 #       else:
 #           print 'I don\'t know what it is'
   #         print pulsesorig
   #         print pulsedif
  #          t2=3


#        print 'Space Quality is: %s' % (SpaceQuality)
#        print 'RC5 Quality is: %s' % (RC5Quality)


        SpaceTotal=sum(SpaceQuality)
        RC5Total=sum(RC5Quality)
#        print SpaceTotal
#        print RC5Total


        if ( (RC5Total / SpaceTotal) >  5 ) and ( SpaceTotal < 50):
            print 'Everyone Agrees: Space Encoded (S/N is %s)' % (RC5Total/SpaceTotal)

        elif ( SpaceTotal + RC5Total > 5 ) and ( RC5Total < 50):
            print 'Everyone Agrees: RC5 or RC6 (S/N is %s)' % (SpaceTotal/RC5Total)
        else:
            if (SpaceTotal>RC5Total):
                print 'Guessing: RC5 with S/N of %s' % (SpaceTotal/RC5Total)
            else:
                print 'Guessing: Space Encoding with S/N of %s' % (RC5Total/SpaceTotal)
            print pulsesorig

#            print 'Test 1 is: %s' % (t1)
#            print 'Test 2 is: %s' % (t2)



#        print pulsedif
#        print '###'
#        print spaces    
        spacedif=BenFunction.DiffArray(spaces)
        spacedif.sort()
#        print spacedif
#        print '###'
        print ''
    print '## Ben\'s Attempt Stops Here ##'





def identifyCodes(codes, source):
    totalCount = 0
    totalCodes = 0




    # sort the codes by length (and values) to quickly separate remotes
    for x in range(20, -1, -1):
        codes.sort(lambda a, b: sortOnValue(a, b, x))

    decoderList = {
        decoders.RC5()          : [[], []],
        decoders.RC6()          : [[], []],
        decoders.SpaceEncoded() : [[], []]
    }
    for code in codes:
        (code['binned'], code['error']) = computeBins(code['raw'])
        if decodeCode(decoderList, code):
            if combineSuccesses:
                separate = binner.divideSignals(code['raw'])
                for x in range(len(separate)):
                    decoderList[code['decoder']][x].extend(separate[x])
        # random noise works... SAD ... not anymore
        elif tryNoise:
            # use some random noise to perturb the signal and try some more
            raw = code['raw']
            for y in range(0):
                code['raw'] = []
                for x in range(len(raw)):
                    # linear and geometric noise, or one, or the other
                    code['raw'].append(raw[x] + random.randint(-100, 100))
                    code['raw'].append(int(raw[x] * (1 + random.randint(-10, 10) / 100.0)))
                    code['raw'].append(int(raw[x] * (1 + random.randint(-10, 10) / 100.0)) + random.randint(-100, 100))
                (code['binned'],
                 code['error']) = computeBins(code['raw'])
                if decodeCode(decoderList, code):
                    break
            code['raw'] = raw

    # see which decoders were actually used and pre-compute their mappings
    usedDecoders = {}
    for decoder in decoderList:
        if len(decoderList[decoder][0]) > 0:
            decoderList[decoder].append([])
            for x in range(2):
                decoderList[decoder][-1].extend(decoderList[decoder][x])
            usedDecoders[decoder] = binner.makeMappings(decoderList[decoder])

    # create bins based on previously recognized codes
    lastDitch = [[],[],[]]
    for code in codes:
        if 'decoder' not in code:
            for decoder in usedDecoders:
                (binned, error) = (code['binned'], code['error'])
                (code['binned'], code['error']) = binner.computeBins_fromMappings(usedDecoders[decoder], code['raw'])

                # only keep the new binning if it is better than the old
                if not decodeCode([decoder], code) and \
                   error < code['error']:
                    (code['binned'], code['error']) = (binned, error)

        # combine the codes of all failures in a last-ditch effort
        if combineFailures:
            if 'decoder' not in code:
                separate = binner.divideSignals(code['raw'])
                for x in range(len(separate)):
                    lastDitch[x].extend(separate[x])
                    lastDitch[-1].extend(separate[x])

    if combineFailures:
        for code in codes:
            if 'decoder' not in code:
                (binned, error) = (code['binned'], code['error'])
                (code['binned'],
                 code['error']) = binner.computeBins_fromResults(lastDitch,
                                                                 code['raw'])

                # only keep the new binning if it is better than the old
                if not decodeCode(decoderList, code) and \
                   error < code['error'] and False:
                    (code['binned'], code['error']) = (binned, error)

    count = 0
    for code in codes:
        if 'decoder' not in code:
            message(LOG_NORMAL, "No decoder recognized %s\n" % code['name'])
        else:
            count += 1

    # print the unrecognized ones so the user can take a look
    displayCodes(codes, 'raw')
    displayCodes(codes, 'binned')

    print 'Identified %d / %d from %s\n' % (count, len(codes), source)
    totalCount += count
    totalCodes += len(codes)
    return (totalCount, totalCodes)
