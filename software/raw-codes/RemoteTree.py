from PyQt4 import QtCore, QtGui

from support import *

import sender

msgs = []
def displayWarning(msg, oneTime = False, question = False):
    if oneTime:
        if msg in msgs:
            return
        msgs.append(msg)
    if question:
        return QtGui.QMessageBox.question(None, "Question", msg, QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
    QtGui.QMessageBox.warning(None, "Warning", msg)

class RemoteTree(QtGui.QTreeWidget):
    def __init__(self, *args):
        QtGui.QTreeWidget.__init__(self, *args)
        self.mouseEvent = None

    # need to record the state of the right button on the press event
    def mousePressEvent(self, event):
        self.mouseEvent = QtGui.QMouseEvent(event)
        QtGui.QTreeWidget.mousePressEvent(self, event)

    def makePopupMenu(self, items):
        menu = QtGui.QMenu(self)
        for item in items:
            if item:
                action = menu.addAction(item)
                QtCore.QObject.connect(action, QtCore.SIGNAL("triggered()"),
                                       eval('self.menuClick_%s' % item.replace('&', '').replace(' ', '_')))
            else:
                menu.addSeparator()
        return menu

    def clickHandler(self, item, column):
        # if this was a right-click on an item popup a menu
        if int(self.mouseEvent.buttons()) == self.mouseEvent.button() and \
           self.mouseEvent.button() == QtCore.Qt.RightButton:
            cmds = []
            if not self.currentItem().parent():
                cmds.extend(('&Add Button', '&Properties'))
            else:
                cmds.append('&Send')
            cmds.extend((None, '&Rename', '&Delete'))

            self.makePopupMenu(cmds).popup(self.mouseEvent.globalPos())

    def menuClick_Properties(self):
        self.detailsDlg = makeDialog('RemoteDetailsDlg', True,
                                     self.currentItem())

    def menuClick_Delete(self):
        if self.currentItem().parent():
            self.parent.deleteItem(str(self.currentItem().parent().text(0)),
                                   str(self.currentItem().text(0)))
        else:
            self.parent.deleteItem(str(self.currentItem().text(0)))

    def menuClick_Rename(self):
        self.editItem(self.currentItem(), 0)

    def menuClick_Send(self):
        sender.sendSignal(self.parent.remotes[str(self.currentItem().parent().text(0))], str(self.currentItem().text(0)))

    def menuClick_Add_Button(self):
        print 'Recving....'

class RemoteTreeItem(QtGui.QTreeWidgetItem):
    def __init__(self, *args):
        QtGui.QTreeWidgetItem.__init__(self, *args)

    def setData(self, column, role, value):
        text = str(value.toString()).strip()

        if column == 0:
            if self.parent():
                if not text:
                    displayWarning('Code names cannot be deleted.  To delete the code right-click and select "Delete".', True)
                    return

                # a few temporary variables
                parent = str(self.parent().text(0))
                oldName = str(self.text(0))
                codes = self.treeWidget().parent.remotes[parent]['codes']

                if text == oldName:
                    return

                if text in codes:
                    displayWarning('Code names must be unique.')
                    return

                self.treeWidget().parent.renameCode(parent, oldName, text)
            else:
                if not text:
                    displayWarning('Remote control names cannot be deleted.  To delete the remote right-click and select "Delete".', True)
                    return

                oldName = str(self.text(0))

                if text == oldName:
                    return

                if text in self.treeWidget().parent.remotes:
                    if displayWarning('Do you wish to merge codes from %s into %s?' % (oldName, text), question = True) == QtGui.QMessageBox.Yes:
                        self.treeWidget().parent.mergeRemotes(text, oldName)
                    return

                self.treeWidget().parent.renameRemote(oldName, text)
        elif column == 1:
            try:
                int(text, 0)
            except ValueError:
                # warn user once of their mistake and do NOT call the parent
                displayWarning('Codes must be specified as numbers.\n(will be displayed in hex format)', True)
                # TODO: must handle the pre_data_bits....
                return

            self.treeWidget().parent.setCodeValue(str(self.parent().text(0)),
                                                  str(self.text(0)),
                                                  int(text, 0))
        QtGui.QTreeWidgetItem.setData(self,
                                      column, role, QtCore.QVariant(text))
