#!/usr/bin/python -tt

import iguanaIR
import struct
import subprocess
import time

# General device transaction function
# handles get-version command specially and processes the version
# otherwise dumps from the raw command output directory or returns errors if applicable
def deviceTransaction(device,type, data = ''):
    retval = False
    req = iguanaIR.createRequest(type, data)
    if not iguanaIR.writeRequest(req, device):
        print 'Failed to write packet.'
        return False
    else:
        resp = iguanaIR.readResponse(device, 10000)
        if resp is None:
            print 'Repsonse is None (this is odd)'
        elif iguanaIR.responseIsError(resp):
            print 'Error response.'
        elif type == iguanaIR.IG_DEV_GETVERSION:
            # Device responded with get-version, handle differently
            data = iguanaIR.removeData(resp)
            retval = ord(data[0]) + (ord(data[1])*100)
        else:
            retval = iguanaIR.removeData(resp)
    return retval

#SendAndReceive -- Sends a signal from one device and receives it on a different device
#Todo: Run lirc internally on specified device looking at specified lirc config file
def SendAndReceive(sender,receiver,sendtype="irsend", remote="TESTREMOTE", button="pause"):
    #turn on receiver
    deviceTransaction(receiver,iguanaIR.IG_DEV_RECVON)
    if sendtype == "irsend":
        irsend=subprocess.Popen(("irsend", "SEND_ONCE",remote,button))
        irsend.wait()
        if irsend.returncode != 0 :
            print "Error Sending with irsend"
            return False
    # To Do -- add case for Joe's sending program instead of irsend
    else:
        print "Undefined Sending Method"
        return False
	
    #Processing of the Receiver Data
    
    #Variable initialization
    #type is the type (Pulse or Space) of the most recent data.
    #prevtype is the type of the previous data.
    #initalize type as -1 meaning neight pulse nor space, so we detect
    #first data as "new" and not a continuation of previous pulse or space.
    type = -1
    
    #current is the current time count for the pulse / space that we are counting
    current = 0
    
    #record is a variable about whether we should record the data or not 
    #used to ignore the first spaces
    record = False
	
    #Define the signal definition of PULSE and SPACE for clarity
    PULSE = iguanaIR.IG_PULSE_BIT
    SPACE = 0
    
    #spaces and pulses are arrays containing the length of the spaces and pulses
    #initialize as a an empty array
    spaces = []
    pulses = []
    while True:
        try:
            packet = iguanaIR.readResponse(receiver, 1000)
        except KeyboardInterrupt:
            break
        data = iguanaIR.removeData(packet)
        
        #Not sure why, but in struct.unpack I can't do ('I' * len(data)/4, but I can if I get
        #it to a temporary variable first. Anywy, unpacks the data as a 32bit integer (I) where 
        #every 4 bytes of data in the array data is one 32bit integar. (so if data packet is 8
        #long, that's two integers, ('II')
        templength = len(data)/4
        for signal in struct.unpack('I' * templength , data):            
            
            typeprev = type
            type = signal & iguanaIR.IG_PULSE_BIT
            if typeprev == type:
                #This newest packet is a continuation of the previous packet
                #Add its length to the variable current and move on
                current += signal & iguanaIR.IG_PULSE_MASK
			
            else:
                #Ok, this packet is new 
                if type == PULSE:
					#We received the begining of a pulse, that means we should start recording,
					#but first we should record previous space, which is not complete. Assuming, that it exists
					#(not true if this is the first packet) and if we are recording
					
                    if record==True and typeprev != -1:
                        #record previous space
                        spaces.append(current)
                    record=True
				
                elif type == SPACE:
                    #We received a new space. Record previous pulse unless this is first packet or we aren't recording
                    if record==True and typeprev != -1:
                        #record previous pulse
                        pulses.append(current)
                else: 
                    print "Error: received new packet that is neigther Pulse nor Space"
                    break
				
                #Reset current count, since we are starting a new pulse or space
                current = signal & iguanaIR.IG_PULSE_MASK
				
            #When does this loop end? It will end if we get a long space (10000) after we are record
            #(after we received a pulse)
            #Or after a very long space regardless.... this is a timeout and the earlier says that we got our signal
				
        if (current > 1000000 ) and (record == True):
            print 'Captured the signal'
            break
        if current > 10000000:
            print "No Signal... timeout reached"
            break	
		
    #Outside the while loop... we either got our signal, timed-out. Turn off receiver
    deviceTransaction(receiver,iguanaIR.IG_DEV_RECVOFF)
    return (pulses, spaces)

    #I think this is unnecessary... above return line happens first, ignores second return line
    #return True


#connect to sending and receiving devices and check their version
sender = iguanaIR.connect('main')
receiver = iguanaIR.connect('tester')
version = deviceTransaction(sender,iguanaIR.IG_DEV_GETVERSION)
print 'Sending Device Version is', version
version = deviceTransaction(receiver,iguanaIR.IG_DEV_GETVERSION)
print 'Receiving Device Version is', version


print "First Send / Receive"
(p1, s1 ) = SendAndReceive(sender,receiver,"irsend","testremote","pause")

print "Second Send / Receive"
(p2, s2 ) = SendAndReceive(sender,receiver,"irsend","testremote","pause")


#Got the data from two different send / receive combinations. 
#Now process those signals

#My routine needs the length of both pulse arrays and the same length
#for both space arrays.

#Print out lines where the difference in length of pulse / space is greater than maxdiff
#in future recode to handle percent difference and different requirements on pulses and spaces
maxdiff=0

if len(p1) == len(p2):
    i=0
    for code in p1 :
        if abs(p1[i] - p2[i]) > maxdiff:
            print "Pulse width error too large at line ", i ,": ", p1[i], " vs ",p2[i], "   Diff: ",p1[i]-p2[i]
        i=i+1

else:
    print "Arrays not right length -- aborting"
    print "First shot number of pulses is ",len(p1)
    print "Second shot number of pulses is ", len(p2)



if len(s1)  == len(s2):
        i=0
        if abs(s1[i] - s2[i]) > maxdiff:
            print "Space width error too large at line ", i , " ", s1[i], " vs ",s2[i], "   Diff: ",s1[i]-s2[i]
        i=i+1

else:
    print "Arrays not right length -- aborting"
    print "First shot number of spaces is ", len(s1)
    print "Second shot number of spaces is ", len(s2)
