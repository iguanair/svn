import os
import sys

#output "constants"
LOG_FATAL  = 0
LOG_ERROR  = 1
LOG_WARN   = 2
LOG_ALWAYS = 2.5
LOG_NORMAL = 3
LOG_INFO   = 4
LOG_DEBUG  = 5

msgPrefixes = [
    "FATAL: ",
    "ERROR: ",
    "WARNING: ",
    "",
    "INFO: ",
    "DEBUG: "
]

null = open('/dev/null', 'r+')

currentLevel = LOG_NORMAL
logFile = None

# shared variables
maxWidth = 80
tryNoise = False
combineSuccesses = True
combineFailures = False

def dieCleanly(level = None):
    """Exit the application with proper cleanup."""

    #TODO: perform application cleanup

    if level == None:
        level = LOG_ERROR

    #exit with appropriate value
    if level == LOG_FATAL:
        sys.exit(1)
    sys.exit(0)


def message(level, msg):
    """Print a message to a certain debug level"""
    retval = None

    if level <= currentLevel or level == LOG_ALWAYS:
        out = sys.stdout

        # if logfile is open print to it instead
        if logFile == "-":
            out = sys.log
        elif level <= LOG_WARN:
            out = sys.stderr

        retval = msgPrefixes[int(level + 0.5)] + msg
        out.write(retval)
        retval = len(retval)

    if level <= LOG_FATAL:
        dieCleanly(level)

    return retval

def changeLevel(amount):
    global currentLevel
    currentLevel += amount

def makeDialog(name, show = False, *args):
    module = __import__(name)
    uiModule = __import__('Ui_%s' % name)
    dialog = eval('module.%s(*args)' % name)
    dialog.ui = eval('uiModule.Ui_%s()' % name)
    dialog.ui.setupUi(dialog)
    if show:
        dialog.show()

    return dialog
