from __future__ import with_statement

import struct
import threading
import select
import socket
import os
import time

import iguanaIR

def parseConfiguration(filename):
    # read the configuration file for remotes
    remotes = {}
    with open(filename, 'r') as conf:
        remote = None
        for line in iter(conf.readline, ''):
            line = line[:-1]
            if not line or line.strip()[0] == '#':
                continue

            parts = line.strip().split(None, 1)
            if parts:
                if parts[0].lower() == 'begin':
                    if parts[1].lower() == 'remote':
                        remote = {}
                    elif parts[1].lower() == 'codes':
                        remote['new-codes'] = {}
                    elif parts[1].lower() == 'raw_codes':
                        remote['new-raw_codes'] = {}
                    else:
                        raise Exception("Unknown begin: %s" % parts[1])

                elif parts[0].lower() == 'end':
                    if parts[1].lower() == 'remote':
                        remotes[remote['name']] = remote
                        remote = None
                    elif parts[1].lower() == 'codes':
                        remote['codes'] = remote['new-codes']
                        del remote['new-codes']
                    elif parts[1].lower() == 'raw_codes':
                        remote['raw_codes'] = remote['new-raw_codes']
                        del remote['new-raw_codes']
                    else:
                        raise Exception("Unknown end: %s" % parts[1])
                
                elif remote is None:
                    raise Exception("Bad format for config file: %s" % line)
                elif 'new-codes' in remote:
                    remote['new-codes'][parts[0]] = int(parts[1], 0)
#                    print 'memememem'
                elif 'new-raw_codes' in remote and parts[0] == 'name':
                    name = parts[1]
                    remote['new-raw_codes'][name] = []
                    while True:
                        parts = conf.readline().split()
                        if not parts:
                            break
                        remote['new-raw_codes'][name].extend(parts)
                else:
                    name = parts[0].lower()
                    value = []
                    for part in parts[1].split():
                        try:
                            value.append(int(part, 0))
                        except ValueError:
                            value = parts[1]
                            break
                    else:
                        if len(value) == 1:
                            value = value[0]
                    remote[name] = value
    return remotes

def writeConfiguration(remotes, filename):
    output = open(filename, "w")
    output.write("""
# Written by PIRC
#
# Please make this file available to others
# by sending it to <lirc@bartelmus.de>
#
# contributed by 
#
# brand:                       lircd.conf
# model no. of remote control: 
# devices being controlled by this remote:
#
""")
    for remote in remotes:
        output.write("""
begin remote
""")
        remote = remotes[remote]
        keys = remote.keys()
        for name in ('', 'name', 'bits', 'flags', 'eps', 'aeps', 'frequency',
                     '', 'header', 'one', 'zero', 'plead', 'ptrail', 'repeat', 'pre_data_bits', 'pre_data', 'gap', 'min_repeat', 'toggle_bit'):
            if not name:
                output.write('\n')
            elif name in keys:
                value = remote[name]
                if isinstance(value, list):
                    for x in range(len(value)):
                        value[x] = '%5d' % value[x]
                    value = ' '.join(value)
                elif name == 'pre_data':
                    value = '0x%X' % value
                output.write('  %s%s%s\n' % (name, ' ' * (14 - len(name)),
                                             value))
                keys.remove(name)

        output.write('\n\n      begin codes\n')
        for code in remote['codes']:
            format = '          %%s%%s0x%%0%sX\n' % ((remote['bits'] + 3) / 4)
            output.write(format % (code, ' ' * (24 - len(code)),
                                   remote['codes'][code]))
        output.write('      end codes\n\n')
        keys.remove('codes')

        output.write("end remote\n")
        if keys:
            raise Exception('Unused remote variables: %s' % keys)

def packSignalsForSend(signals):
    isSpace = False
    copy = []
    for signal in signals:
        signal &= iguanaIR.IG_PULSE_MASK
        if isSpace:
            isSpace = False
        else:
            signal |= iguanaIR.IG_PULSE_BIT
            isSpace = True
        copy.append(signal)

    return struct.pack('I' * len(signals), *copy)

# the following provides a lirc-style interface for already compiled clients
state = {
    'listener' : None,
    'done'     : False,
    'sockName' : 'temp.sock'
}
def listenForClients(sendFunc):
    state['send'] = sendFunc
    state['listener'] = threading.Thread(target = listenLoop,
                                         args = (state,))
    state['listener'].start()

def stopListening():
    if state['listener']:
        state['done'] = True
        state['listener'].join()

clients = []
def listenLoop(state):
    global clients

    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM, 0)
    while True:
        try:
            sock.bind((state['sockName']))
        except socket.error, inst:
            if inst[0] == 98:
                try:
                    open(state['sockName'], 'r')
                except IOError, inst:
                    if inst[0] == 6:
                        os.unlink(state['sockName'])
                        continue
                    else:
                        raise
            else:
                raise
        break
    os.chmod(state['sockName'], 0666)
    sock.listen(5)

    while not state['done']:
        sockets = [sock]
        sockets.extend(clients)
        (canRead, canWrite, hasError) = select.select(sockets, [], sockets, 1)
        for client in canRead:
            if client == sock:
                (incoming, junk) = sock.accept()
                clients.append(incoming)
            else:
                data = client.recv(1024)
                if not data:
                    client.close()
                    clients.remove(client)
                else:
                    (command, args) = data.split(None, 1)
                    if command == 'send_once':
                        client.send("BEGIN\n")
                        client.send(data)
                        state['send'](*(args.split()))
                        client.send("SUCCESS\nEND\n")
                    else:
                        print [data]

prevRecvTime = time.time()
count = 0
def sendToClients(code, remote, button):
    global clients, prevRecvTime, count

    recvTime = time.time()

    # TODO: there is probably a better way....
    if recvTime - prevRecvTime >= 0.2:
        count = 0
    else:
        count += 1
    prevRecvTime = recvTime
    text = '%.16x %.2d %s %s\n' % (code['value'], count,
                                   button, remote['name'])
    for client in clients:
        client.send(text)
