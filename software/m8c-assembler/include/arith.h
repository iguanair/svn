//-----------------------------------------------------------------------------
//  FILENAME: arith.h
//
//  DESCRIPTION: Prototypes for the fastcall16 arithmetic functions in the
//               arith_lm, arith_lv, arith_sm, arith_sv libraries.
//
//-----------------------------------------------------------------------------
//  Copyright (c) Cypress MicroSystems 2005. All Rights Reserved.
//-----------------------------------------------------------------------------


#include <m8c.h>

#pragma fastcall16 mul8
#pragma fastcall16 mulu_8x8_16
#pragma fastcall16 mul_8x8_16
#pragma fastcall16 mul16
#pragma fastcall16 mulu_16x16_32
#pragma fastcall16 mul_16x16_32
#pragma fastcall16 mul32
#pragma fastcall16 divu_8x8_8
#pragma fastcall16 modu_8x8_8
#pragma fastcall16 div_8x8_8
#pragma fastcall16 mod_8x8_8
#pragma fastcall16 divu_16x16_16
#pragma fastcall16 modu_16x16_16
#pragma fastcall16 div_16x16_16
#pragma fastcall16 mod_16x16_16
#pragma fastcall16 divu_32x32_32
#pragma fastcall16 modu_32x32_32
#pragma fastcall16 div_32x32_32
#pragma fastcall16 mod_32x32_32
#pragma fastcall16 fpadd
#pragma fastcall16 fpsub
#pragma fastcall16 fpmul
#pragma fastcall16 fpdiv
#pragma fastcall16 fp2long
#pragma fastcall16 long2fp
#pragma fastcall16 fp2ulong
#pragma fastcall16 ulong2fp
#pragma fastcall16 fpcmp


//-----------------------------------------------------------------------------
// The following functions are not yet implemented.
//
// Note that some of these function prototypes specify 4-byte return types.
// This is not supported in fastcall16.  These function prototypes must be
// corrected as the functions are implemented.
//
// Move prototypes above this comment as the functions are implemented,
// delete this comment when no prototypes are left below.
//-----------------------------------------------------------------------------

// Return a * b
CHAR  mul8(CHAR a, CHAR b);

// Return a * b
WORD  mulu_8x8_16(BYTE a, BYTE b);

// Return a * b
INT   mul_8x8_16(CHAR a, CHAR b);

// Return a * b
WORD  mul16(WORD a, WORD b);

// Return a * b
void  mulu_16x16_32(WORD a, WORD b, DWORD *product);

// Return a * b
void  mul_16x16_32(INT a, INT b, LONG *product);

// Return a * b
void  mul32(DWORD a, DWORD b, DWORD *product);

// Return a / b
BYTE  divu_8x8_8(BYTE a, BYTE b);

// Return a % b
BYTE  modu_8x8_8(BYTE a, BYTE b);

// Return a / b
CHAR  div_8x8_8(CHAR a, CHAR b);

// Return a % b
CHAR  mod_8x8_8(CHAR a, CHAR b);

// Return a / b
WORD  divu_16x16_16(WORD a, WORD b);

// Return a % b
WORD  modu_16x16_16(WORD a, WORD b);

// Return a / b
INT   div_16x16_16(INT a, INT b);

// Return a % b
INT   mod_16x16_16(INT a, INT b);

// Return a / b
void divu_32x32_32(DWORD a, DWORD b, DWORD *quotient);

// Return a % b
void modu_32x32_32(DWORD a, DWORD b, DWORD *remainder);

// Return a / b
void  div_32x32_32(LONG a, LONG b, LONG *quotient);

// Return a % b
void  mod_32x32_32(LONG a, LONG b, LONG *remainder);

// Return a + b
void fpadd(float a, float b, float *sum);

// Return a - b
void fpsub(float a, float b, float *difference);

// Return a * b
void fpmul(float a, float b, float *product);

// Return a / b
void fpdiv(float a, float b, float *quotient);

// Convert a to LONG
void  fp2long(float a, LONG *result);

// Convert a to float
void long2fp(long a, float *result);

// Convert a to DWORD
void fp2ulong(float a, DWORD *result);

// Convert a to float
void ulong2fp(DWORD a, float *result);

// Return 1 if a > b, 0 if a == b, or -1 if a < b
CHAR  fpcmp(float a, float b);


// end of file arith.h
