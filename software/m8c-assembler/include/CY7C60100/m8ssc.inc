;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  M8SSC.INC -- CY7C601xx Microcontroller Device SSC Declarations
;;;
;;;  Copyright (c) 2005 - 2006, Cypress Semiconductor, Inc. All rights reserved.
;;;
;;;
;;;  This file provides address constants, bit field masks and a set of macro
;;;  facilities for the Cypress CY7C601xx Microcontroller family.
;;;
;;;  Last Modified: August 31, 2005
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Common Supervisory Code Variables
   bSSC_KEY1:                       equ      F8h   ; supervisory key
   bSSC_KEYSP:                      equ      F9h   ; supervisory stack ptr key
   bSSC_TABLE_TableId:              equ      FAh   ; table ID
         
OPER_KEY:		                    equ      3Ah   ; operation key
;--------------------------------
; SSC_Action macro command codes 
;--------------------------------
FLASH_READ:                         equ      1     ; flash read command
FLASH_WRITE:                        equ      2     ; flash write command
FLASH_ERASE:                        equ      3     ; flash erase command

TABLE_READ:							equ      6     ; table read command
FLASH_CHECKSUM:						equ      7	   ; flash checksum calculation command
CALIBRATE0:                         equ      8     ; Calibrate without checksum
CALIBRATE1:                         equ      9     ; Calibrate with checksum
;--------------------------------
; SSC_Action table read addresses
;--------------------------------
SILICON_ID_1:						equ		 F8h   ; first byte of silicon ID
SILICON_ID_0:						equ		 F9h   ; second byte of silicon ID
VOLTAGE_TRIM_3V:					equ		 F8h   ; 3.3V internal voltage reference trim value
OSCILLATOR_TRIM_3V:					equ		 F9h   ; 3.3V internal main oscillator trim value
VOLTAGE_TRIM_5V:					equ		 FCh   ; 5V internal voltage reference trim value
OSCILLATOR_TRIM_5V:					equ		 FDh   ; 5V internal main oscillator trim value
;-----------------------------------------------------------------------------
;  MACRO NAME: SSC_Action
;
;  DESCRIPTION:
;     Performs locally defined supervisory operations.
;     Macro Instantiation: SSC_Action bOperation
;
;     !!! DO NOT CHANGE THIS CODE !!!
;        This sequence of opcodes provides a 
;        signature for the debugger and ICE.
;     !!! DO NOT CHANGE THIS CODE !!!
;
;  ARGUMENTS:
;     BYTE  bOperation   - specified supervisory operation - defined operations
;                          are:  FLASH_WRITE, FLASH_ERASE, FLASH_READ, TABLE_READ,
;                                FLASH_CHECKSUM, PROTECT_BLOCK
;
;  RETURNS:
;     none.
;
;  SIDE EFFECTS:
;     A and X registers are destroyed
;
;  PROCEDURE:  
;     1) specify a 3 byte stack frame.  Save in [KEYSP]
;     2) insert the flash Supervisory key in [KEY1]
;     3) store function code in A
;     4) call the supervisory code
;-----------------------------------------------------------------------------
macro SSC_Action  
      mov   X, SP                         ; copy SP into X
      mov   A, X                          ; mov to A
      add   A, 3                          ; create 3 byte stack frame
      mov   [bSSC_KEYSP], A               ; save stack frame for supervisory code
      mov   [bSSC_KEY1], OPER_KEY		  ; load the supervisory code for supervisory operations
      mov   A, @0                         ; load A with specific Flash operation
      SSC                                 ; SSC call the supervisory code
endm
