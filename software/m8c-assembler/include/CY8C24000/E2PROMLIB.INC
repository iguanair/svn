;------------------------------------------------------------------------------
;  FILENAME:   E2PROMLIB.inc for PSoC CY8C22/24/27xxx devices
;   Version: 2.2.0.5, Updated on 2004/06/29 at 16:08:54
;------------------------------------------------------------------------------
;  DESCRIPTION:
;     Include file for E2PROM Library routines.  These routines use the
;     FlashBlock functions to emulate an EEPROM device.  These algorithms
;     translate flash block oriented operations into byte-wise operations.
;
;------------------------------------------------------------------------------
;  Copyright (c) Cypress MicroSystems 2000-2004. All rights reserved.
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;  FUNCTION NAME: bE2Write
;
;  DESCRIPTION:
;     Writes the specified E2PROM data at the wAddr and wByteCount from RAM into
;     Flash into the defined E2PROM.
;
;     Prototype in C is:
;
;        #pragma  fastcall16 bE2Write
;        BYTE bE2Write( WORD wAddr, BYTE * pbData, WORD wByteCount, CHAR cTemperature );
;
;  ARGUMENTS:
;
;     wAddr:         WORD   - relative OFFSET in defined E2PROM to write data
;     pbData:        BYTE * - pointer to the RAM buffer of data to write
;     wByteCount:    WORD   - number of bytes to write into E2PROM
;     cTemperature:  CHAR   - temperature in degrees celsius
;
;     Hidden Argument:
;     CPU Flag       BYTE   - passed on stack after calling address
;     wFirstBlockId  WORD   - first block ID of E2PROM device
;                           - must be placed on stack after CPU flag
;
;  RETURNS:    NOERROR, FAILURE, or STACKOVERFLOW
;
;  SIDE EFFECTS:  If a partial block is to be saved to flash, then a 64 byte buffer
;                 is temporary allocated.
;
;------------------------------------------------------------------------------
;-------------------------------------
;  Argument Frame Structure
;-------------------------------------
E2_WR_ARG_cTemperature:          equ     -12       ; chip temperature
E2_WR_ARG_wByteCount:            equ     -11       ; Byte Count to Write
E2_WR_ARG_pbData:                equ     -9        ; Data buffer with data to write
E2_WR_ARG_wAddr:                 equ     -7        ; Address offset in E2PROM to write
E2_WR_RETURN_ADDRESS:            equ     -5        ; callers return address
E2_WR_CPU_FLAGS:                 equ     -3        ; CPU flag stored on caller frame for RETI return
E2_WR_ARG_wFirstBlockID:         equ     -2        ; first block ID

E2_WR_ARG_STACK_FRAME_SIZE:      equ      7        ; SIZE of the argument list in stack frame


;-------------------------------------
;  Return Values
;-------------------------------------
NOERROR:                         equ       0       ; Successfull completion
FAILURE:                         equ      -1       ; Error condition
STACKOVERFLOW:                   equ      -2       ; Error Stack Overflow



;-----------------------------------------------------------------------------
;  FUNCTION NAME: E2Read
;
;  DESCRIPTION:
;     Reads the specified E2PROM data at offset=wAddr for wByteCount bytes  and
;     places the data read into the RAM buffer pbDataDest.
;
;     Prototype in C is:
;
;        #pragma  fastcall16 E2Read
;        void E2Read( WORD wAddr, BYTE * pbDataDest, WORD wByteCount );
;
;  ARGUMENTS:
;
;     wAddr:         WORD   - relative OFFSET in defined E2PROM to read data
;     pbDataDest:    BYTE * - pointer to the RAM buffer to place read data
;     wByteCount:    WORD   - number of bytes to read from E2PROM
;
;    Hidden Argument - 
;     CPU Flag       BYTE   - passed on stack after calling return address
;     bFirstBlockId  BYTE   - first block ID of E2PROM device - passed in Accumulator
;
;  RETURNS:       NONE.
;
;  SIDE EFFECTS:  NONE.
;
;-----------------------------------------------------------------------------
E2_RD_ARG_wByteCount:         equ     -9           ; Byte Count to Read
E2_RD_ARG_pbDataDest:         equ     -7           ; Data buffer to store read data
E2_RD_ARG_wAddr:              equ     -5           ; Address offset in E2PROM to Read
E2_RD_CALLER_RETURN_ADDR:     equ     -3 
E2_RD_CPU_FLAG:               equ     -1           ; CPU flag stored on caller frame for RETI return
E2_RD_ARG_STACK_FRAME_SIZE:   equ      6           ; SIZE of argument list in stack frame

;--------------------
;  End of File
;--------------------
