;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  M8C.INC -- M8C21030 Microcontroller Device System Declarations
;;;
;;;  Copyright (c) 2005 Cypress MicroSystems, Inc. All rights reserved.
;;;
;;;
;;;  This file provides address constants, bit field masks and a set of macro
;;;  facilities for the Cypress MicroSystems 21x3x Microcontroller devices.
;;;
;;;  Last Modified: January 21, 2005
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;=============================================================================
;; Definition of abbreviations used in the descriptions below
;;  (RW)   The register or bit supports reads and writes
;;  (W)    The register or bit is write-only
;;  (R)    The register or bit is read-only
;;  (#)    Access to the register is bit specific (see the family datasheet)
;;  (RC)   The register or bit can be read, but writing a 0 will clear it,
;;         writing a 1 will have no effect.
;;=============================================================================

;;=============================================================================
;;      System Registers
;;=============================================================================

;----------------------------
;  Flag Register Bit Fields
;----------------------------
FLAG_PGMODE_MASK:  equ C0h     ; Paging control for > 256 bytes of RAM
FLAG_PGMODE_0:     equ 00h     ; Direct to Page 0,      indexed to Page 0
FLAG_PGMODE_1:     equ 40h     ; Direct to Page 0,      indexed to STK_PP page
FLAG_PGMODE_2:     equ 80h     ; Direct to CUR_PP page, indexed to IDX_PP page
FLAG_PGMODE_3:     equ C0h     ; Direct to CUR_PP page, indexed to STK_PP page
FLAG_PGMODE_00b:   equ 00h     ; Same as PGMODE_0
FLAG_PGMODE_01b:   equ 40h     ; Same as PGMODE_1
FLAG_PGMODE_10b:   equ 80h     ; Same as PGMODE_2
FLAG_PGMODE_11b:   equ C0h     ; Same as PGMODE_3
FLAG_XIO_MASK:     equ 10h     ; I/O Bank select for register space
FLAG_SUPER:        equ 08h     ; Supervisor Mode
FLAG_CARRY:        equ 04h     ; Carry Condition Flag
FLAG_ZERO:         equ 02h     ; Zero  Condition Flag
FLAG_GLOBAL_IE:    equ 01h     ; Glogal Interrupt Enable


;;=============================================================================
;;      Register Space, Bank 0
;;=============================================================================

;------------------------------------------------
;  Port Registers
;  Note: Also see this address range in Bank 1.
;------------------------------------------------
; Port 0
PRT0DR:       equ 00h          ; Port 0 Data Register                     (RW)
PRT0IE:       equ 01h          ; Port 0 Interrupt Enable Register         (RW)
; Port 1
PRT1DR:       equ 04h          ; Port 1 Data Register                     (RW)
PRT1IE:       equ 05h          ; Port 1 Interrupt Enable Register         (RW)
; Port 2
PRT2DR:       equ 08h          ; Port 2 Data Register                     (RW)
PRT2IE:       equ 09h          ; Port 2 Interrupt Enable Register         (RW)
; Port 3
PRT3DR:       equ 0Ch          ; Port 3 Data Register                     (RW)
PRT3IE:       equ 0Dh          ; Port 3 Interrupt Enable Register         (RW)

VLT_CR:       equ E3h          ; Voltage Monitor Control Register         (RW)
VLT_CR_PORLEV:          equ 30h    ; MASK: Mask for Power on Reset level control
VLT_CR_POR_LOW:         equ 00h    ;   Lowest  Precision Power-on Reset trip point
VLT_CR_POR_MID:         equ 10h    ;   Middle  Precision Power-on Reset trip point
VLT_CR_POR_HIGH:        equ 20h    ;   Highest Precision Power-on Reset trip point
VLT_CR_LVDTBEN:         equ 08h    ; MASK: Enable the CPU Throttle Back on LVD
VLT_CR_VM:              equ 07h    ; MASK: Mask for Voltage Monitor level setting
        
;-----------------------------------------------
;  SPI Registers
;  Note: Also see this address range in Bank 1.
;-----------------------------------------------

SPI_TXR:      equ 29h  ; SPI Transmit Data Register (W)
SPI_RXR:      equ 2Ah  ; SPI Receive Data Register (R)
              
SPI_CR:       equ 2Bh  ; SPI Control Register
SPI_CR_LSb_First:       equ 80h  ; (RW)
SPI_CR_Overrun:         equ 40h  ; (R)
SPI_CR_SPI_Complete:    equ 20h  ; (R)
SPI_CR_TX_Reg_Empty:    equ 10h  ; (R)
SPI_CR_RX_Reg_Full:     equ 08h  ; (R)
SPI_CR_Clock_Phase:     equ 04h  ; (RW)
SPI_CR_Clock_Polarity:  equ 02h  ; (RW)
SPI_CR_Enable:          equ 01h  ; (RW)

;-------------------------------------
;  Analog Control Registers
;-------------------------------------
AMUXCFG:      equ 61h          ; Analog MUX Configuration
AMUXCFG_ICAPEN:         equ C0h  ; Internal capacitance to the analog global bus (RW)
AMUXCFG_INTCAP:         equ 03h  ; External integration capacitor in the charge integration mode (RW)


;-----------------------------------------------
;  Global General Purpose Data Registers
;-----------------------------------------------
TMP_DR0:      equ 6Ch          ; Temporary Data Register 0  (RW)
TMP_DR1:      equ 6Dh          ; Temporary Data Register 1  (RW)
TMP_DR2:      equ 6Eh          ; Temporary Data Register 2  (RW)
TMP_DR3:      equ 6Fh          ; Temporary Data Register 3  (RW)

;------------------------------------------------------------------------------
;  Analog Comparator registers
;------------------------------------------------------------------------------

CMP_RDC:      equ 78h  ; Comparator Read/Clear Register 
CMP_RDC_CMP1D:          equ 20h ; MASK: Dynamic status of comparator 1    (R)
CMP_RDC_CMP0D:          equ 10h ; MASK: Dynamic status of comparator 0    (R)
CMP_RDC_CMP1L:          equ 02h ; MASK: Latched status of comparator 1    (RC)
CMP_RDC_CMP0L:          equ 01h ; MASK: Latched status of comparator 0    (RC)
                                      
CMP_MUX:      equ 79h  ; Comparator Multiplexor Register 0        (RW)
CMP_MUX_INP1:           equ C0h ; MASK: Comparator 1 Positive Terminal Input 
CMP_MUX_INN1:           equ 30h ; MASK: Comparator 1 Negative Terminal Input
CMP_MUX_INP0:           equ 0Ch ; MASK: Comparator 0 Positive Terminal Input 
CMP_MUX_INN0:           equ 03h ; MASK: Comparator 0 Negative Terminal Input

CMP_CR0:      equ 7Ah  ; Comparator Control Register 0             (RW)
CMP_CR0_CMP1_Range:     equ 20h ; MASK: Comparator 1 Range 
CMP_CR0_CMP1EN:         equ 10h ; MASK: Comparator 1 Enable 
CMP_CR0_CMP0_Range:     equ 02h ; MASK: Comparator 0 Range 
CMP_CR0_CMP0EN:         equ 01h ; MASK: Comparator 0 Enable

CMP_CR1:      equ 7Bh  ; Comparator Control Register 1             (RW)
CMP_CR1_CINT1:          equ 80h ; Comparator 1 select for input to the analog interrupt
CMP_CR1_CPIN1:          equ 40h ; Comparator 1 signal for possible connection to GPIO pin
CMP_CR1_CRST1:          equ 20h ; Comparator 1 latch reset
CMP_CR1_CDS1:           equ 10h ; Data output select for the comparator 1 channel
CMP_CR1_CINT0:          equ 08h ; Comparator 0 select for input to the analog interrupt
CMP_CR1_CPIN0:          equ 04h ; Comparator 0 signal for possible connection to GPIO pin
CMP_CR1_CRST0:          equ 02h ; Comparator 0 latch reset                               
CMP_CR1_CDS0:           equ 01h ; Data output select for the comparator 0 channel        

CMP_LUT:      equ 7Ch  ; Comparator LUT Register
CMP_LUT_LUT0:           equ F0h ; MASK: Enable Comparator to Drive GlobalOut Odd, bit 5 (RW)
CMP_LUT_LUT1:           equ 0Fh ; MASK: Enable Comparator to Drive GlobalOut Odd, bit 1 (RW)


;-----------------------------------------------
;  Capsense Registers
;-----------------------------------------------

CS_CR0:       equ A0h  ; Capsense Control Register 0               (RW)
CS_CR0_CSOUT:           equ C0h ; MASK: Capsense Output 
CS_CR0_MODE:            equ 06h ; MASK: Capsense Counter Mode
CS_CR0_EN:              equ 01h ; MASK: Counter Enable

CS_CR1:       equ A1h  ; Capsense Control Register 1               (RW)
CS_CR1_CHAIN:           equ 80h ; MASK: Counter Chain Control 
CS_CR1_CLKSEL:          equ 60h ; MASK: Capsense Clock Selection
CS_CR1_RLOCLK:          equ 10h ; MASK: Relaxation Oscillator Clock Select
CS_CR1_INV:             equ 08h ; MASK: Input Invert   
CS_CR1_INSEL:           equ 06h ; MASK: Input Selection

CS_CR2:       equ A2h  ; Capsense Control Register 2               (RW)
CS_CR2_IRANGE:          equ C0h ; MASK: IDAC current output 
CS_CR2_IDACDIR:         equ 20h ; MASK: Source/sink state of the IDAC
CS_CR2_IDAC_EN:         equ 10h ; MASK: Manual connection of the IDAC to the analog global bus enable
CS_CR2_PXD_EN:          equ 04h ; MASK: Ground and analog global bus switch   
CS_CR2_CI_EN:           equ 02h ; MASK: Charge integration enable
CS_CR2_RO_EN:           equ 01h ; MASK: Relaxation oscillator enable

CS_CR3:       equ A3h  ; Capsense Control Register 3               (RW)
CS_CR3_REFMUX:          equ 40h ; MASK: Input voltage of the reference buffer 
CS_CR3_REFMODE:         equ 20h ; MASK: Manual connection of the reference buffer output to the analog global bus enable
CS_CR3_REF_EN:          equ 10h ; MASK: Drive the analog global bus by reference buffer enable
CS_CR3_LPFilt:          equ 0Ch ; MASK: Low pass filter approximate time   
CS_CR3_LPF_EN:          equ 03h ; MASK: Low pass filter enable

CS_CNTL:      equ A4h  ; Capsense Counter Low Byte Register               (RO)
CS_CNTH:      equ A5h  ; Capsense Counter High Byte Register              (RO)

CS_STAT:      equ A6h  ; CapSense Status Register              
CS_STAT_INS:            equ 80h  ; Input Status                                  (RC)
CS_STAT_COLS:           equ 40h  ; Counter Carry Out Low Status                  (RC)
CS_STAT_COHS:           equ 20h  ; Counter Carry Out High Status                 (RC)
CS_STAT_PPS:            equ 10h  ; Pulse Width/Period Measurement Status         (RC)
CS_STAT_INM:            equ 08h  ; Input Interrupt/Mask                          (RW)
CS_STAT_COLM:           equ 04h  ; Counter Carry Out Low Interrupt Mask          (RW)
CS_STAT_COHM:           equ 02h  ; Counter Carry Out High Interrupt Mask         (RW)
CS_STAT_PPM:            equ 01h  ; Pulse Width/Period Measurement Interrupt Mask (RW)


;-----------------------------------------------
;  Programmable Timer Registers
;-----------------------------------------------

PT_CFG:       equ B0h  ; Programmable Timer Configuration Register
PT_CFG_One_Shot:        equ 02h  ; One-shot mode (One complete count then stops) (RW)
PT_CFG_START:           equ 01h  ; Timer Start (RW)

PT_DATA1:     equ B1h  ; Programmable Timer Data Register 1  (RW)

PT_DATA0:     equ B2h  ; Programmable Timer Data Register 0  (RW)

;-----------------------------------------------
;  Ram Page Pointers
;-----------------------------------------------
CUR_PP:       equ D0h           ; Current   Page Pointer
CUR_PP_Page_Bits:       equ 07h  ; SRAM Page for generic SRAM access (RW)
STK_PP:       equ D1h           ; Stack     Page Pointer
STK_PP_Page_Bits:       equ 07h  ; SRAM Page to hold the stack (RW)
IDX_PP:       equ D3h           ; Index     Page Pointer
IDX_PP_Page_Bits:       equ 07h  ; SRAM Page for operating indexed memory access (RW)
MVR_PP:       equ D4h           ; MVI Read  Page Pointer
MVR_PP_Page_Bits:       equ 07h  ; SRAM Page for operating MVI Read instructions (RW)
MVW_PP:       equ D5h           ; MVI Write Page Pointer
MVW_PP_Page_Bits:       equ 07h  ; SRAM Page for operating MVI Write instructions (RW)

;------------------------------------------------
;  I2C Configuration Registers
;------------------------------------------------
I2C_CFG:      equ D6h          ; I2C Configuration Register               (RW)
I2C_CFG_PSelect:         equ 40h
I2C_CFG_Stop_IE:         equ 10h
I2C_CFG_Clock_Rate_100K: equ 00h
I2C_CFG_Clock_Rate_400K: equ 04h
I2C_CFG_Clock_Rate_50K:  equ 08h
I2C_CFG_Clock_Rate:      equ 0Ch
I2C_CFG_Enable:          equ 01h

I2C_SCR:      equ D7h          ; I2C Status and Control Register          (#)
I2C_SCR_Bus_Error:       equ 80h  ; (RC)
I2C_SCR_Stop_Status:     equ 20h  ; (RC)
I2C_SCR_ACK:             equ 10h  ; (RW)
I2C_SCR_Address:         equ 08h  ; (RC)
I2C_SCR_Transmit:        equ 04h  ; (RW)
I2C_SCR_LRB:             equ 02h  ; (RC)
I2C_SCR_Byte_Complete:   equ 01h  ; (RC)

I2C_DR:       equ D8h          ; I2C Data Register                        (RW)


;------------------------------------------------
;  System and Global Resource Registers
;------------------------------------------------
INT_CLR0:     equ DAh          ; Interrupt Clear Register 0               (RW)
                         ; Use INT_MSK0 bit field masks

INT_MSK0:     equ E0h          ; General Interrupt Mask Register          (RW)
INT_MSK0_I2C:           equ 80h
INT_MSK0_Sleep:         equ 40h
INT_MSK0_SPI:           equ 20h
INT_MSK0_GPIO:          equ 10h
INT_MSK0_Timer:         equ 08h
INT_MSK0_Capsense:      equ 04h
INT_MSK0_Analog:        equ 02h
INT_MSK0_V_Monitor:     equ 01h

INT_SW_EN:    equ E1h  ; Interrupt Software Enable Register
INT_SW_EN_ENSWINT:      equ 01h  ; (RW)

INT_VC:       equ E2h          ; Interrupt vector register                (RC)
RES_WDT:      equ E3h          ; Watch Dog Timer Register                 (W)

;------------------------------------------------------
;  System Status and Control Registers
;
;  Note: The following registers are mapped into both
;        register bank 0 AND register bank 1.
;------------------------------------------------------
CPU_F:        equ F7h          ; CPU Flag Register Access                 (RO)
                                   ; Use FLAG_ masks defined at top of file
IDAC_D:       equ FDh		   ; DAC Data Register                        (RW)

CPU_SCR1:     equ FEh          ; CPU Status and Control Register #1       (#)
CPU_SCR1_IRESS:         equ 80h    ; MASK: flag, Internal Reset Status bit
CPU_SCR1_SLIMO:         equ 10h	   ; MASK: Slow IMO (internal main osc) enable
CPU_SCR1_IRAMDIS:       equ 01h    ; MASK: Disable RAM initialization on WDR

CPU_SCR0:     equ FFh          ; CPU Status and Control Register #2       (#)
CPU_SCR0_GIES:          equ 80h    ; MASK: Global Interrupt Enable shadow
CPU_SCR0_WDRS:          equ 20h    ; MASK: Watch Dog Timer Reset
CPU_SCR0_PORS:          equ 10h    ; MASK: power-on reset bit PORS
CPU_SCR0_Sleep:         equ 08h    ; MASK: Enable Sleep
CPU_SCR0_STOP:          equ 01h    ; MASK: Halt CPU bit


;;=============================================================================
;;      Register Space, Bank 1
;;=============================================================================

;------------------------------------------------
;  Port Registers
;  Note: Also see this address range in Bank 0.
;------------------------------------------------
; Port 0
PRT0DM0:      equ 00h          ; Port 0 Drive Mode 0                      (RW)
PRT0DM1:      equ 01h          ; Port 0 Drive Mode 1                      (RW)

; Port 1
PRT1DM0:      equ 04h          ; Port 1 Drive Mode 0                      (RW)
PRT1DM1:      equ 05h          ; Port 1 Drive Mode 1                      (RW)

; Port 2
PRT2DM0:      equ 08h          ; Port 2 Drive Mode 0                      (RW)
PRT2DM1:      equ 09h          ; Port 2 Drive Mode 1                      (RW)

; Port 3
PRT3DM0:      equ 0Ch          ; Port 3 Drive Mode 0                      (RW)
PRT3DM1:      equ 0Dh          ; Port 3 Drive Mode 1                      (RW)

;------------------------------------------------
;  Analog Mux Bus Port Enable Bits
;------------------------------------------------
MUX_CR0:      equ D8h          ; Analog Mux Port 0 Bit Enables Register
MUX_CR1:      equ D9h          ; Analog Mux Port 1 Bit Enables Register
MUX_CR2:      equ DAh          ; Analog Mux Port 2 Bit Enables Register
MUX_CR3:      equ DBh          ; Analog Mux Port 3 Bit Enables Register

IO_CFG:       equ DCh  ; Input/Output Configuration Register
IO_CFG_REG_EN:          equ 02h  ; Regulator on Port 1 outputs
IO_CFG_IOINT:           equ 01h  ; GPIO interrupt mode

OUT_P1:       equ DDh  ; Output Override to Port 1 Register
OUT_P1_P16D:            equ 80h  ; Enable data Output to P1[6] when P16EN is high
OUT_P1_P16EN:           equ 40h  ; Enable pin P1[6] for output
OUT_P1_P14D:            equ 20h  ; Enable data Output to P1[4] when P14EN is high
OUT_P1_P14EN:           equ 10h  ; Enable pin P1[4] for output
OUT_P1_P12D:            equ 08h  ; Enable data Output to P1[2] when P12EN is high
OUT_P1_P12EN:           equ 04h  ; Enable pin P1[2] for output
OUT_P1_P10D:            equ 02h  ; Enable data Output to P1[0] when P10EN is high
OUT_P1_P10EN:           equ 01h  ; Enable pin P1[1] for output

;------------------------------------------------
;  Clock and System Control Registers
;------------------------------------------------

OSC_CR0:      equ E0h          ; System Oscillator Control Register 0     (RW)
OSC_CR0_Disable_Buzz:   equ 40h
OSC_CR0_No_Buzz:        equ 20h
OSC_CR0_SLEEP:          equ 18h
OSC_CR0_SLEEP_512Hz:    equ 00h  ; 1.95 ms (512 Hz)
OSC_CR0_SLEEP_64Hz:     equ 08h  ; 15.6 ms (64 Hz)
OSC_CR0_SLEEP_8Hz:      equ 10h  ; 125 ms (8 Hz)
OSC_CR0_SLEEP_1Hz:      equ 18h  ; 1 s (1 Hz)
OSC_CR0_CPU:            equ 07h  ; MASK: Set CPU Frequency
;                  IMO = 12 MHz
OSC_CR0_CPU_1d5MHz:        equ 00h  ; Set CPU Freq bits for 1.5 MHz Operation   
OSC_CR0_CPU_3MHz:          equ 01h  ; Set CPU Freq bits for 3 MHz Operation     
OSC_CR0_CPU_6MHz:          equ 02h  ; Set CPU Freq bits for 6 MHz Operation     
OSC_CR0_CPU_12MHz:         equ 03h  ; Set CPU Freq bits for 12 MHz Operation    
OSC_CR0_CPU_750kHz:        equ 04h  ; Set CPU Freq bits for 750 kHz Operation   
OSC_CR0_CPU_375kHz:        equ 05h  ; Set CPU Freq bits for 375 kHz Operation   
OSC_CR0_CPU_93d7kHz:       equ 06h  ; Set CPU Freq bits for 93.7 kHz Operation  
OSC_CR0_CPU_46d8kHz:       equ 07h  ; Set CPU Freq bits for 46.8 kHz Operation  
;                  IMO = 12 MHz
OSC_CR0_CPU_IMO12_1d5MHz:  equ 00h  ; Set CPU Freq bits for 1.5 MHz Operation
OSC_CR0_CPU_IMO12_3MHz:    equ 01h  ; Set CPU Freq bits for 3 MHz Operation
OSC_CR0_CPU_IMO12_6MHz:    equ 02h  ; Set CPU Freq bits for 6 MHz Operation
OSC_CR0_CPU_IMO12_12MHz:   equ 03h  ; Set CPU Freq bits for 12 MHz Operation
OSC_CR0_CPU_IMO12_750kHz:  equ 04h  ; Set CPU Freq bits for 750 kHz Operation
OSC_CR0_CPU_IMO12_375kHz:  equ 05h  ; Set CPU Freq bits for 375 kHz Operation
OSC_CR0_CPU_IMO12_93d7kHz: equ 06h  ; Set CPU Freq bits for 93.7 kHz Operation
OSC_CR0_CPU_IMO12_46d8kHz: equ 07h  ; Set CPU Freq bits for 46.8 kHz Operation
;                  IMO = 6 MHz
OSC_CR0_CPU_IMO6_750kHz:   equ 00h  ; Set CPU Freq bits for 750 kHz Operation
OSC_CR0_CPU_IMO6_1d5MHz:   equ 01h  ; Set CPU Freq bits for 1.5 MHz Operation
OSC_CR0_CPU_IMO6_3MHz:     equ 02h  ; Set CPU Freq bits for 3 MHz Operation
OSC_CR0_CPU_IMO6_6MHz:     equ 03h  ; Set CPU Freq bits for 6 MHz Operation
OSC_CR0_CPU_IMO6_375kHz:   equ 04h  ; Set CPU Freq bits for 375 kHz Operation
OSC_CR0_CPU_IMO6_187d5kHz: equ 05h  ; Set CPU Freq bits for 187.5 kHz Operation
OSC_CR0_CPU_IMO6_46d9kHz:  equ 06h  ; Set CPU Freq bits for 46.9 kHz Operation
OSC_CR0_CPU_IMO6_23d4kHz:  equ 07h  ; Set CPU Freq bits for 23.4 kHz Operation

OSC_CR2:      equ E2h          ; Oscillator Control Register 2            (RW)
OSC_CR2_EXTCLKEN:       equ 04h    ; MASK: Enable/Disable External Clock
OSC_CR2_IMODIS:         equ 02h    ; MASK: Enable/Disable System (IMO) Clock Net

VLT_CR:       equ E3h          ; Voltage Monitor Control Register         (RW)
VLT_CR_PORLEV:          equ 30h    ; MASK: Mask for Power on Reset level control
VLT_CR_POR_LOW:         equ 00h    ;   Lowest  Precision Power-on Reset trip point
VLT_CR_POR_MID:         equ 10h    ;   Middle  Precision Power-on Reset trip point
VLT_CR_POR_HIGH:        equ 20h    ;   Highest Precision Power-on Reset trip point
VLT_CR_LVDTBEN:         equ 08h    ; MASK: Enable the CPU Throttle Back on LVD
VLT_CR_VM:              equ 07h    ; MASK: Mask for Voltage Monitor level setting

VLT_CMP:      equ E4h          ; Voltage Monitor Comparators Register     (R)
VLT_CMP_NoWrite:        equ 08h    ; MASK: Vcc below Flash Write level
VLT_CMP_LVD:            equ 02h    ; MASK: Vcc below LVD trip level
VLT_CMP_PPOR:           equ 01h    ; MASK: Vcc below PPOR trip level

IMO_TR:       equ E8h          ; Internal Main Oscillator Trim Register   (W)

ILO_TR:       equ E9h          ; Internal Low-speed Oscillator Trim       (W)
ILO_TR_Bias_Trim:       equ C0h  ; (W)
ILO_TR_Freq_Trim:       equ 0Fh  ; (W)

BDG_TR:       equ EAh          ; Band Gap Trim Register                   (W)
BDG_TR_TC:              equ E0h  ; Temperature coefficient (RW)
BDG_TR_V:               equ 1Fh  ; Bandgap reference (RW)

SLP_CFG:      equ EBh  ; Sleep Configuration Register
SLP_CFG_PSSDC:          equ C0h  ; Sleep duty cycle (RW)

;;=============================================================================
;;      M8C System Macros
;;  These macros should be used when their functions are needed.
;;=============================================================================

;----------------------------------------------------
;  Swapping Register Banks
;----------------------------------------------------
    macro M8C_SetBank0
    and   F, ~FLAG_XIO_MASK
    endm

    macro M8C_SetBank1
    or    F, FLAG_XIO_MASK
    endm

;----------------------------------------------------
;  Global Interrupt Enable/Disable
;----------------------------------------------------
    macro M8C_EnableGInt
    or    F, FLAG_GLOBAL_IE
    endm

    macro M8C_DisableGInt
    and   F, ~FLAG_GLOBAL_IE
    endm

;----------------------------------------------------
;  Enable/Disable Interrupt Mask
;
;  Use the following macros to enable/disable
;  bits in the Interrupt mask registers,
;  INT_MSK0, INT_MSK1 or INT_MSK3.
;
;  Usage:    M8C_DisableIntMask INT_MSKN, MASK
;            M8C_EnableIntMask  INT_MSKN, MASK
;
;  where INT_MSKN is INT_MSK0, INT_MSK1 or INT_MSK3
;        and MASK is the bit set to enable or disable
;----------------------------------------------------
; Disable Interrupt Bit Mask(s)
    macro M8C_DisableIntMask
    and   reg[@0], ~@1              ; disable specified interrupt enable bit
    endm

; Enable Interrupt Bit Mask(s)
    macro M8C_EnableIntMask
    or    reg[@0], @1               ; enable specified interrupt enable bit
    endm

;----------------------------------------------------
;  Clear Posted Interrupt Flag Mask
;
;  Use the following macros to clear the
;  bits in the Interrupt Clear registers,
;  INT_CLR0, INT_CLR1 or INT_CLR3.
;  Usage:    M8C_ClearIntFlag INT_CLRN, MASK
;
;  where INT_MSKN is INT_CLR0, INT_CLR1 or INT_CLR3
;        and MASK is the bit set to enable or disable
;----------------------------------------------------
    macro M8C_ClearIntFlag
    mov   reg[@0], ~@1              ; clear specified interrupt enable bit
    endm

;----------------------------------------------------
;  Power-On Reset & WatchDog Timer Functions
;----------------------------------------------------
    macro M8C_EnableWatchDog
    and   reg[CPU_SCR0], ~CPU_SCR0_PORS
    endm

    macro M8C_ClearWDT
    mov   reg[RES_WDT], 00h
    endm

    macro M8C_ClearWDTAndSleep
    mov   reg[RES_WDT], 38h
    endm

;----------------------------------------------------
;  Sleep, CPU Stop & Software Reset
;----------------------------------------------------
    macro M8C_Sleep
    or    reg[CPU_SCR0], CPU_SCR0_Sleep
    ; The next instruction to be executed depends on the state of the
    ; various interrupt enable bits. If some interrupts are enabled
    ; and the global interrupts are disabled, the next instruction will
    ; be the one that follows the invocation of this macro. If global
    ; interrupts are also enabled then the next instruction will be
    ; from the interrupt vector table. If no interrupts are enabled
    ; then the CPU sleeps forever.
    endm

    macro M8C_Stop
    ; In general, you probably don't want to do this, but here's how:
    or    reg[CPU_SCR0], CPU_SCR0_STOP
    ; Next instruction to be executed is located in the interrupt
    ; vector table entry for Power-On Reset.
    endm

    macro M8C_Reset
    ; Restore CPU to the power-on reset state.
    mov A, 0
    SSC
    ; Next non-supervisor instruction will be at interrupt vector 0.
    endm

;----------------------------------------------------
; ImageCraft Code Compressor Actions
;----------------------------------------------------
    ; Suspend Code Compressor
    ; Must not span a RET or RETI instruction
    ; without resuming code compression
    macro Suspend_CodeCompressor
    or   F, 0
    endm

    ; Resume Code Compression
    macro Resume_CodeCompressor
    add  SP, 0
    endm

; end of file m8c.inc
