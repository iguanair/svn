//=============================================================================
//
//    m8c.h: M8C20xxx Microcontroller Device System Declarations
//
//    Copyright: Cypress MicroSystems 2005. All Rights Reserved.
//
//
//    This file provides address constants, bit field masks and a set of macro
//    facilities for the Cypress MicroSystems 20xxx Microcontroller families.
//
//    Last Modified: January 13, 2006
//
//=============================================================================

#ifndef M8C_C_HEADER
#define M8C_C_HEADER

//-----------------------------------------------
// Define System Types
//-----------------------------------------------
typedef  unsigned char  BOOL;
typedef  unsigned char  BYTE;
typedef  signed   char  CHAR;
typedef  unsigned int   WORD;
typedef  signed   int   INT;
typedef  unsigned long  DWORD;
typedef  signed   long  LONG;

//-----------------------------------------------
// Define Boolean TRUE/FALSE
//-----------------------------------------------
#define  TRUE  ((BOOL) 1)
#define  FALSE ((BOOL) 0)


//=============================================================================
//=============================================================================
//      System Registers
//=============================================================================
//=============================================================================

//-----------------------------------------------
//  Flag Register Bit Fields
//-----------------------------------------------
#define  FLAG_PGMODE_MASK  (0xC0)
#define  FLAG_PGMODE_0     (0x00)
#define  FLAG_PGMODE_1     (0x40)
#define  FLAG_PGMODE_2     (0x80)
#define  FLAG_PGMODE_3     (0xC0)
#define  FLAG_PGMODE_00b   (0x00)
#define  FLAG_PGMODE_01b   (0x40)
#define  FLAG_PGMODE_10b   (0x80)
#define  FLAG_PGMODE_11b   (0xC0)
#define  FLAG_XIO_MASK     (0x10)
#define  FLAG_SUPER        (0x08)
#define  FLAG_CARRY        (0x04)
#define  FLAG_ZERO         (0x02)
#define  FLAG_GLOBAL_IE    (0x01)


//=============================================================================
//=============================================================================
//      Register Space, Bank 0
//=============================================================================
//=============================================================================

//-----------------------------------------------
//  Port Registers
//  Note: Also see this address range in Bank 1.
//-----------------------------------------------
// Port 0
#pragma  ioport   PRT0DR:     0x000  // Port 0 Data Register (RW)
BYTE              PRT0DR;
#pragma  ioport   PRT0IE:     0x001  // Port 0 Interrupt Enable Register (RW)
BYTE              PRT0IE;
// Port 1
#pragma  ioport   PRT1DR:     0x004  // Port 1 Data Register (RW)
BYTE              PRT1DR;
#pragma  ioport   PRT1IE:     0x005  // Port 1 Interrupt Enable Register (RW)
BYTE              PRT1IE;
// Port 2
#pragma  ioport   PRT2DR:     0x008  // Port 2 Data Register (RW)
BYTE              PRT2DR;
#pragma  ioport   PRT2IE:     0x009  // Port 2 Interrupt Enable Register (RW)
BYTE              PRT2IE;
// Port 3
#pragma  ioport   PRT3DR:     0x00C  // Port 3 Data Register (RW)
BYTE              PRT3DR;
#pragma  ioport   PRT3IE:     0x00D  // Port 3 Interrupt Enable Register (RW)
BYTE              PRT3IE;

//-----------------------------------------------
//  SPI Registers
//  Note: Also see this address range in Bank 1.
//-----------------------------------------------

#pragma  ioport   SPI_TXR:    0x029  // SPI Transmit Data Register (W)
BYTE              SPI_TXR;
#pragma  ioport   SPI_RXR:    0x02A  // SPI Receive Data Register (R)
BYTE              SPI_RXR;
#pragma  ioport   SPI_CR:     0x02B  // SPI Control Register
BYTE              SPI_CR;
#define  SPI_CR_LSb_First         (0x80)  // (RW)
#define  SPI_CR_Overrun           (0x40)  // (R)
#define  SPI_CR_SPI_Complete      (0x20)  // (R)
#define  SPI_CR_TX_Reg_Empty      (0x10)  // (R)
#define  SPI_CR_RX_Reg_Full       (0x08)  // (R)
#define  SPI_CR_Clock_Phase       (0x04)  // (RW)
#define  SPI_CR_Clock_Polarity    (0x02)  // (RW)
#define  SPI_CR_Enable            (0x01)  // (RW)


//-----------------------------------------------
//  Analog Control Registers
//-----------------------------------------------

#pragma  ioport   AMUXCFG:    0x061  // Analog MUX Configuration
BYTE              AMUXCFG;
#define  AMUXCFG_ICAPEN           (0xC0)  // Internal capacitance to the analog global bus (RW)
#define  AMUXCFG_INTCAP           (0x03)  // External integration capacitor in the charge integration mode (RW)


//-----------------------------------------------
//  Global General Purpose Data Registers
//-----------------------------------------------
#pragma  ioport   TMP_DR0:    0x06C  // Temporary Data Register 0 (RW)
BYTE              TMP_DR0;              
#pragma  ioport   TMP_DR1:    0x06D  // Temporary Data Register 1 (RW)
BYTE              TMP_DR1;              
#pragma  ioport   TMP_DR2:    0x06E  // Temporary Data Register 2 (RW)
BYTE              TMP_DR2;
#pragma  ioport   TMP_DR3:    0x06F  // Temporary Data Register 3 (RW)
BYTE              TMP_DR3;


//------------------------------------------------------------------------------
//  Analog Comparator registers
//------------------------------------------------------------------------------

#pragma  ioport   CMP_RDC:    0x078  // Comparator Read/Clear Register 
BYTE              CMP_RDC;
#define  CMP_RDC_CMP1D            (0x20) //MASK: Dynamic status of comparator 1    (R)
#define  CMP_RDC_CMP0D            (0x10) //MASK: Dynamic status of comparator 0    (R)
#define  CMP_RDC_CMP1L            (0x02) //MASK: Latched status of comparator 1    (RC)
#define  CMP_RDC_CMP0L            (0x01) //MASK: Latched status of comparator 0    (RC)
                                      
#pragma  ioport   CMP_MUX:    0x079  // Comparator Multiplexor Register 0        (RW)
BYTE              CMP_MUX;
#define  CMP_MUX_INP1             (0xC0) //MASK: Comparator 1 Positive Terminal Input 
#define  CMP_MUX_INN1             (0x30) //MASK: Comparator 1 Negative Terminal Input
#define  CMP_MUX_INP0             (0x0C) //MASK: Comparator 0 Positive Terminal Input 
#define  CMP_MUX_INN0             (0x03) //MASK: Comparator 0 Negative Terminal Input

#pragma  ioport   CMP_CR0:    0x07A //Comparator Control Register 0             (RW)
BYTE              CMP_CR0;            
#define  CMP_CR0_CMP1_Range       (0x20) //MASK: Comparator 1 Range 
#define  CMP_CR0_CMP1EN           (0x10) //MASK: Comparator 1 Enable 
#define  CMP_CR0_CMP0_Range       (0x02) //MASK: Comparator 0 Range 
#define  CMP_CR0_CMP0EN           (0x01) //MASK: Comparator 0 Enable

#pragma  ioport   CMP_CR1:    0x07B //Comparator Control Register 1             (RW)
BYTE              CMP_CR1;            
#define  CMP_CR1_CINT1            (0x80)  // Comparator 1 select for input to the analog interrupt
#define  CMP_CR1_CPIN1            (0x40)  // Comparator 1 signal for possible connection to GPIO pin
#define  CMP_CR1_CRST1            (0x20)  // Comparator 1 latch reset
#define  CMP_CR1_CDS1             (0x10)  // Data output select for the comparator 1 channel
#define  CMP_CR1_CINT0            (0x08)  // Comparator 0 select for input to the analog interrupt
#define  CMP_CR1_CPIN0            (0x04)  // Comparator 0 signal for possible connection to GPIO pin
#define  CMP_CR1_CRST0            (0x02)  // Comparator 0 latch reset                               
#define  CMP_CR1_CDS0             (0x01)  // Data output select for the comparator 0 channel        

#pragma  ioport   CMP_LUT:    0x07C //Comparator LUT Register
BYTE              CMP_LUT;           
#define  CMP_LUT_LUT0             (0xF0) //MASK: Enable Comparator to Drive GlobalOut Odd, bit 5 (RW)
#define  CMP_LUT_LUT1             (0x0F) //MASK: Enable Comparator to Drive GlobalOut Odd, bit 1 (RW)
                                      
//-----------------------------------------------
//  Capsense Registers
//-----------------------------------------------

#pragma  ioport   CS_CR0:     0x0A0  // Capsense Control Register 0               (RW)
BYTE              CS_CR0;
#define  CS_CR0_CSOUT             (0xC0) //MASK: Capsense Output 
#define  CS_CR0_MODE              (0x06) //MASK: Capsense Counter Mode
#define  CS_CR0_EN                (0x01) //MASK: Counter Enable

#pragma  ioport   CS_CR1:     0x0A1  // Capsense Control Register 1               (RW)
BYTE              CS_CR1;
#define  CS_CR1_CHAIN             (0x80) //MASK: Counter Chain Control 
#define  CS_CR1_CLKSEL            (0x60) //MASK: Capsense Clock Selection
#define  CS_CR1_RLOCLK            (0x10) //MASK: Relaxation Oscillator Clock Select
#define  CS_CR1_INV               (0x08) //MASK: Input Invert   
#define  CS_CR1_INSEL             (0x06) //MASK: Input Selection

#pragma  ioport   CS_CR2:     0x0A2  // Capsense Control Register 2               (RW)
BYTE              CS_CR2;
#define  CS_CR2_IRANGE            (0xC0) //MASK: IDAC current output 
#define  CS_CR2_IDACDIR           (0x20) //MASK: Source/sink state of the IDAC
#define  CS_CR2_IDAC_EN           (0x10) //MASK: Manual connection of the IDAC to the analog global bus enable
#define  CS_CR2_PXD_EN            (0x04) //MASK: Ground and analog global bus switch   
#define  CS_CR2_CI_EN             (0x02) //MASK: Charge integration enable
#define  CS_CR2_RO_EN             (0x01) //MASK: Relaxation oscillator enable

#pragma  ioport   CS_CR3:     0x0A3  // Capsense Control Register 3               (RW)
BYTE              CS_CR3;
#define  CS_CR3_REFMUX            (0x40) //MASK: Input voltage of the reference buffer 
#define  CS_CR3_REFMODE           (0x20) //MASK: Manual connection of the reference buffer output to the analog global bus enable
#define  CS_CR3_REF_EN            (0x10) //MASK: Drive the analog global bus by reference buffer enable
#define  CS_CR3_LPFilt            (0x0C) //MASK: Low pass filter approximate time   
#define  CS_CR3_LPF_EN            (0x03) //MASK: Low pass filter enable

#pragma  ioport   CS_CNTL:    0x0A4  // Capsense Counter Low Byte Register               (RO)
BYTE              CS_CNTL;
#pragma  ioport   CS_CNTH:    0x0A5  // Capsense Counter High Byte Register              (RO)
BYTE              CS_CNTH;

#pragma  ioport   CS_STAT:    0x0A6  // CapSense Status Register              
BYTE              CS_STAT;
#define  CS_STAT_INS              (0x80)  // Input Status                                  (RC)
#define  CS_STAT_COLS             (0x40)  // Counter Carry Out Low Status                  (RC)
#define  CS_STAT_COHS             (0x20)  // Counter Carry Out High Status                 (RC)
#define  CS_STAT_PPS              (0x10)  // Pulse Width/Period Measurement Status         (RC)
#define  CS_STAT_INM              (0x08)  // Input Interrupt/Mask                          (RW)
#define  CS_STAT_COLM             (0x04)  // Counter Carry Out Low Interrupt Mask          (RW)
#define  CS_STAT_COHM             (0x02)  // Counter Carry Out High Interrupt Mask         (RW)
#define  CS_STAT_PPM              (0x01)  // Pulse Width/Period Measurement Interrupt Mask (RW)

//-----------------------------------------------
//  Programmable Timer Registers
//-----------------------------------------------

#pragma  ioport   PT_CFG:     0x0B0  // Programmable Timer Configuration Register
BYTE              PT_CFG;
#define  PT_CFG_One_Shot          (0x02)  // One-shot mode (One complete count then stops) (RW)
#define  PT_CFG_START             (0x01)  // Timer Start (RW)

#pragma  ioport   PT_DATA1:   0x0B1  // Programmable Timer Data Register 1  (RW)
BYTE              PT_DATA1;

#pragma  ioport   PT_DATA0:   0x0B2  // Programmable Timer Data Register 0  (RW)
BYTE              PT_DATA0;

//-----------------------------------------------
//  Ram Page Pointers
//-----------------------------------------------
#pragma  ioport   CUR_PP:     0x0D0  // Current   Page Pointer
BYTE              CUR_PP;
#define  CUR_PP_Page_Bits         (0x07)  // SRAM Page for generic SRAM access (RW)

#pragma  ioport   STK_PP:     0x0D1  // Stack     Page Pointer
BYTE              STK_PP;
#define  STK_PP_Page_Bits         (0x07)  // SRAM Page to hold the stack (RW)

#pragma  ioport   IDX_PP:     0x0D3  // Index     Page Pointer
BYTE              IDX_PP;
#define  IDX_PP_Page_Bits         (0x07)  // SRAM Page for operating indexed memory access (RW)

#pragma  ioport   MVR_PP:     0x0D4  // MVI Read  Page Pointer
BYTE              MVR_PP;
#define  MVR_PP_Page_Bits         (0x07)  // SRAM Page for operating MVI Read instructions (RW)

#pragma  ioport   MVW_PP:     0x0D5  // MVI Write Page Pointer
BYTE              MVW_PP;
#define  MVW_PP_Page_Bits         (0x07)  // SRAM Page for operating MVI Write instructions (RW)

//-----------------------------------------------
//  I2C Configuration Registers
//-----------------------------------------------
#pragma  ioport   I2C_CFG:    0x0D6  // I2C Configuration Register  (RW)
BYTE              I2C_CFG;
#define  I2C_CFG_PSelect          (0x40)
#define  I2C_CFG_Stop_IE          (0x10)
#define  I2C_CFG_Clock_Rate_100K  (0x00)
#define  I2C_CFG_Clock_Rate_400K  (0x04)
#define  I2C_CFG_Clock_Rate_50K   (0x08)
#define  I2C_CFG_Clock_Rate       (0x0C)
#define  I2C_CFG_Enable           (0x01)

#pragma  ioport   I2C_SCR:    0x0D7  // I2C Status and Control Register
BYTE              I2C_SCR;
#define  I2C_SCR_Bus_Error        (0x80)  // (RC)
#define  I2C_SCR_Stop_Status      (0x20)  // (RC)
#define  I2C_SCR_ACK              (0x10)  // (RW)
#define  I2C_SCR_Address          (0x08)  // (RC)
#define  I2C_SCR_Transmit         (0x04)  // (RW)
#define  I2C_SCR_LRB              (0x02)  // (RC)
#define  I2C_SCR_Byte_Complete    (0x01)  // (RC)

#pragma  ioport   I2C_DR:     0x0D8  // I2C Data Register (RW)
BYTE              I2C_DR;


//-----------------------------------------------
//  System and Global Resource Registers
//-----------------------------------------------
#pragma  ioport   INT_CLR0:   0x0DA  // Interrupt Clear Register 0 (RW)
BYTE              INT_CLR0;
                                  // Use INT_MSK0 bit field masks

#pragma  ioport   INT_MSK0:   0x0E0  // General Interrupt Mask Register (RW)
BYTE              INT_MSK0;
#define  INT_MSK0_I2C             (0x80)
#define  INT_MSK0_Sleep           (0x40)
#define  INT_MSK0_SPI             (0x20)
#define  INT_MSK0_GPIO            (0x10)
#define  INT_MSK0_Timer           (0x08)
#define  INT_MSK0_Capsense        (0x04)
#define  INT_MSK0_Analog          (0x02)
#define  INT_MSK0_V_Monitor       (0x01)

#pragma  ioport   INT_SW_EN:  0x0E1  // Interrupt Software Enable Register
BYTE              INT_SW_EN;
#define  INT_SW_EN_ENSWINT           (0x01)  // (RW)

#pragma  ioport   INT_VC:     0x0E2  // Interrupt vector clear register (RC)
BYTE              INT_VC;      
#pragma  ioport   RES_WDT:    0x0E3  // Watch Dog Timer
BYTE              RES_WDT;

//-----------------------------------------------
//  System Status and Control Register
//
//  Note: the following register is mapped into
//  both register bank 0 AND register bank 1.
//-----------------------------------------------
#pragma  ioport   IDAC_D:     0x0FD   // DAC Data Register
BYTE              IDAC_D;

#pragma  ioport   CPU_SCR1:   0x0FE   // System Status and Control Register 1
BYTE              CPU_SCR1;
#define  CPU_SCR1_IRESS           (0x80)  // (R)
#define  CPU_SCR1_SLIMO           (0x10)  // (RW)
#define  CPU_SCR1_IRAMDIS         (0x01)  // (RW)

#pragma  ioport   CPU_SCR0:   0x0FF  // System Status and Control Register 0
BYTE              CPU_SCR0;
#define  CPU_SCR0_GIES            (0x80)  // Global interrupt enable status (R)
#define  CPU_SCR0_WDRS            (0x20)  // Watchdog Reset Status (RC)
#define  CPU_SCR0_PORS            (0x10)  // Power On Reset Status (RC)
#define  CPU_SCR0_Sleep           (0x08)  // CPU sleep enable (RW)
#define  CPU_SCR0_STOP            (0x01)  // M8C stop M8C code execution halt (RW)


//=============================================================================
//=============================================================================
//      Register Space, Bank 1
//=============================================================================
//=============================================================================


//-----------------------------------------------
//  Port Registers
//  Note: Also see this address range in Bank 0.
//-----------------------------------------------
// Port 0
#pragma  ioport   PRT0DM0:    0x100  // Port 0 Drive Mode 0
BYTE              PRT0DM0;
#pragma  ioport   PRT0DM1:    0x101  // Port 0 Drive Mode 1
BYTE              PRT0DM1;

// Port 1
#pragma  ioport   PRT1DM0:    0x104  // Port 1 Drive Mode 0
BYTE              PRT1DM0;
#pragma  ioport   PRT1DM1:    0x105  // Port 1 Drive Mode 1
BYTE              PRT1DM1;

// Port 2
#pragma  ioport   PRT2DM0:    0x108  // Port 2 Drive Mode 0
BYTE              PRT2DM0;
#pragma  ioport   PRT2DM1:    0x109  // Port 2 Drive Mode 1
BYTE              PRT2DM1;

// Port 3
#pragma  ioport   PRT3DM0:    0x10C  // Port 3 Drive Mode 0
BYTE              PRT3DM0;
#pragma  ioport   PRT3DM1:    0x10D  // Port 3 Drive Mode 1
BYTE              PRT3DM1;

//-----------------------------------------------
//  SPI Registers
//  Note: Also see this address range in Bank 0.
//-----------------------------------------------


#pragma  ioport   SPI_CFG:    0x129  //    SPI Configuration Register
BYTE              SPI_CFG;
#define  SPI_CFG_Clock_Sel        (0xE0)  // SYSCLK in Master mode
#define  SPI_CFG_Bypass           (0x10)  // Bypass Synchronization
#define  SPI_CFG_SS               (0x08)  // Slave Select in Slave mode
#define  SPI_CFG_SS_EN            (0x04)  // Internal Slave Select Enable
#define  SPI_CFG_Int_Sel          (0x02)  // Interrupt Select
#define  SPI_CFG_Slave            (0x01)  // Slave Mode enable

//------------------------------------------------
//  Analog Mux Bus Port Enable Bits and I/O Registers
//------------------------------------------------

#pragma  ioport   MUX_CR0:    0x1D8  // Analog Mux Port 0 Bit Enables
BYTE              MUX_CR0;
#pragma  ioport   MUX_CR1:    0x1D9  // Analog Mux Port 1 Bit Enables
BYTE              MUX_CR1;		  
#pragma  ioport   MUX_CR2:    0x1DA  // Analog Mux Port 2 Bit Enables
BYTE              MUX_CR2;
#pragma  ioport   MUX_CR3:    0x1DB  // Analog Mux Port 3 Bit Enables
BYTE              MUX_CR3;

#pragma  ioport   IO_CFG:     0x1DC  // Input/Output Configuration Register
BYTE              IO_CFG;
#define  IO_CFG_REG_EN            (0x02)  // Regulator on Port 1 outputs
#define  IO_CFG_IOINT             (0x01)  // GPIO interrupt mode

#pragma  ioport   OUT_P1:     0x1DD  // Output Override to Port 1 Register
BYTE              OUT_P1;
#define  OUT_P1_P16D              (0x80)  // Enable data Output to P1[6] when P16EN is high
#define  OUT_P1_P16EN             (0x40)  // Enable pin P1[6] for output
#define  OUT_P1_P14D              (0x20)  // Enable data Output to P1[4] when P14EN is high
#define  OUT_P1_P14EN             (0x10)  // Enable pin P1[4] for output
#define  OUT_P1_P12D              (0x08)  // Enable data Output to P1[2] when P12EN is high
#define  OUT_P1_P12EN             (0x04)  // Enable pin P1[2] for output
#define  OUT_P1_P10D              (0x02)  // Enable data Output to P1[0] when P10EN is high
#define  OUT_P1_P10EN             (0x01)  // Enable pin P1[1] for output
                           
//------------------------------------------------
//  Clock and System Control Registers
//------------------------------------------------

#pragma  ioport   OSC_CR0:    0x1E0  // System Oscillator Control Register 0
BYTE              OSC_CR0;
#define  OSC_CR0_Disable_Buzz      (0x40)
#define  OSC_CR0_No_Buzz           (0x20)
#define  OSC_CR0_SLEEP             (0x18)
#define  OSC_CR0_SLEEP_512Hz       (0x00)  // 1.95 ms (512 Hz)
#define  OSC_CR0_SLEEP_64Hz        (0x08)  // 15.6 ms (64 Hz)
#define  OSC_CR0_SLEEP_8Hz         (0x10)  // 125 ms (8 Hz)
#define  OSC_CR0_SLEEP_1Hz         (0x18)  // 1 s (1 Hz)
//                  IMO = 12 MHz
#define  OSC_CR0_CPU_1d5MHz        (0x00)  // Set CPU Freq bits for 1.5 MHz Operation   
#define  OSC_CR0_CPU_3MHz          (0x01)  // Set CPU Freq bits for 3 MHz Operation     
#define  OSC_CR0_CPU_6MHz          (0x02)  // Set CPU Freq bits for 6 MHz Operation     
#define  OSC_CR0_CPU_12MHz         (0x03)  // Set CPU Freq bits for 12 MHz Operation    
#define  OSC_CR0_CPU_750kHz        (0x04)  // Set CPU Freq bits for 750 kHz Operation   
#define  OSC_CR0_CPU_375kHz        (0x05)  // Set CPU Freq bits for 375 kHz Operation   
#define  OSC_CR0_CPU_93d7kHz       (0x06)  // Set CPU Freq bits for 93.7 kHz Operation  
#define  OSC_CR0_CPU_46d8kHz       (0x07)  // Set CPU Freq bits for 46.8 kHz Operation  
//                  IMO = 12 MHz
#define  OSC_CR0_CPU_IMO12_1d5MHz  (0x00)  // Set CPU Freq bits for 1.5 MHz Operation
#define  OSC_CR0_CPU_IMO12_3MHz    (0x01)  // Set CPU Freq bits for 3 MHz Operation
#define  OSC_CR0_CPU_IMO12_6MHz    (0x02)  // Set CPU Freq bits for 6 MHz Operation
#define  OSC_CR0_CPU_IMO12_12MHz   (0x03)  // Set CPU Freq bits for 12 MHz Operation
#define  OSC_CR0_CPU_IMO12_750kHz  (0x04)  // Set CPU Freq bits for 750 kHz Operation
#define  OSC_CR0_CPU_IMO12_375kHz  (0x05)  // Set CPU Freq bits for 375 kHz Operation
#define  OSC_CR0_CPU_IMO12_93d7kHz (0x06)  // Set CPU Freq bits for 93.7 kHz Operation
#define  OSC_CR0_CPU_IMO12_46d8kHz (0x07)  // Set CPU Freq bits for 46.8 kHz Operation
//                  IMO = 6 MHz
#define  OSC_CR0_CPU_IMO6_750kHz   (0x00)  // Set CPU Freq bits for 750 kHz Operation
#define  OSC_CR0_CPU_IMO6_1d5MHz   (0x01)  // Set CPU Freq bits for 1.5 MHz Operation
#define  OSC_CR0_CPU_IMO6_3MHz     (0x02)  // Set CPU Freq bits for 3 MHz Operation
#define  OSC_CR0_CPU_IMO6_6MHz     (0x03)  // Set CPU Freq bits for 6 MHz Operation
#define  OSC_CR0_CPU_IMO6_375kHz   (0x04)  // Set CPU Freq bits for 375 kHz Operation
#define  OSC_CR0_CPU_IMO6_187d5kHz (0x05)  // Set CPU Freq bits for 187.5 kHz Operation
#define  OSC_CR0_CPU_IMO6_46d9kHz  (0x06)  // Set CPU Freq bits for 46.9 kHz Operation
#define  OSC_CR0_CPU_IMO6_23d4kHz  (0x07)  // Set CPU Freq bits for 23.4 kHz Operation

#pragma  ioport   OSC_CR2:    0x1E2  // Oscillator Control Register 2
BYTE              OSC_CR2;
#define  OSC_CR2_EXTCLKEN         (0x04)  // External clock mode enable
#define  OSC_CR2_IMODIS           (0x02)  // Internal oscillator disable

#pragma  ioport   VLT_CR:     0x1E3  // Voltage Monitor Control Register
BYTE              VLT_CR;
#define  VLT_CR_PORLEV            (0x30)  //MASK: Mask for Power on Reset level control  
#define  VLT_CR_POR_LOW           (0x00)  //  Lowest  Precision Power-on Reset trip point
#define  VLT_CR_POR_MID           (0x10)  //  Middle  Precision Power-on Reset trip point
#define  VLT_CR_POR_HIGH          (0x20)  //  Highest Precision Power-on Reset trip point
#define  VLT_CR_LVDTBEN           (0x08)  //MASK: Enable the CPU Throttle Back on LVD    
#define  VLT_CR_VM                (0x07)  //MASK: Mask for Voltage Monitor level setting 

#pragma  ioport   VLT_CMP:    0x1E4  // Voltage Monitor Comparators Register
BYTE              VLT_CMP;
#define  VLT_CMP_NoWrite          (0x08)  // MASK: Vcc below Flash Write level (R)
#define  VLT_CMP_LVD              (0x02)  // MASK: Vcc below LVD trip level    (R)
#define  VLT_CMP_PPOR             (0x01)  // MASK: Vcc below PPOR trip level   (R)

#pragma  ioport   IMO_TR:     0x1E8  // Internal Main Oscillator Trim Register
BYTE              IMO_TR;
#pragma  ioport   ILO_TR:     0x1E9  // Internal Low-speed Oscillator Trim
BYTE              ILO_TR;
#define  ILO_TR_Bias_Trim         (0xC0)  // (W)
#define  ILO_TR_Freq_Trim         (0x0F)  // (W)

#pragma  ioport   BDG_TR:     0x1EA  // Band Gap Trim Register
BYTE              BDG_TR;
#define  BDG_TR_TC                (0xE0)  // Temperature coefficient (RW)
#define  BDG_TR_V                 (0x1F)  // Bandgap reference (RW)

#pragma  ioport   SLP_CFG:    0x1EB  // Sleep Configuration Register
BYTE              SLP_CFG;
#define  SLP_CFG_PSSDC            (0xC0)  // Sleep duty cycle (RW)


//=============================================================================
//=============================================================================
//      M8C System Macros
//=============================================================================
//=============================================================================


//-----------------------------------------------
//  Swapping Register Banks
//-----------------------------------------------
#define  M8C_SetBank0            asm("and F, EFh")
#define  M8C_SetBank1            asm("or  F, 10h")

//-----------------------------------------------
//  Global Interrupt Enable/Disable
//-----------------------------------------------
#define  M8C_EnableGInt          asm("or  F, 01h")
#define  M8C_DisableGInt         asm("and F, FEh")

//---------------------------------------------------
// Enable/Disable Interrupt Mask
//
// Usage:    M8C_DisableIntMask INT_MSKN, MASK
//           M8C_EnableIntMask  INT_MSKN, MASK
//
// where INT_MSKN is INT_MSK0, INT_MSK1 or INT_MSK3
//       and MASK is the bit set to enable or disable
//---------------------------------------------------
// Disable Interrupt Bit Mask(s)
#define  M8C_DisableIntMask( INT_MSKN_REG, MASK )   (INT_MSKN_REG &= ~MASK)

// Enable Interrupt Bit Mask(s)
#define  M8C_EnableIntMask( INT_MSKN_REG, MASK )    (INT_MSKN_REG |= MASK)

//---------------------------------------------------
// Clear Posted Interrupt Flag
//
// Usage:    M8C_ClearIntFlag   INT_CLRN, MASK
//
// where INT_MSKN is INT_CLR0, INT_CLR1 or INT_CLR3
//       and MASK is the bit set to enable or disable
//---------------------------------------------------
#define  M8C_ClearIntFlag( INT_CLRN_REG, MASK )    (INT_CLRN_REG = ~MASK)


//-----------------------------------------------
//  Power-On Reset & WatchDog Timer Functions
//-----------------------------------------------
#define  M8C_EnableWatchDog      (CPU_SCR0 &= ~CPU_SCR0_PORS)
#define  M8C_ClearWDT            (RES_WDT = 0x00)
#define  M8C_ClearWDTAndSleep    (RES_WDT = 0x38)


//-----------------------------------------------
//  Sleep, CPU Stop & Software Reset
//-----------------------------------------------
#define  M8C_Sleep               (CPU_SCR0 |= CPU_SCR0_Sleep)
#define  M8C_Stop                (CPU_SCR0 |= CPU_SCR0_STOP)

#define  M8C_Reset               asm("mov A, 0\nSSC");


//-----------------------------------------------
// ImageCraft Code Compressor Actions
//-----------------------------------------------

    // Suspend Code Compressor
    // Must not span a RET or RETI instruction
    // without resuming code compression

#define  Suspend_CodeCompressor  asm("or F, 0")

    // Resume Code Compressor
#define  Resume_CodeCompressor   asm("add SP,0")

#endif


// end of file m8c.h
