/******************************************************************************
*   FILENAME:   FlashBlock.h
*   @Version@
*******************************************************************************
*   DESCRIPTION:
*   Flash Block programming header file.
*
*   NOTE: Calling any function in this library will result in the permanent
*   use of RAM address locations 0xF8 - 0xFF.  This area is at the top
*   of the stack and should only be a concern if stack space is at a
*   premium.
*******************************************************************************
*   Copyright (C) Cypress Semiconductor 2005. All rights reserved.
******************************************************************************/
#include <m8c.h>

/******************************************/
/* Call routines using Fastcall mechanism */
/******************************************/
#pragma fastcall16 bFlashWriteBlock
#pragma fastcall16 FlashReadBlock


/*****************************************************************************
*   FUNCTION NAME: bFlashWriteBlock
*
*   DESCRIPTION:
*   Writes 64 bytes of data to the flash at the specified blockId.
*
*   Regardless of the size of the buffer, this routine always writes 64
*   bytes of data. If the buffer is less than 64 bytes, then the next
*   64-N bytes of data will be written to fill the rest of flash block data.
*
;     TIMING NOTE:
;            The CPUCLK source and speed select is changed for 12 Mhz operation
;            from the internal osc for flash programming.  The clock is restored
;            to its previous setting upon conclusion of the write operation.
;
*
*   ARGUMENTS:
*   FLASH_WRITE_STRUC *  pFlashWriteData
*
*   pointer to a structure that holds the calling arguments and some
*   reserved space for temporary local variables.
*
*   RETURNS:
*   BYTE - successful if NON-Zero returned.
*           Causes for Error are:  
*           1) Protection bits not set properly
*           3) Incorrect Temperature table version 
*
*   SIDE EFFECTS:
*   1) CPU clock temporarily set to 12MHz or 6MHz depending upon SLIMO setting.
*
*******************************************************************************/
// FlashWrite argument/data structure
typedef struct
{
   BYTE           bARG_BlockId;           // block ID
   BYTE  *        pARG_FlashBuffer;       // flash buffer pointer - 2 bytes
   CHAR           cARG_Temperature;       // die Temperature, -40 to 100
   BYTE           bDATA_PWErase;          // Temporary storage (reserved)
   BYTE           bDATA_PWProgram;        // Temporary storage (reserved)
   BYTE           bDATA_PWMultiplier;     // Temporary storage (reserved)
}
FLASH_WRITE_STRUCT;

extern BYTE bFlashWriteBlock( FLASH_WRITE_STRUCT * pFlashWriteData );

/******************************************************************************
*   FUNCTION NAME: FlashReadBlock
*
*   DESCRIPTION:
*   Reads a specified flash block to a buffer in RAM.
*
*   ARGUMENTS:
*   FLASH_READ_STRUC *  pFlashReadData
*
*   pointer to a structure that holds the calling arguments.
*
*   RETURNS: void
*   Data read is returned at specified pFlashBuffer.
*
*   SIDE EFFECTS:
*   none.
*
*******************************************************************************/
typedef struct
{
   BYTE           bARG_BlockId;        // BYTE block ID
   BYTE *         pARG_FlashBuffer;    // flash buffer pointer - 2 bytes
   BYTE           bARG_ReadCount;      // BYTE Read count
}
FLASH_READ_STRUCT;

extern void FlashReadBlock( FLASH_READ_STRUCT * );

// End of Flashblock.h
