;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  memory.inc -- CY7C63000 Microcontroller Family Memory Declarations
;;;
;;;  Copyright (c) 2004 Cypress Semiconductor, Inc. All rights reserved.
;;;
;;;
;;;  This file provides memory address constants 
;;;
;;;  Last Modified: Mar 22, 2004
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;