;------------------------------------------------------------------------------
;   FILENAME:   FlashBlock.inc
;   Version: 4.0.0.8, Updated on 2004/07/27 at 21:59:26
;------------------------------------------------------------------------------
;   DESCRIPTION:
;   FlashBlock library routine include file.
;
;   Copyright (C) Cypress MicroSystems 2000-2004. All rights reserved.
;
;------------------------------------------------------------------------------
;   FUNCTION NAME: bFlashWriteBlock
;
;   DESCRIPTION:
;   Writes 64 bytes of data to the flash at the specified blockId.
;
;   Regardless of the size of the buffer, this routine always writes 64
;   bytes of data. If the buffer is less than 64 bytes, then the next
;   64-N bytes of data will be written to fill the rest of flash block data.
;
;     TIMING NOTE on SYSCLK:
;        This algorithm will properly handle the appropriate timing when 
;        SYSCLK set to the IMO (internal main oscillator) setting with a power 
;        setting of 5.0/3.3/2.7 with SLIMO enabled or disabled.  
;
;        For a SYSCLK driven by an EXTERNAL CLOCK, it is advised that the
;        caller set the SYSCLK to the internal IMO setting prior to calling this
;        function and then restore the external clock upon return.
;
;        Flash block write timing is based on a 24MHz IMO with or without the
;        SLIMO bit enabled.
;
;   ARGUMENTS:
;   FLASH_WRITE_STRUCT *  psBlockWriteData
;
;   pointer to a structure that holds the calling arguments and some reserved
;   space for temporary local variables.
;
;   A = MSB of pointer
;   X = LSB of pointer
;
;   1) ALLOCATE the structure and set the X pointer to the first location
;   in the structure.  Note that this structure can be allocated on the stack.
;   2) Set the first three stucture elements with the required data: block ID,
;   flash buffer pointer, and temperature.
;   3) call this routine.
;   4) Restore stack if necessary
;
;   RETURNS: in Accumuator
;   BYTE - successful if NON-Zero returned.
;           Causes for Error are:  
;           1) Protection bits not set properly
;           2) Incorrect Temperature table version 
;
;   SIDE EFFECTS:
;   1) CPU clock temporarily set to 12MHz if SLIMO is NOT enabled and 6MHz if
;      SLIMO is enabled.
;
;-----------------------------------------------------------------------------
; Definition of FLASH_WRITE_STRUCT:

wARG_BlockId:              equ      0        ; block ID
pARG_FlashBuffer:          equ      2        ; flash buffer pointer - 2 bytes
cARG_Temperature:          equ      4        ; flash Tempurature
bDATA_PWErase:             equ      5        ; temporary storage (reserved)
bDATA_PWProgram:           equ      6        ; temporary storage (reserved)
bDATA_PWMultiplier:        equ      7        ; temporary storage (reserved)

FLASH_WRITE_STRUCT_SIZE:   equ      8        ; Calling frame size


;------------------------------------------------------------------------------
;   FUNCTION NAME: FlashReadBlock
;
;   DESCRIPTION:
;   Reads a specified flash block to a buffer in RAM.
;
;   ARGUMENTS:
;   FLASH_READ_STRUCT *  psBlockReadData
;
;   pointer to a structure that holds the calling arguments and some reserved
;   space for temporary local variables.
;
;   A = MSB of pointer
;   X = LSB of pointer
;
;   1) ALLOCATE the structure and set the X pointer to the first location
;   in the structure.  Note that this structure can be allocated on the stack.
;   2) Set the first three stucture elements with the required data: block ID,
;   flash buffer pointer, and count.
;   3) call this routine.
;   4) Restore stack if necessary
;
;
;  RETURNS: void
;     Data read is returned at specified pFlashBuffer.
;
;  SIDE EFFECTS:
;     none.
;
;------------------------------------------------------------------------------
; Definition of FLASH_READ_STRUCT:

wARG_BlockId:              equ      0        ; block ID
pARG_FlashBuffer:          equ      2        ; flash buffer pointer - 2 bytes
wARG_ReadCount:            equ      4        ; Read count - 2 bytes

FLASH_READ_STRUCT_SIZE:    equ      6        ; Calling frame size

;---------------------
;  End of File
;---------------------

