#ifndef __STRING_H
#define __STRING_H
#include <_const.h>

#if defined(_HC12)
#pragma nonpaged_function strcpy
#endif

#if defined(_M8C) && defined(_LMM)
#define strcpy	strcpyLMM
#define cstrcpy	cstrcpyLMM
#define strcmp	strcmpLMM
#define	cstrcmp	cstrcmpLMM
#define	strlen	strlenLMM
#define	cstrlen	cstrlenLMM
#define	strcat	strcatLMM
#define	cstrcat	cstrcatLMM
#define memcpy memcpyLMM
#define memset memsetLMM
#endif

#ifndef __SIZE_T
#define __SIZE_T
typedef unsigned int size_t;
#endif

void *memcpy(void *, void *, size_t);
void *memset(void *, int, size_t);
void *memchr(void *, int, size_t);
int memcmp(void *, void *, size_t);
void *memmove(void *, void *, size_t);

char *strchr(CONST char *, int);
int strcoll(CONST char *, CONST char *);
size_t strcspn(CONST char *, CONST char *);
char *strncat(char *, CONST char *, size_t);
int strncmp(CONST char *, CONST char *, size_t);
char *strncpy(char *, CONST char *, size_t);
char *strpbrk(CONST char *, CONST char *);
char *strrchr(CONST char *, int);
size_t strspn(CONST char *, CONST char *);
char *strstr(CONST char *, CONST char *);

#if !defined(_M8C)
char *strtok(char *, CONST char *);
#endif

size_t strlen(CONST char *);
char *strcpy(char *, CONST char *);
int strcmp(CONST char *, CONST char *);
char *strcat(char *, CONST char *);

#if defined(_AVR) || defined(_M8C)
size_t cstrlen(const char *cs);
char *cstrcpy(char *, const char *cs);
char *cstrncpy(char *, const char *cs, size_t);
int cstrcmp(const char *cs, char *);
char *cstrcat(char *, const char *);
#endif

#if defined(_AVR)
int cstrncmp(const char *cs, char *i, int);
/* thanks to JA, need to add prototypes to HELP file */
char *cstrstr(char *ramstr, const char *romstr);
char *cstrstrx(char *ramstr, const char *romstr); /* use elpm */
void *cmemcpy(void *, const void *, size_t);
void *cmemchr(const void *, int, size_t);
int cmemcmp(const void *, void *, size_t);
#endif

/*
size_t strxfrm(char *, char *, size_t);
*/
#endif
