;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  M8C.INC -- M8C27000 Microcontroller Family System Declarations
;;;
;;;  Copyright (c) 2003-2004, Cypress MicroSystems, Inc. All rights reserved.
;;;
;;;
;;;  This file provides address constants, bit field masks and a set of macro
;;;  facilities for the Cypress MicroSystems 27xxx Microcontroller family.
;;;
;;;  Last Modified: August 2, 2004
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;=============================================================================
;; Definition of abbreviations used in the descriptions below
;;  (RW)   The register or bit supports reads and writes
;;  (W)    The register or bit is write-only
;;  (R)    The register or bit is read-only
;;  (#)    Access to the register is bit specific (see the family datasheet)
;;  (RC)   The register or bit can be read, but writing a 0 will clear it,
;;         writing a 1 will have no effect.
;;=============================================================================

;;=============================================================================
;;      System Registers
;;=============================================================================

;----------------------------
;  Flag Register Bit Fields
;----------------------------
FLAG_XIO_MASK:  equ 10h
FLAG_SUPER:     equ 08h
FLAG_CARRY:     equ 04h
FLAG_ZERO:      equ 02h
FLAG_GLOBAL_IE: equ 01h


;;=============================================================================
;;      Register Space, Bank 0
;;=============================================================================

;------------------------------------------------
;  Port Registers
;  Note: Also see this address range in Bank 1.
;------------------------------------------------
; Port 0
PRT0DR:       equ 00h          ; Port 0 Data Register                     (RW)
PRT0IE:       equ 01h          ; Port 0 Interrupt Enable Register         (RW)
PRT0GS:       equ 02h          ; Port 0 Global Select Register            (RW)
PRT0DM2:      equ 03h          ; Port 0 Drive Mode 2                      (RW)
; Port 1
PRT1DR:       equ 04h          ; Port 1 Data Register                     (RW)
PRT1IE:       equ 05h          ; Port 1 Interrupt Enable Register         (RW)
PRT1GS:       equ 06h          ; Port 1 Global Select Register            (RW)
PRT1DM2:      equ 07h          ; Port 1 Drive Mode 2                      (RW)
; Port 2
PRT2DR:       equ 08h          ; Port 2 Data Register                     (RW)
PRT2IE:       equ 09h          ; Port 2 Interrupt Enable Register         (RW)
PRT2GS:       equ 0Ah          ; Port 2 Global Select Register            (RW)
PRT2DM2:      equ 0Bh          ; Port 2 Drive Mode 2                      (RW)
; Port 3
PRT3DR:       equ 0Ch          ; Port 3 Data Register                     (RW)
PRT3IE:       equ 0Dh          ; Port 3 Interrupt Enable Register         (RW)
PRT3GS:       equ 0Eh          ; Port 3 Global Select Register            (RW)
PRT3DM2:      equ 0Fh          ; Port 3 Drive Mode 2                      (RW)
; Port 4
PRT4DR:       equ 10h          ; Port 4 Data Register                     (RW)
PRT4IE:       equ 11h          ; Port 4 Interrupt Enable Register         (RW)
PRT4GS:       equ 12h          ; Port 4 Global Select Register            (RW)
PRT4DM2:      equ 13h          ; Port 4 Drive Mode 2                      (RW)
; Port 5
PRT5DR:       equ 14h          ; Port 5 Data Register                     (RW)
PRT5IE:       equ 15h          ; Port 5 Interrupt Enable Register         (RW)
PRT5GS:       equ 16h          ; Port 5 Global Select Register            (RW)
PRT5DM2:      equ 17h          ; Port 5 Drive Mode 2                      (RW)

;------------------------------------------------
;  Digital PSoC(tm) block Registers
;  Note: Also see this address range in Bank 1.
;------------------------------------------------
; Digital PSoC block 00, Basic Type B
DBB00DR0:     equ 20h          ; data register 0                          (#)
DBB00DR1:     equ 21h          ; data register 1                          (W)
DBB00DR2:     equ 22h          ; data register 2                          (RW)
DBB00CR0:     equ 23h          ; control & status register 0              (#)

; Digital PSoC block 01, Basic Type B
DBB01DR0:     equ 24h          ; data register 0                          (#)
DBB01DR1:     equ 25h          ; data register 1                          (W)
DBB01DR2:     equ 26h          ; data register 2                          (RW)
DBB01CR0:     equ 27h          ; control & status register 0              (#)

; Digital PSoC block 02, Communications Type B
DCB02DR0:     equ 28h          ; data register 0                          (#)
DCB02DR1:     equ 29h          ; data register 1                          (W)
DCB02DR2:     equ 2Ah          ; data register 2                          (RW)
DCB02CR0:     equ 2Bh          ; control & status register 0              (#)

; Digital PSoC block 03, Communications Type B
DCB03DR0:     equ 2Ch          ; data register 0                          (#)
DCB03DR1:     equ 2Dh          ; data register 1                          (W)
DCB03DR2:     equ 2Eh          ; data register 2                          (RW)
DCB03CR0:     equ 2Fh          ; control & status register 0              (#)

; Digital PSoC block 10, Basic Type B
DBB10DR0:     equ 30h          ; data register 0                          (#)
DBB10DR1:     equ 31h          ; data register 1                          (W)
DBB10DR2:     equ 32h          ; data register 2                          (RW)
DBB10CR0:     equ 33h          ; control & status register 0              (#)

; Digital PSoC block 11, Basic Type B
DBB11DR0:     equ 34h          ; data register 0                          (#)
DBB11DR1:     equ 35h          ; data register 1                          (W)
DBB11DR2:     equ 36h          ; data register 2                          (RW)
DBB11CR0:     equ 37h          ; control & status register 0              (#)

; Digital PSoC block 12, Communications Type B
DCB12DR0:     equ 38h          ; data register 0                          (#)
DCB12DR1:     equ 39h          ; data register 1                          (W)
DCB12DR2:     equ 3Ah          ; data register 2                          (RW)
DCB12CR0:     equ 3Bh          ; control & status register 0              (#)

; Digital PSoC block 13, Communications Type B
DCB13DR0:     equ 3Ch          ; data register 0                          (#)
DCB13DR1:     equ 3Dh          ; data register 1                          (W)
DCB13DR2:     equ 3Eh          ; data register 2                          (RW)
DCB13CR0:     equ 3Fh          ; control & status register 0              (#)

;-------------------------------------
;  Analog Resource Control Registers
;-------------------------------------
AMX_IN:       equ 60h          ; Analog Input Multiplexor Control         (RW)
AMX_IN_ACI3:          equ C0h    ; MASK: column 3 input mux
AMX_IN_ACI2:          equ 30h    ; MASK: column 2 input mux
AMX_IN_ACI1:          equ 0Ch    ; MASK: column 1 input mux
AMX_IN_ACI0:          equ 03h    ; MASK: column 0 input mux

ARF_CR:       equ 63h          ; Analog Reference Control Register        (RW)
ARF_CR_HBE:           equ 40h    ; MASK: Bias level control
ARF_CR_REF:           equ 38h    ; MASK: Analog Reference controls
ARF_CR_REFPWR:        equ 07h    ; MASK: Analog Reference power
ARF_CR_APWR:          equ 04h    ; MASK: use deprecated; see datasheet
ARF_CR_SCPWR:         equ 03h    ; MASK: Switched Cap block power

CMP_CR0:      equ 64h          ; Analog Comparator Bus 0 Register         (#)
CMP_CR0_COMP3:        equ 80h    ; MASK: Column 3 comparator state        (R)
CMP_CR0_COMP2:        equ 40h    ; MASK: Column 2 comparator state        (R)
CMP_CR0_COMP1:        equ 20h    ; MASK: Column 1 comparator state        (R)
CMP_CR0_COMP0:        equ 10h    ; MASK: Column 0 comparator state        (R)
CMP_CR0_AINT3:        equ 08h    ; MASK: Column 3 interrupt source        (RW)
CMP_CR0_AINT2:        equ 04h    ; MASK: Column 2 interrupt source        (RW)
CMP_CR0_AINT1:        equ 02h    ; MASK: Column 1 interrupt source        (RW)
CMP_CR0_AINT0:        equ 01h    ; MASK: Column 0 interrupt source        (RW)

ASY_CR:       equ 65h          ; Analog Synchronizaton Control            (#)
ASY_CR_SARCOUNT:      equ 70h    ; MASK: SAR support: resolution count    (W)
ASY_CR_SARSIGN:       equ 08h    ; MASK: SAR support: sign                (RW)
ASY_CR_SARCOL:        equ 06h    ; MASK: SAR support: column spec         (RW)
ASY_CR_SYNCEN:        equ 01h    ; MASK: Stall bit                        (RW)

CMP_CR1:      equ 66h          ; Analog Comparator Bus 1 Register         (RW)
CMP_CR1_ASYNCH3:      equ 80h    ; MASK: Column 3 comparator bus synch
CMP_CR1_ASYNCH2:      equ 40h    ; MASK: Column 2 comparator bus synch
CMP_CR1_ASYNCH1:      equ 20h    ; MASK: Column 1 comparator bus synch
CMP_CR1_ASYNCH0:      equ 10h    ; MASK: Column 0 comparator bus synch

;---------------------------------------------------
;  Analog PSoC block Registers
;
;  Note: the following registers are mapped into
;  both register bank 0 AND register bank 1.
;---------------------------------------------------

; Continuous Time PSoC block Type B Row 0 Col 0
ACB00CR3:     equ 70h          ; Control register 3                       (RW)
ACB00CR0:     equ 71h          ; Control register 0                       (RW)
ACB00CR1:     equ 72h          ; Control register 1                       (RW)
ACB00CR2:     equ 73h          ; Control register 2                       (RW)

; Continuous Time PSoC block Type B Row 0 Col 1
ACB01CR3:     equ 74h          ; Control register 3                       (RW)
ACB01CR0:     equ 75h          ; Control register 0                       (RW)
ACB01CR1:     equ 76h          ; Control register 1                       (RW)
ACB01CR2:     equ 77h          ; Control register 2                       (RW)

; Continuous Time PSoC block Type B Row 0 Col 2
ACB02CR3:     equ 78h          ; Control register 3                       (RW)
ACB02CR0:     equ 79h          ; Control register 0                       (RW)
ACB02CR1:     equ 7Ah          ; Control register 1                       (RW)
ACB02CR2:     equ 7Bh          ; Control register 2                       (RW)

; Continuous Time PSoC block Type B Row 0 Col 3
ACB03CR3:     equ 7Ch          ; Control register 3                       (RW)
ACB03CR0:     equ 7Dh          ; Control register 0                       (RW)
ACB03CR1:     equ 7Eh          ; Control register 1                       (RW)
ACB03CR2:     equ 7Fh          ; Control register 2                       (RW)

; Switched Cap PSoC blockType C Row 1 Col 0
ASC10CR0:     equ 80h          ; Control register 0                       (RW)
ASC10CR1:     equ 81h          ; Control register 1                       (RW)
ASC10CR2:     equ 82h          ; Control register 2                       (RW)
ASC10CR3:     equ 83h          ; Control register 3                       (RW)

; Switched Cap PSoC blockType D Row 1 Col 1
ASD11CR0:     equ 84h          ; Control register 0                       (RW)
ASD11CR1:     equ 85h          ; Control register 1                       (RW)
ASD11CR2:     equ 86h          ; Control register 2                       (RW)
ASD11CR3:     equ 87h          ; Control register 3                       (RW)

; Switched Cap PSoC blockType C Row 1 Col 2
ASC12CR0:     equ 88h          ; Control register 0                       (RW)
ASC12CR1:     equ 89h          ; Control register 1                       (RW)
ASC12CR2:     equ 8Ah          ; Control register 2                       (RW)
ASC12CR3:     equ 8Bh          ; Control register 3                       (RW)

; Switched Cap PSoC blockType D Row 1 Col 3
ASD13CR0:     equ 8Ch          ; Control register 0                       (RW)
ASD13CR1:     equ 8Dh          ; Control register 1                       (RW)
ASD13CR2:     equ 8Eh          ; Control register 2                       (RW)
ASD13CR3:     equ 8Fh          ; Control register 3                       (RW)

; Switched Cap PSoC blockType D Row 2 Col 0
ASD20CR0:     equ 90h          ; Control register 0                       (RW)
ASD20CR1:     equ 91h          ; Control register 1                       (RW)
ASD20CR2:     equ 92h          ; Control register 2                       (RW)
ASD20CR3:     equ 93h          ; Control register 3                       (RW)

; Switched Cap PSoC blockType C Row 2 Col 1
ASC21CR0:     equ 94h          ; Control register 0                       (RW)
ASC21CR1:     equ 95h          ; Control register 1                       (RW)
ASC21CR2:     equ 96h          ; Control register 2                       (RW)
ASC21CR3:     equ 97h          ; Control register 3                       (RW)

; Switched Cap PSoC blockType D Row 2 Col 2
ASD22CR0:     equ 98h          ; Control register 0                       (RW)
ASD22CR1:     equ 99h          ; Control register 1                       (RW)
ASD22CR2:     equ 9Ah          ; Control register 2                       (RW)
ASD22CR3:     equ 9Bh          ; Control register 3                       (RW)

; Switched Cap PSoC blockType C Row 2 Col 3
ASC23CR0:     equ 9Ch          ; Control register 0                       (RW)
ASC23CR1:     equ 9Dh          ; Control register 1                       (RW)
ASC23CR2:     equ 9Eh          ; Control register 2                       (RW)
ASC23CR3:     equ 9Fh          ; Control register 3                       (RW)

;------------------------------------------------
;  Row Digital Interconnects
;
;  Note: the following registers are mapped into
;  both register bank 0 AND register bank 1.
;------------------------------------------------

RDI0RI:       equ B0h          ; Row Digital Interconnect Row 0 Input Reg (RW)
RDI0SYN:      equ B1h          ; Row Digital Interconnect Row 0 Sync Reg  (RW)
RDI0IS:       equ B2h          ; Row 0 Input Select Register              (RW)
RDI0LT0:      equ B3h          ; Row 0 Look Up Table Register 0           (RW)
RDI0LT1:      equ B4h          ; Row 0 Look Up Table Register 1           (RW)
RDI0RO0:      equ B5h          ; Row 0 Output Register 0                  (RW)
RDI0RO1:      equ B6h          ; Row 0 Output Register 1                  (RW)

RDI1RI:       equ B8h          ; Row Digital Interconnect Row 1 Input Reg (RW)
RDI1SYN:      equ B9h          ; Row Digital Interconnect Row 1 Sync Reg  (RW)
RDI1IS:       equ BAh          ; Row 1 Input Select Register              (RW)
RDI1LT0:      equ BBh          ; Row 1 Look Up Table Register 0           (RW)
RDI1LT1:      equ BCh          ; Row 1 Look Up Table Register 1           (RW)
RDI1RO0:      equ BDh          ; Row 1 Output Register 0                  (RW)
RDI1RO1:      equ BEh          ; Row 1 Output Register 1                  (RW)

;------------------------------------------------
;  I2C Configuration Registers
;------------------------------------------------
I2C_CFG:      equ D6h          ; I2C Configuration Register               (RW)
I2C_CFG_PINSEL:         equ 40h  ; MASK: Select P1[0] and P1[1] for I2C
I2C_CFG_BUSERR_IE:      equ 20h  ; MASK: Enable interrupt on Bus Error
I2C_CFG_STOP_IE:        equ 10h  ; MASK: Enable interrupt on Stop
I2C_CFG_CLK_RATE_100K:  equ 00h  ; MASK: I2C clock set at 100K
I2C_CFG_CLK_RATE_400K:  equ 04h  ; MASK: I2C clock set at 400K
I2C_CFG_CLK_RATE_50K:   equ 08h  ; MASK: I2C clock set at 50K
I2C_CFG_CLK_RATE_1M6:   equ 0Ch  ; MASK: I2C clock set at 1.6M
I2C_CFG_CLK_RATE:       equ 0Ch  ; MASK: I2C clock rate setting mask
I2C_CFG_PSELECT_MASTER: equ 02h  ; MASK: Enable I2C Master
I2C_CFG_PSELECT_SLAVE:  equ 01h  ; MASK: Enable I2C Slave

I2C_SCR:      equ D7h          ; I2C Status and Control Register          (#)
I2C_SCR_BUSERR:        equ 80h   ; MASK: I2C Bus Error detected           (RC)
I2C_SCR_LOSTARB:       equ 40h   ; MASK: I2C Arbitration lost             (RC)
I2C_SCR_STOP:          equ 20h   ; MASK: I2C Stop detected                (RC)
I2C_SCR_ACK:           equ 10h   ; MASK: ACK the last byte                (RW)
I2C_SCR_ADDR:          equ 08h   ; MASK: Address rcv'd is Slave address   (RC)
I2C_SCR_XMIT:          equ 04h   ; MASK: Set transfer to tranmit mode     (RW)
I2C_SCR_LRB:           equ 02h   ; MASK: Last recieved bit                (RC)
I2C_SCR_BYTECOMPLETE:  equ 01h   ; MASK: Transfer of byte complete        (RC)

I2C_DR:       equ D8h          ; I2C Data Register                        (RW)

I2C_MSCR:     equ D9h          ; I2C Master Status and Control Register   (#)
I2C_MSCR_BUSY:         equ 08h   ; MASK: I2C Busy (Start detected)        (R)
I2C_MSCR_MODE:         equ 04h   ; MASK: Start has been generated         (R)
I2C_MSCR_RESTART:      equ 02h   ; MASK: Generate a Restart condition     (RW)
I2C_MSCR_START:        equ 01h   ; MASK: Generate a Start condition       (RW)

;------------------------------------------------
;  System and Global Resource Registers
;------------------------------------------------
INT_CLR0:     equ DAh          ; Interrupt Clear Register 0               (RW)
                               ; Use INT_MSK0 bit field masks
INT_CLR1:     equ DBh          ; Interrupt Clear Register 1               (RW)
                               ; Use INT_MSK1 bit field masks
INT_CLR3:     equ DDh          ; Interrupt Clear Register 3               (RW)
                               ; Use INT_MSK3 bit field masks

INT_MSK3:     equ DEh          ; I2C and Software Mask Register           (RW)
INT_MSK3_ENSWINT:          equ 80h ; MASK: enable/disable SW interrupt
INT_MSK3_I2C:              equ 01h ; MASK: enable/disable I2C interrupt

INT_MSK0:     equ E0h          ; General Interrupt Mask Register          (RW)
INT_MSK0_VC3:              equ 80h ; MASK: enable/disable VC3 interrupt
INT_MSK0_SLEEP:            equ 40h ; MASK: enable/disable sleep interrupt
INT_MSK0_GPIO:             equ 20h ; MASK: enable/disable GPIO  interrupt
INT_MSK0_ACOLUMN_3:        equ 10h ; MASK: enable/disable Analog col 3 interrupt
INT_MSK0_ACOLUMN_2:        equ 08h ; MASK: enable/disable Analog col 2 interrupt
INT_MSK0_ACOLUMN_1:        equ 04h ; MASK: enable/disable Analog col 1 interrupt
INT_MSK0_ACOLUMN_0:        equ 02h ; MASK: enable/disable Analog col 0 interrupt
INT_MSK0_VOLTAGE_MONITOR:  equ 01h ; MASK: enable/disable Volts interrupt

INT_MSK1:     equ E1h          ; Digital PSoC block Mask Register         (RW)
INT_MSK1_DCB13:            equ 80h ; MASK: enable/disable DCB13 block interrupt
INT_MSK1_DCB12:            equ 40h ; MASK: enable/disable DCB12 block interrupt
INT_MSK1_DBB11:            equ 20h ; MASK: enable/disable DBB11 block interrupt
INT_MSK1_DBB10:            equ 10h ; MASK: enable/disable DBB10 block interrupt
INT_MSK1_DCB03:            equ 08h ; MASK: enable/disable DCB03 block interrupt
INT_MSK1_DCB02:            equ 04h ; MASK: enable/disable DCB02 block interrupt
INT_MSK1_DBB01:            equ 02h ; MASK: enable/disable DBB01 block interrupt
INT_MSK1_DBB00:            equ 01h ; MASK: enable/disable DBB00 block interrupt

INT_VC:       equ E2h          ; Interrupt vector register                (RC)
RES_WDT:      equ E3h          ; Watch Dog Timer Register                 (W)

; DECIMATOR Registers
DEC_DH:       equ E4h          ; Data Register (high byte)                (RC)
DEC_DL:       equ E5h          ; Data Register ( low byte)                (RC)
DEC_CR0:      equ E6h          ; Data Control Register 0                  (RW)
DEC_CR1:      equ E7h          ; Data Control Register 1                  (RW)

; Multiplier and MAC (Multiply/Accumulate) Unit
MUL_X:        equ E8h          ; Multiplier X Register (write)            (W)
MUL_Y:        equ E9h          ; Multiplier Y Register (write)            (W)
MUL_DH:       equ EAh          ; Multiplier Result Data (high byte read)  (R)
MUL_DL:       equ EBh          ; Multiplier Result Data ( low byte read)  (R)
MAC_X:        equ ECh          ; write = MAC X register [also see ACC_DR1]
ACC_DR1:      equ MAC_X        ; read =  MAC Accumulator, byte 1          (RW)
MAC_Y:        equ EDh          ; write = MAC Y register [also see ACC_DR0]
ACC_DR0:      equ MAC_Y        ; read =  MAC Accumulator, byte 0          (RW)
MAC_CL0:      equ EEh          ; write = MAC Clear Accum [also see ACC_DR3]
ACC_DR3:      equ MAC_CL0      ; read =  MAC Accumulator, byte 3          (RW)
MAC_CL1:      equ EFh          ; write = MAC Clear Accum [also see ACC_DR2]
ACC_DR2:      equ MAC_CL1      ; read =  MAC Accumulator, byte 2          (RW)

;------------------------------------------------------
;  System Status and Control Registers
;
;  Note: The following registers are mapped into both
;        register bank 0 AND register bank 1.
;------------------------------------------------------
CPU_F:        equ F7h          ; CPU Flag Register Access                 (RO)
                                   ; Use FLAG_ masks defined at top of file

CPU_SCR1:     equ FEh          ; CPU Status and Control Register #1       (#)
CPU_SCR1_SLIMO:         equ 10h	   ; MASK: Slow IMO (internal main osc) enable
CPU_SCR1_IRESS:         equ 80h    ; MASK: flag, Internal Reset Status bit
CPU_SCR1_ECO_ALWD_WR:   equ 08h    ; MASK: flag, ECO allowed has been written
CPU_SCR1_ECO_ALLOWED:   equ 04h    ; MASK: ECO allowed to be enabled
CPU_SCR1_IRAMDIS:       equ 01h    ; MASK: Disable RAM initialization on WDR

CPU_SCR0:     equ FFh          ; CPU Status and Control Register #2       (#)
CPU_SCR0_GIE_MASK:      equ 80h    ; MASK: Global Interrupt Enable shadow
CPU_SCR0_WDRS_MASK:     equ 20h    ; MASK: Watch Dog Timer Reset
CPU_SCR0_PORS_MASK:     equ 10h    ; MASK: power-on reset bit PORS
CPU_SCR0_SLEEP_MASK:    equ 08h    ; MASK: Enable Sleep
CPU_SCR0_STOP_MASK:     equ 01h    ; MASK: Halt CPU bit


;;=============================================================================
;;      Register Space, Bank 1
;;=============================================================================

;------------------------------------------------
;  Port Registers
;  Note: Also see this address range in Bank 0.
;------------------------------------------------
; Port 0
PRT0DM0:      equ 00h          ; Port 0 Drive Mode 0                      (RW)
PRT0DM1:      equ 01h          ; Port 0 Drive Mode 1                      (RW)
PRT0IC0:      equ 02h          ; Port 0 Interrupt Control 0               (RW)
PRT0IC1:      equ 03h          ; Port 0 Interrupt Control 1               (RW)

; Port 1
PRT1DM0:      equ 04h          ; Port 1 Drive Mode 0                      (RW)
PRT1DM1:      equ 05h          ; Port 1 Drive Mode 1                      (RW)
PRT1IC0:      equ 06h          ; Port 1 Interrupt Control 0               (RW)
PRT1IC1:      equ 07h          ; Port 1 Interrupt Control 1               (RW)

; Port 2
PRT2DM0:      equ 08h          ; Port 2 Drive Mode 0                      (RW)
PRT2DM1:      equ 09h          ; Port 2 Drive Mode 1                      (RW)
PRT2IC0:      equ 0Ah          ; Port 2 Interrupt Control 0               (RW)
PRT2IC1:      equ 0Bh          ; Port 2 Interrupt Control 1               (RW)

; Port 3
PRT3DM0:      equ 0Ch          ; Port 3 Drive Mode 0                      (RW)
PRT3DM1:      equ 0Dh          ; Port 3 Drive Mode 1                      (RW)
PRT3IC0:      equ 0Eh          ; Port 3 Interrupt Control 0               (RW)
PRT3IC1:      equ 0Fh          ; Port 3 Interrupt Control 1               (RW)

; Port 4
PRT4DM0:      equ 10h          ; Port 4 Drive Mode 0                      (RW)
PRT4DM1:      equ 11h          ; Port 4 Drive Mode 1                      (RW)
PRT4IC0:      equ 12h          ; Port 4 Interrupt Control 0               (RW)
PRT4IC1:      equ 13h          ; Port 4 Interrupt Control 1               (RW)

; Port 5
PRT5DM0:      equ 14h          ; Port 5 Drive Mode 0                      (RW)
PRT5DM1:      equ 15h          ; Port 5 Drive Mode 1                      (RW)
PRT5IC0:      equ 16h          ; Port 5 Interrupt Control 0               (RW)
PRT5IC1:      equ 17h          ; Port 5 Interrupt Control 1               (RW)

;------------------------------------------------
;  Digital PSoC(tm) block Registers
;  Note: Also see this address range in Bank 0.
;------------------------------------------------

; Digital PSoC block 00, Basic Type B
DBB00FN:      equ 20h          ; Function Register                        (RW)
DBB00IN:      equ 21h          ;    Input Register                        (RW)
DBB00OU:      equ 22h          ;   Output Register                        (RW)

; Digital PSoC block 01, Basic Type B
DBB01FN:      equ 24h          ; Function Register                        (RW)
DBB01IN:      equ 25h          ;    Input Register                        (RW)
DBB01OU:      equ 26h          ;   Output Register                        (RW)

; Digital PSoC block 02, Communications Type B
DCB02FN:      equ 28h          ; Function Register                        (RW)
DCB02IN:      equ 29h          ;    Input Register                        (RW)
DCB02OU:      equ 2Ah          ;   Output Register                        (RW)

; Digital PSoC block 03, Communications Type B
DCB03FN:      equ 2Ch          ; Function Register                        (RW)
DCB03IN:      equ 2Dh          ;    Input Register                        (RW)
DCB03OU:      equ 2Eh          ;   Output Register                        (RW)

; Digital PSoC block 10, Basic Type B
DBB10FN:      equ 30h          ; Function Register                        (RW)
DBB10IN:      equ 31h          ;    Input Register                        (RW)
DBB10OU:      equ 32h          ;   Output Register                        (RW)

; Digital PSoC block 11, Basic Type B
DBB11FN:      equ 34h          ; Function Register                        (RW)
DBB11IN:      equ 35h          ;    Input Register                        (RW)
DBB11OU:      equ 36h          ;   Output Register                        (RW)

; Digital PSoC block 12, Communications Type B
DCB12FN:      equ 38h          ; Function Register                        (RW)
DCB12IN:      equ 39h          ;    Input Register                        (RW)
DCB12OU:      equ 3Ah          ;   Output Register                        (RW)

; Digital PSoC block 13, Communications Type B
DCB13FN:      equ 3Ch          ; Function Register                        (RW)
DCB13IN:      equ 3Dh          ;    Input Register                        (RW)
DCB13OU:      equ 3Eh          ;   Output Register                        (RW)

;------------------------------------------------
;  System and Global Resource Registers
;  Note: Also see this address range in Bank 0.
;------------------------------------------------

CLK_CR0:      equ 60h          ; Analog Column Clock Select Register 0    (RW)
CLK_CR0_ACOLUMN_3:    equ C0h    ; MASK: Specify clock for analog cloumn
CLK_CR0_ACOLUMN_2:    equ 30h    ; MASK: Specify clock for analog cloumn
CLK_CR0_ACOLUMN_1:    equ 0Ch    ; MASK: Specify clock for analog cloumn
CLK_CR0_ACOLUMN_0:    equ 03h    ; MASK: Specify clock for analog cloumn

CLK_CR1:      equ 61h          ; Analog Clock Source Select Register 1    (RW)
CLK_CR1_SHDIS:        equ 40h    ; MASK: Sample and Hold Disable (all Columns)
CLK_CR1_ACLK1:        equ 38h    ; MASK: Digital PSoC block for analog source
CLK_CR1_ACLK2:        equ 07h    ; MASK: Digital PSoC block for analog source

ABF_CR0:      equ 62h          ; Analog Output Buffer Control Register 0  (RW)
ABF_CR0_ACOL1MUX:     equ 80h    ; MASK: Analog Column 1 Mux control
ABF_CR0_ACOL2MUX:     equ 40h    ; MASK: Analog Column 2 Mux control
ABF_CR0_ABUF1EN:      equ 20h    ; MASK: Enable ACol 1 analog buffer (P0[5])
ABF_CR0_ABUF2EN:      equ 10h    ; MASK: Enable ACol 2 analog buffer (P0[4])
ABF_CR0_ABUF0EN:      equ 08h    ; MASK: Enable ACol 0 analog buffer (P0[3])
ABF_CR0_ABUF3EN:      equ 04h    ; MASK: Enable ACol 3 analog buffer (P0[2])
ABF_CR0_BYPASS:       equ 02h    ; MASK: Bypass the analog buffers
ABF_CR0_PWR:          equ 01h    ; MASK: High power mode on all analog buffers

AMD_CR0:      equ 63h          ; Analog Modulator Control Register 0      (RW)
AMD_CR0_AMOD2:        equ 70h    ; MASK: Modulation source for analog column 2
AMD_CR0_AMOD0:        equ 07h    ; MASK: Modulation source for analog column 1

AMD_CR1:      equ 66h          ; Analog Modulator Control Register 1      (RW)
AMD_CR1_AMOD3:        equ 70h    ; MASK: Modulation ctrl for analog column 3
AMD_CR1_AMOD1:        equ 07h    ; MASK: Modulation ctrl for analog column 1

ALT_CR0:      equ 67h          ; Analog Look Up Table (LUT) Register 0    (RW)
ALT_CR0_LUT1:         equ F0h    ; MASK: Look up table 1 selection
ALT_CR0_LUT0:         equ 0Fh    ; MASK: Look up table 0 selection

ALT_CR1:      equ 68h          ; Analog Look Up Table (LUT) Register 1    (RW)
ALT_CR1_LUT3:         equ F0h    ; MASK: Look up table 3 selection
ALT_CR1_LUT2:         equ 0Fh    ; MASK: Look up table 2 selection

CLK_CR2:      equ 69h          ; Analog Clock Source Control Register 2   (RW)
CLK_CR2_ACLK1R:       equ 08h    ; MASK: Analog Clock 1 selection range
CLK_CR2_ACLK0R:       equ 01h    ; MASK: Analog Clock 0 selection range

;------------------------------------------------
;  Global Digital Interconnects
;------------------------------------------------

GDI_O_IN:     equ D0h          ; Global Dig Interconnect Odd Inputs Reg   (RW)
GDI_E_IN:     equ D1h          ; Global Dig Interconnect Even Inputs Reg  (RW)
GDI_O_OU:     equ D2h          ; Global Dig Interconnect Odd Outputs Reg  (RW)
GDI_E_OU:     equ D3h          ; Global Dig Interconnect Even Outputs Reg (RW)

;------------------------------------------------
;  Clock and System Control Registers
;------------------------------------------------

OSC_GO_EN:    equ DDh          ; Oscillator to Global Outputs Enable Register (RW)
OSC_GOEN_SLPINT:      equ 80h	 ; Enable Sleep Timer onto GOE[7]
OSC_GOEN_VC3:         equ 40h    ; Enable VC3 onto GOE[6]
OSC_GOEN_VC2:         equ 20h    ; Enable VC2 onto GOE[5]
OSC_GOEN_VC1:         equ 10h    ; Enable VC1 onto GOE[4]
OSC_GOEN_SYSCLKX2:    equ 08h    ; Enable 2X SysClk onto GOE[3]
OSC_GOEN_SYSCLK:      equ 04h    ; Enable 1X SysClk onto GOE[2]
OSC_GOEN_CLK24M:      equ 02h    ; Enable 24 MHz clock onto GOE[1]
OSC_GOEN_CLK32K:      equ 01h    ; Enable 32 kHz clock onto GOE[0]

OSC_CR4:      equ DEh          ; Oscillator Control Register 4            (RW)
OSC_CR4_VC3:          equ 03h    ; MASK: System VC3 Clock source

OSC_CR3:      equ DFh          ; Oscillator Control Register 3            (RW)

OSC_CR0:      equ E0h          ; System Oscillator Control Register 0     (RW)
OSC_CR0_32K_SELECT:   equ 80h    ; MASK: Enable/Disable External XTAL Osc
OSC_CR0_PLL_MODE:     equ 40h    ; MASK: Enable/Disable PLL
OSC_CR0_NO_BUZZ:      equ 20h    ; MASK: Bandgap always powered/BUZZ bandgap
OSC_CR0_SLEEP:        equ 18h    ; MASK: Set Sleep timer freq/period
OSC_CR0_SLEEP_512Hz:  equ 00h    ;     Set sleep bits for 1.95ms period
OSC_CR0_SLEEP_64Hz:   equ 08h    ;     Set sleep bits for 15.6ms period
OSC_CR0_SLEEP_8Hz:    equ 10h    ;     Set sleep bits for 125ms period
OSC_CR0_SLEEP_1Hz:    equ 18h    ;     Set sleep bits for 1 sec period
OSC_CR0_CPU:          equ 07h    ; MASK: Set CPU Frequency
OSC_CR0_CPU_3MHz:     equ 00h    ;     set CPU Freq bits for 3MHz Operation
OSC_CR0_CPU_6MHz:     equ 01h    ;     set CPU Freq bits for 6MHz Operation
OSC_CR0_CPU_12MHz:    equ 02h    ;     set CPU Freq bits for 12MHz Operation
OSC_CR0_CPU_24MHz:    equ 03h    ;     set CPU Freq bits for 24MHz Operation
OSC_CR0_CPU_1d5MHz:   equ 04h    ;     set CPU Freq bits for 1.5MHz Operation
OSC_CR0_CPU_750kHz:   equ 05h    ;     set CPU Freq bits for 750kHz Operation
OSC_CR0_CPU_187d5kHz: equ 06h    ;     set CPU Freq bits for 187.5kHz Operation
OSC_CR0_CPU_93d7kHz:  equ 07h    ;     set CPU Freq bits for 93.7kHz Operation

OSC_CR1:      equ E1h          ; System VC1/VC2 Divider Control Register  (RW)
OSC_CR1_VC1:          equ F0h    ; MASK: System VC1 24MHz/External Clk divider
OSC_CR1_VC2:          equ 0Fh    ; MASK: System VC2 24MHz/External Clk divider

OSC_CR2:      equ E2h          ; Oscillator Control Register 2            (RW)
OSC_CR2_PLLGAIN:      equ 80h    ; MASK: High/Low gain
OSC_CR2_EXTCLKEN:     equ 04h    ; MASK: Enable/Disable External Clock
OSC_CR2_IMODIS:       equ 02h    ; MASK: Enable/Disable System (IMO) Clock Net
OSC_CR2_SYSCLKX2DIS:  equ 01h    ; MASK: Enable/Disable 48MHz clock source

VLT_CR:       equ E3h          ; Voltage Monitor Control Register         (RW)
VLT_CR_SMP:           equ 80h    ; MASK: Enable Switch Mode Pump
VLT_CR_PORLEV:        equ 30h    ; MASK: Mask for Power on Reset level control
VLT_CR_POR_LOW:       equ 00h    ;   Lowest  Precision Power-on Reset trip point
VLT_CR_POR_MID:       equ 10h    ;   Middle  Precision Power-on Reset trip point
VLT_CR_POR_HIGH:      equ 20h    ;   Highest Precision Power-on Reset trip point
VLT_CR_LVDTBEN:       equ 08h    ; MASK: Enable the CPU Throttle Back on LVD
VLT_CR_VM:            equ 07h    ; MASK: Mask for Voltage Monitor level setting
VLT_CR_3V0_POR:       equ 00h    ; -- deprecated symbols --
VLT_CR_4V5_POR:       equ 10h    ;    deprecated
VLT_CR_4V75_POR:      equ 20h    ;    deprecated
VLT_CR_DISABLE:       equ 30h    ;    deprecated

VLT_CMP:      equ E4h          ; Voltage Monitor Comparators Register     (R)
VLT_CMP_PUMP:         equ 04h    ; MASK: Vcc below SMP trip level
VLT_CMP_LVD:          equ 02h    ; MASK: Vcc below LVD trip level
VLT_CMP_PPOR:         equ 01h    ; MASK: Vcc below PPOR trip level

IMO_TR:       equ E8h          ; Internal Main Oscillator Trim Register   (W)
ILO_TR:       equ E9h          ; Internal Low-speed Oscillator Trim       (W)
BDG_TR:       equ EAh          ; Band Gap Trim Register                   (W)
ECO_TR:       equ EBh          ; External Oscillator Trim Register        (W)

;;=============================================================================
;;      M8C System Macros
;;  These macros should be used when their functions are needed.
;;=============================================================================

;----------------------------------------------------
;  Swapping Register Banks
;----------------------------------------------------
    macro M8C_SetBank0
    and   F, ~FLAG_XIO_MASK
    endm

    macro M8C_SetBank1
    or    F, FLAG_XIO_MASK
    endm

;----------------------------------------------------
;  Global Interrupt Enable/Disable
;----------------------------------------------------
    macro M8C_EnableGInt
    or    F, FLAG_GLOBAL_IE
    endm

    macro M8C_DisableGInt
    and   F, ~FLAG_GLOBAL_IE
    endm

;----------------------------------------------------
;  Enable/Disable Interrupt Mask
;
;  Use the following macros to enable/disable
;  bits in the Interrupt mask registers,
;  INT_MSK0, INT_MSK1 or INT_MSK3.
;
;  Usage:    M8C_DisableIntMask INT_MSKN, MASK
;            M8C_EnableIntMask  INT_MSKN, MASK
;
;  where INT_MSKN is INT_MSK0, INT_MSK1 or INT_MSK3
;        and MASK is the bit set to enable or disable
;----------------------------------------------------
; Disable Interrupt Bit Mask(s)
    macro M8C_DisableIntMask
    and   reg[@0], ~@1              ; disable specified interrupt enable bit
    endm

; Enable Interrupt Bit Mask(s)
    macro M8C_EnableIntMask
    or    reg[@0], @1               ; enable specified interrupt enable bit
    endm

;----------------------------------------------------
;  Clear Posted Interrupt Flag Mask
;
;  Use the following macros to clear the
;  bits in the Interrupt Clear registers,
;  INT_CLR0, INT_CLR1 or INT_CLR3.
;  Usage:    M8C_ClearIntFlag INT_CLRN, MASK
;
;  where INT_MSKN is INT_CLR0, INT_CLR1 or INT_CLR3
;        and MASK is the bit set to enable or disable
;----------------------------------------------------
    macro M8C_ClearIntFlag
    mov   reg[@0], ~@1              ; clear specified interrupt enable bit
    endm

;----------------------------------------------------
;  Power-On Reset & WatchDog Timer Functions
;----------------------------------------------------
    macro M8C_EnableWatchDog
    and   reg[CPU_SCR0], ~CPU_SCR0_PORS_MASK
    endm

    macro M8C_ClearWDT
    mov   reg[RES_WDT], 00h
    endm

    macro M8C_ClearWDTAndSleep
    mov   reg[RES_WDT], 38h
    endm

;----------------------------------------------------
;  CPU Stall for Analog PSoC Block Writes
;----------------------------------------------------
    macro M8C_Stall
    or    reg[ASY_CR], ASY_CR_SYNCEN
    endm

    macro M8C_Unstall
    and   reg[ASY_CR], ~ASY_CR_SYNCEN
    endm

;----------------------------------------------------
;  Sleep, CPU Stop & Software Reset
;----------------------------------------------------
    macro M8C_Sleep
    or    reg[CPU_SCR0], CPU_SCR0_SLEEP_MASK
    ; The next instruction to be executed depends on the state of the
    ; various interrupt enable bits. If some interrupts are enabled
    ; and the global interrupts are disabled, the next instruction will
    ; be the one that follows the invocation of this macro. If global
    ; interrupts are also enabled then the next instruction will be
    ; from the interrupt vector table. If no interrupts are enabled
    ; then the CPU sleeps forever.
    endm

    macro M8C_Stop
    ; In general, you probably don't want to do this, but here's how:
    or    reg[CPU_SCR0], CPU_SCR0_STOP_MASK
    ; Next instruction to be executed is located in the interrupt
    ; vector table entry for Power-On Reset.
    endm

    macro M8C_Reset
    ; Restore CPU to the power-on reset state.
    mov A, 0
    SSC
    ; Next non-supervisor instruction will be at interrupt vector 0.
    endm

;----------------------------------------------------
; ImageCraft Code Compressor Actions
;----------------------------------------------------
    ; Suspend Code Compressor
    ; Must not span a RET or RETI instruction
    ; without resuming code compression
    macro Suspend_CodeCompressor
    or   F, 0
    endm

    ; Resume Code Compression
    macro Resume_CodeCompressor
    add  SP, 0
    endm

; end of file m8c.inc
