//=============================================================================
//
//    m8c.h: CYWUSB69xx Microcontroller Device System Declarations
//
//    Copyright: Cypress Semiconductor 2005. All Rights Reserved.
//
//
//    This file provides address constants, bit field masks and a set of macro
//    facilities for the Cypress MicroSystems CYWUSB69xx Microcontrollers.
//
//    Last Modified: August 31, 2005
//
//=============================================================================

#ifndef M8C_C_HEADER
#define M8C_C_HEADER

//-----------------------------------------------
// Define System Types
//-----------------------------------------------
typedef  unsigned char  BOOL;
typedef  unsigned char  BYTE;
typedef  signed   char  CHAR;
typedef  unsigned int   WORD;
typedef  signed   int   INT;
typedef  unsigned long  DWORD;
typedef  signed   long  LONG;

//-----------------------------------------------
// Define Boolean TRUE/FALSE
//-----------------------------------------------
#define  TRUE  ((BOOL) 1)
#define  FALSE ((BOOL) 0)


//=============================================================================
//=============================================================================
//      System Registers
//=============================================================================
//=============================================================================

//-----------------------------------------------
//  Flag Register Bit Fields
//-----------------------------------------------
#define  FLAG_PGMODE_MASK  (0xC0)
#define  FLAG_PGMODE_0     (0x00)
#define  FLAG_PGMODE_1     (0x40)
#define  FLAG_PGMODE_2     (0x80)
#define  FLAG_PGMODE_3     (0xC0)
#define  FLAG_PGMODE_00b   (0x00)
#define  FLAG_PGMODE_01b   (0x40)
#define  FLAG_PGMODE_10b   (0x80)
#define  FLAG_PGMODE_11b   (0xC0)
#define  FLAG_XIO_MASK     (0x10)
#define  FLAG_SUPER        (0x08)
#define  FLAG_CARRY        (0x04)
#define  FLAG_ZERO         (0x02)
#define  FLAG_GLOBAL_IE    (0x01)


//=============================================================================
//=============================================================================
//      Register Space, Bank 0
//=============================================================================
//=============================================================================

//-----------------------------------------------
//  Port Registers
//  Note: Also see this address range in Bank 1.
//-----------------------------------------------
// Port 0
#pragma  ioport   PRT0DR:     0x000  // Port 0 Data Register
BYTE              PRT0DR;
#pragma  ioport   PRT0IE:     0x001  // Port 0 Interrupt Enable Register
BYTE              PRT0IE;
#pragma  ioport   PRT0GS:     0x002  // Port 0 Global Select Register
BYTE              PRT0GS;
#pragma  ioport   PRT0DM2:    0x003  // Port 0 Drive Mode 2
BYTE              PRT0DM2;
// Port 1
#pragma  ioport   PRT1DR:     0x004  // Port 1 Data Register
BYTE              PRT1DR;
#pragma  ioport   PRT1IE:     0x005  // Port 1 Interrupt Enable Register
BYTE              PRT1IE;
#pragma  ioport   PRT1GS:     0x006  // Port 1 Global Select Register
BYTE              PRT1GS;
#pragma  ioport   PRT1DM2:    0x007  // Port 1 Drive Mode 2
BYTE              PRT1DM2;
// Port 2
#pragma  ioport   PRT2DR:     0x008  // Port 2 Data Register
BYTE              PRT2DR;
#pragma  ioport   PRT2IE:     0x009  // Port 2 Interrupt Enable Register
BYTE              PRT2IE;
#pragma  ioport   PRT2GS:     0x00A  // Port 2 Global Select Register
BYTE              PRT2GS;
#pragma  ioport   PRT2DM2:    0x00B  // Port 2 Drive Mode 2
BYTE              PRT2DM2;
// Port 3
#pragma  ioport   PRT3DR:     0x00C  // Port 3 Data Register
BYTE              PRT3DR;
#pragma  ioport   PRT3IE:     0x00D  // Port 3 Interrupt Enable Register
BYTE              PRT3IE;
#pragma  ioport   PRT3GS:     0x00E  // Port 3 Global Select Register
BYTE              PRT3GS;
#pragma  ioport   PRT3DM2:    0x00F  // Port 3 Drive Mode 2
BYTE              PRT3DM2;

//-----------------------------------------------
//  Digital PSoC(tm) block Registers
//  Note: Also see this address range in Bank 1.
//-----------------------------------------------

// Digital PSoC block 00, Basic Type B
#pragma  ioport   DBB00DR0:   0x020  // data register 0
BYTE              DBB00DR0;
#pragma  ioport   DBB00DR1:   0x021  // data register 1
BYTE              DBB00DR1;
#pragma  ioport   DBB00DR2:   0x022  // data register 2
BYTE              DBB00DR2;
#pragma  ioport   DBB00CR0:   0x023  // control & status register 0
BYTE              DBB00CR0;

// Digital PSoC block 01, Basic Type B
#pragma  ioport   DBB01DR0:   0x024  // data register 0
BYTE              DBB01DR0;
#pragma  ioport   DBB01DR1:   0x025  // data register 1
BYTE              DBB01DR1;
#pragma  ioport   DBB01DR2:   0x026  // data register 2
BYTE              DBB01DR2;
#pragma  ioport   DBB01CR0:   0x027  // control & status register 0
BYTE              DBB01CR0;

// Digital PSoC block 02, Communication Type B
#pragma  ioport   DCB02DR0:   0x028  // data register 0
BYTE              DCB02DR0;
#pragma  ioport   DCB02DR1:   0x029  // data register 1
BYTE              DCB02DR1;
#pragma  ioport   DCB02DR2:   0x02A  // data register 2
BYTE              DCB02DR2;
#pragma  ioport   DCB02CR0:   0x02B  // control & status register 0
BYTE              DCB02CR0;

// Digital PSoC block 03, Communication Type B
#pragma  ioport   DCB03DR0:   0x02C  // data register 0
BYTE              DCB03DR0;
#pragma  ioport   DCB03DR1:   0x02D  // data register 1
BYTE              DCB03DR1;
#pragma  ioport   DCB03DR2:   0x02E  // data register 2
BYTE              DCB03DR2;
#pragma  ioport   DCB03CR0:   0x02F  // control & status register 0
BYTE              DCB03CR0;

//-----------------------------------------------
//  Analog Control Registers
//-----------------------------------------------

#pragma  ioport   AMX_IN:     0x060  // Analog Input Multiplexor Control
BYTE              AMX_IN;
#define AMX_IN_ACI1     (0x0C)
#define AMX_IN_ACI0     (0x03)

#pragma  ioport   AMUXCFG:    0x061  // Analog MUX Configuration
BYTE              AMUXCFG;
#define AMUXCFG_INTCAP  (0x30)
#define AMUXCFG_MUXCLK  (0x0E)
#define AMUXCFG_EN      (0x01)

#pragma  ioport   PWM_CR:     0x062  // Pulse-Width Modulator Control
BYTE              PWM_CR;
#define PWM_CR_HIGH      (0x38)
#define PWM_CR_LOW       (0x06)
#define PWM_CR_EN        (0x01)

#pragma  ioport   CMP_CR0:     0x064  // Analog Comparator Bus Register 0
BYTE              CMP_CR0;
#define CMP_CR0_COMP1    (0x20)
#define CMP_CR0_COMP0    (0x10)
#define CMP_CR0_AINT1    (0x02)
#define CMP_CR0_AINT0    (0x01)

#pragma  ioport  CMP_CR1:     0x066   // Analog Comparator Bus Register 1
BYTE             CMP_CR1;
#define CMP_CR1_CLDIS1  (0x20)
#define CMP_CR1_CLDIS0  (0x10)

#pragma  ioport  ADC0_CR:     0x068   // Analog Column 0 Configuration
BYTE             ADC0_CR;
#define ADC0_CR_CMPST   (0x80)
#define ADC0_CR_LOREN   (0x40)
#define ADC0_CR_SHEN    (0x20)
#define ADC0_CR_CBSRC   (0x08)
#define ADC0_CR_ADCM    (0x04)
#define ADC0_CR_EN      (0x01)

#pragma  ioport  ADC1_CR:     0x069   // Analog Column 1 Configuration
BYTE             ADC1_CR;
#define ADC1_CR_CMPST   (0x80)
#define ADC1_CR_LOREN   (0x40)
#define ADC1_CR_SHEN    (0x20)
#define ADC1_CR_CBSRC   (0x08)
#define ADC1_CR_ADCM    (0x04)
#define ADC1_CR_EN      (0x01)

// Continuous Time PSoC Block Type E Row 0 Col 0
#pragma  ioport   ACE00CR1:   0x72  // Control register 1
BYTE              ACE00CR1;
#pragma  ioport   ACE00CR2:   0x73  // Control register 2
BYTE              ACE00CR2;

// Continuous Time PSoC Block Type E Row 0 Col 1
#pragma  ioport   ACE01CR1:   0x76  // Control register 1
BYTE              ACE01CR1;
#pragma  ioport   ACE01CR2:   0x77  // Control register 2
BYTE              ACE01CR2;

// Switched Cap PSoC Block Type E Row 1 Col 0
#pragma  ioport   ASE10CR0:   0x80  // Control register 0
BYTE              ASE10CR0;

// Switched Cap PSoC Block Type E Row 1 Col 1
#pragma  ioport   ASE11CR0:   0x84  // Control register 0
BYTE              ASE11CR0;

//-----------------------------------------------
//  Global General Purpose Data Registers
//-----------------------------------------------
#pragma  ioport   TMP_DR0:     0x6C
BYTE              TMP_DR0;
#pragma  ioport   TMP_DR1:     0x6D
BYTE              TMP_DR1;
#pragma  ioport   TMP_DR2:     0x6E
BYTE              TMP_DR2;
#pragma  ioport   TMP_DR3:     0x6F
BYTE              TMP_DR3;

//-----------------------------------------------
//  Row Digital Interconnects
//
//  Note: the following registers are mapped into
//  both register bank 0 AND register bank 1.
//-----------------------------------------------

#pragma  ioport   RDI0RI:     0xB0  // Row Digital Interconnect Row 0 Input
BYTE              RDI0RI;
#pragma  ioport   RDI0SYN:    0xB1  // Row Digital Interconnect Row 0 Sync Reg
BYTE              RDI0SYN;
#pragma  ioport   RDI0IS:     0xB2  // Row 0 Input Select Register
BYTE              RDI0IS;
#pragma  ioport   RDI0LT0:    0xB3  // Row 0 Look Up Table Register 0
BYTE              RDI0LT0;
#pragma  ioport   RDI0LT1:    0xB4  // Row 0 Look Up Table Register 1
BYTE              RDI0LT1;
#pragma  ioport   RDI0RO0:    0xB5  // Row 0 Output Register 0
BYTE              RDI0RO0;
#pragma  ioport   RDI0RO1:    0xB6  // Row 0 Output Register 1
BYTE              RDI0RO1;

//-----------------------------------------------
//  Ram Page Pointers
//-----------------------------------------------
#pragma  ioport   CUR_PP:     0x0D0  // Current   Page Pointer
BYTE              CUR_PP;
#pragma  ioport   STK_PP:     0x0D1  // Stack     Page Pointer
BYTE              STK_PP;
#pragma  ioport   IDX_PP:     0x0D3  // Index     Page Pointer
BYTE              IDX_PP;
#pragma  ioport   MVR_PP:     0x0D4  // MVI Read  Page Pointer
BYTE              MVR_PP;
#pragma  ioport   MVW_PP:     0x0D5  // MVI Write Page Pointer
BYTE              MVW_PP;

//-----------------------------------------------
//  I2C Configuration Registers
//-----------------------------------------------
#pragma  ioport   I2C_CFG:    0x0D6  // I2C Configuration Register
BYTE              I2C_CFG;
#define I2C_CFG_PINSEL         (0x40)
#define I2C_CFG_BUSERR_IE      (0x20)
#define I2C_CFG_STOP_IE        (0x10)
#define I2C_CFG_CLK_RATE_100K  (0x00)
#define I2C_CFG_CLK_RATE_400K  (0x04)
#define I2C_CFG_CLK_RATE_50K   (0x08)
#define I2C_CFG_CLK_RATE       (0x0C)
#define I2C_CFG_PSELECT_MASTER (0x02)
#define I2C_CFG_PSELECT_SLAVE  (0x01)

#pragma  ioport   I2C_SCR:    0x0D7  // I2C Status and Control Register
BYTE              I2C_SCR;
#define I2C_SCR_BUSERR       (0x80)
#define I2C_SCR_LOSTARB      (0x40)
#define I2C_SCR_STOP         (0x20)
#define I2C_SCR_ACK          (0x10)
#define I2C_SCR_ADDR         (0x08)
#define I2C_SCR_XMIT         (0x04)
#define I2C_SCR_LRB          (0x02)
#define I2C_SCR_BYTECOMPLETE (0x01)

#pragma  ioport   I2C_DR:     0x0D8  // I2C Data Register
BYTE              I2C_DR;

#pragma  ioport   I2C_MSCR:   0x0D9  // I2C Master Status and Control Register
BYTE              I2C_MSCR;
#define I2C_MSCR_BUSY    (0x08)
#define I2C_MSCR_MODE    (0x04)
#define I2C_MSCR_RESTART (0x02)
#define I2C_MSCR_START   (0x01)

//-----------------------------------------------
//  System and Global Resource Registers
//-----------------------------------------------
#pragma  ioport   INT_CLR0:   0x0DA  // Interrupt Clear Register 0
BYTE              INT_CLR0;
#pragma  ioport   INT_CLR1:   0x0DB  // Interrupt Clear Register 1
BYTE              INT_CLR1;
#pragma  ioport   INT_CLR3:   0x0DD  // Interrupt Clear Register 3
BYTE              INT_CLR3;

#pragma  ioport   INT_MSK3:   0x0DE  // I2C and Software Mask Register
BYTE              INT_MSK3;
#define INT_MSK3_ENSWINT          (0x80)
#define INT_MSK3_I2C              (0x01)

#pragma  ioport   INT_MSK0:   0x0E0  // General Interrupt Mask Register
BYTE              INT_MSK0;
#define  INT_MSK0_VC3             (0x80)
#define  INT_MSK0_SLEEP           (0x40)
#define  INT_MSK0_GPIO            (0x20)
#define  INT_MSK0_ACOLUMN_1       (0x04)
#define  INT_MSK0_ACOLUMN_0       (0x02)
#define  INT_MSK0_VOLTAGE_MONITOR (0x01)

#pragma  ioport   INT_MSK1:   0x0E1  // Digital PSoC block Mask Register
BYTE              INT_MSK1;
#define  INT_MSK1_DCB03           (0x08)
#define  INT_MSK1_DCB02           (0x04)
#define  INT_MSK1_DBB01           (0x02)
#define  INT_MSK1_DBB00           (0x01)

#pragma  ioport   INT_VC:     0x0E2  // Interrupt vector register
BYTE              INT_VC;

#pragma  ioport   RES_WDT:    0x0E3  // Watch Dog Timer
BYTE              RES_WDT;

// DECIMATOR Control Registers
#pragma  ioport   DEC_CR0:    0x0E6  // Data Control Register
BYTE              DEC_CR0;
#pragma  ioport   DEC_CR1:    0x0E7  // Data Control Register
BYTE              DEC_CR1;

#pragma  ioport   DAC_D:      0x0FD   // DAC Data Register
BYTE              DAC_D;

//-----------------------------------------------
//  System Status and Control Register
//
//  Note: the following register is mapped into
//  both register bank 0 AND register bank 1.
//-----------------------------------------------
#pragma  ioport   CPU_F:      0xF7   // CPU Flag Register Access
BYTE              CPU_F;             // Use FLAG_ masks defined at top of file

#pragma  ioport   CPU_SCR1:   0xFE   // System Status and Control Register 1
BYTE              CPU_SCR1;
#define  CPU_SCR1_IRESS        (0x80)
#define  CPU_SCR1_SLIMO        (0x10)
#define  CPU_SCR1_ECO_ALWD_WR  (0x08)
#define  CPU_SCR1_ECO_ALLOWED  (0x04)
#define  CPU_SCR1_IRAMDIS      (0x01)

#pragma  ioport   CPU_SCR0:   0xFF  // System Status and Control Register 0
BYTE              CPU_SCR0;
#define  CPU_SCR0_GIE_MASK     (0x80)
#define  CPU_SCR0_WDRS_MASK    (0x20)
#define  CPU_SCR0_PORS_MASK    (0x10)
#define  CPU_SCR0_SLEEP_MASK   (0x08)
#define  CPU_SCR0_STOP_MASK    (0x01)


//=============================================================================
//=============================================================================
//      Register Space, Bank 1
//=============================================================================
//=============================================================================


//-----------------------------------------------
//  Port Registers
//  Note: Also see this address range in Bank 0.
//-----------------------------------------------
// Port 0
#pragma  ioport   PRT0DM0:    0x100  // Port 0 Drive Mode 0
BYTE              PRT0DM0;
#pragma  ioport   PRT0DM1:    0x101  // Port 0 Drive Mode 1
BYTE              PRT0DM1;
#pragma  ioport   PRT0IC0:    0x102  // Port 0 Interrupt Control 0
BYTE              PRT0IC0;
#pragma  ioport   PRT0IC1:    0x103  // Port 0 Interrupt Control 1
BYTE              PRT0IC1;
// Port 1
#pragma  ioport   PRT1DM0:    0x104  // Port 1 Drive Mode 0
BYTE              PRT1DM0;
#pragma  ioport   PRT1DM1:    0x105  // Port 1 Drive Mode 1
BYTE              PRT1DM1;
#pragma  ioport   PRT1IC0:    0x106  // Port 1 Interrupt Control 0
BYTE              PRT1IC0;
#pragma  ioport   PRT1IC1:    0x107  // Port 1 Interrupt Control 1
BYTE              PRT1IC1;
// Port 2
#pragma  ioport   PRT2DM0:    0x108  // Port 2 Drive Mode 0
BYTE              PRT2DM0;
#pragma  ioport   PRT2DM1:    0x109  // Port 2 Drive Mode 1
BYTE              PRT2DM1;
#pragma  ioport   PRT2IC0:    0x10A  // Port 2 Interrupt Control 0
BYTE              PRT2IC0;
#pragma  ioport   PRT2IC1:    0x10B  // Port 2 Interrupt Control 1
BYTE              PRT2IC1;
// Port 3
#pragma  ioport   PRT3DM0:    0x10C  // Port 3 Drive Mode 0
BYTE              PRT3DM0;
#pragma  ioport   PRT3DM1:    0x10D  // Port 3 Drive Mode 1
BYTE              PRT3DM1;
#pragma  ioport   PRT3IC0:    0x10E  // Port 3 Interrupt Control 0
BYTE              PRT3IC0;
#pragma  ioport   PRT3IC1:    0x10F  // Port 3 Interrupt Control 1
BYTE              PRT3IC1;

//-----------------------------------------------
//  Digital PSoC(tm) block Registers
//  Note: Also see this address range in Bank 1.
//-----------------------------------------------

// Digital PSoC block 00, Basic Type B
#pragma  ioport   DBB00FN:    0x120  // Function Register
BYTE              DBB00FN;
#pragma  ioport   DBB00IN:    0x121  //    Input Register
BYTE              DBB00IN;
#pragma  ioport   DBB00OU:    0x122  //   Output Register
BYTE              DBB00OU;

// Digital PSoC block 01, Basic Type B
#pragma  ioport   DBB01FN:    0x124  // Function Register
BYTE              DBB01FN;
#pragma  ioport   DBB01IN:    0x125  //    Input Register
BYTE              DBB01IN;
#pragma  ioport   DBB01OU:    0x126  //   Output Register
BYTE              DBB01OU;

// Digital PSoC block 02, Communications Type B
#pragma  ioport   DCB02FN:    0x128  // Function Register
BYTE              DCB02FN;
#pragma  ioport   DCB02IN:    0x129  //    Input Register
BYTE              DCB02IN;
#pragma  ioport   DCB02OU:    0x12A  //   Output Register
BYTE              DCB02OU;

// Digital PSoC block 03, Communications Type B
#pragma  ioport   DCB03FN:    0x12C  // Function Register
BYTE              DCB03FN;
#pragma  ioport   DCB03IN:    0x12D  //    Input Register
BYTE              DCB03IN;
#pragma  ioport   DCB03OU:    0x12E  //   Output Register
BYTE              DCB03OU;

//-----------------------------------------------
//  System and Global Resource Registers
//  Note: Also see this address range in Bank 0.
//-----------------------------------------------

#pragma  ioport   CLK_CR0:    0x160  // Analog Column Clock Select Register
BYTE              CLK_CR0;
#define CLK_CR0_ACOLUMN_1             (0x0C)
#define CLK_CR0_ACOLUMN_0             (0x03)

#pragma  ioport   CLK_CR1:    0x161  // Analog Clock Source Select Register
BYTE              CLK_CR1;
#define CLK_CR1_ACLK1                 (0x18)
#define CLK_CR1_ACLK0                 (0x03)

#define CLK_CR1_ACLK2                 (0x07)  //DEPRECATED SHOULD NOT BE USED


#pragma  ioport   ABF_CR0:    0x162  // Analog Output Buffer Control Register
BYTE              ABF_CR0;
#define ABF_CR0_ACOL1MUX              (0x80)

#pragma  ioport   AMD_CR0:    0x163  // Analog Modulator Control Register
BYTE              AMD_CR0;
#define AMD_CR0_AMOD0                 (0x0F)

#pragma  ioport   CMP_GO_EN:  0x164  // Comparator Bus To Global Out Enable
BYTE              CMP_GO_EN;
#define CMP_GO_EN_GOO5                (0x80)
#define CMP_GO_EN_GOO1                (0x40)
#define CMP_GO_EN_SEL1                (0x30)
#define CMP_GO_EN_GOO4                (0x08)
#define CMP_GO_EN_GOO0                (0x04)
#define CMP_GO_EN_SEL0                (0x03)

#pragma  ioport   AMD_CR1:    0x166  // Analog Modulator Control Register 1
BYTE              AMD_CR1;
#define AMD_CR1_AMOD1                 (0x0F)

#pragma  ioport   ALT_CR0:    0x167  // Analog Look Up Table (LUT) Register 0
BYTE              ALT_CR0;
#define ALT_CR0_LUT1                  (0xF0)
#define ALT_CR0_LUT0                  (0x0F)

#pragma  ioport   CLK_CR3:    0x16B  // Analog Clock Source Control 3
BYTE              CLK_CR3;
#define CLK_CR3_SYS1                   (0x40)
#define CLK_CR3_DIVCLK1                (0x30)
#define CLK_CR3_SYS0                   (0x04)
#define CLK_CR3_DIVCLK0                (0x03)

//-----------------------------------------------
//  Global Digital Interconnects
//-----------------------------------------------

#pragma  ioport   GDI_O_IN:   0x1D0  // Global Dig Interconnect Odd Inputs
BYTE              GDI_O_IN;
#pragma  ioport   GDI_E_IN:   0x1D1  // Global Dig Interconnect Even Inputs
BYTE              GDI_E_IN;
#pragma  ioport   GDI_O_OU:   0x1D2  // Global Dig Interconnect Odd Outputs
BYTE              GDI_O_OU;
#pragma  ioport   GDI_E_OU:   0x1D3  // Global Dig Interconnect Even Outputs
BYTE              GDI_E_OU;

#pragma  ioport   MUX_CR0:    0x1D8  // Analog Mux Port 0 Bit Enables
BYTE              MUX_CR0;
#pragma  ioport   MUX_CR1:    0x1D9  // Analog Mux Port 1 Bit Enables
BYTE              MUX_CR1;		  
#pragma  ioport   MUX_CR2:    0x1DA  // Analog Mux Port 2 Bit Enables
BYTE              MUX_CR2;
#pragma  ioport   MUX_CR3:    0x1DB  // Analog Mux Port 3 Bit Enables
BYTE              MUX_CR3;

//------------------------------------------------
//  Clock and System Control Registers
//------------------------------------------------

#pragma  ioport   OSC_GO_EN:  0x1DD  // Oscillator to Global Outputs Enable Register (RW)
BYTE              OSC_GO_EN;
#define OSC_GOEN_SLPINT               (0x80)
#define OSC_GOEN_VC3                  (0x40)
#define OSC_GOEN_VC2                  (0x20)
#define OSC_GOEN_VC1                  (0x10)
#define OSC_GOEN_SYSCLKX2             (0x08)
#define OSC_GOEN_SYSCLK               (0x04)
#define OSC_GOEN_CLK24M               (0x02)
#define OSC_GOEN_CLK32K               (0x01)

#pragma  ioport   OSC_CR4:    0x1DE  // Oscillator Control Register 4
BYTE              OSC_CR4;
#define OSC_CR4_VC3SEL                (0x03)

#pragma  ioport   OSC_CR3:    0x1DF  // Oscillator Control Register 3
BYTE              OSC_CR3;

#pragma  ioport   OSC_CR0:    0x1E0  // System Oscillator Control Register 0
BYTE              OSC_CR0;
#define OSC_CR0_32K_SELECT            (0x80)
#define OSC_CR0_PLL_MODE              (0x40)
#define OSC_CR0_NO_BUZZ               (0x20)
#define OSC_CR0_SLEEP                 (0x18)
#define OSC_CR0_SLEEP_512Hz           (0x00)
#define OSC_CR0_SLEEP_64Hz            (0x08)
#define OSC_CR0_SLEEP_8Hz             (0x10)
#define OSC_CR0_SLEEP_1Hz             (0x18)
#define OSC_CR0_CPU                   (0x07)
#define OSC_CR0_CPU_3MHz              (0x00)
#define OSC_CR0_CPU_6MHz              (0x01)
#define OSC_CR0_CPU_12MHz             (0x02)
#define OSC_CR0_CPU_24MHz             (0x03)
#define OSC_CR0_CPU_1d5MHz            (0x04)
#define OSC_CR0_CPU_750kHz            (0x05)
#define OSC_CR0_CPU_187d5kHz          (0x06)
#define OSC_CR0_CPU_93d7kHz           (0x07)

#pragma  ioport   OSC_CR1:    0x1E1  // System V1/V2 Divider Control Register
BYTE              OSC_CR1;
#define OSC_CR1_VC1                   (0xF0)
#define OSC_CR1_VC2                   (0x0F)

#pragma  ioport   OSC_CR2:    0x1E2  // Oscillator Control Register 2
BYTE              OSC_CR2;
#define OSC_CR2_PLLGAIN               (0x80)
#define OSC_CR2_EXTCLKEN              (0x04)
#define OSC_CR2_IMODIS                (0x02)
#define OSC_CR2_SYSCLKX2DIS           (0x01)

#pragma  ioport   VLT_CR:     0x1E3  // Voltage Monitor Control Register
BYTE              VLT_CR;
#define VLT_CR_SMP                    (0x80)
#define VLT_CR_PORLEV                 (0x30)
#define VLT_CR_POR_LOW                (0x00)
#define VLT_CR_POR_MID                (0x10)
#define VLT_CR_POR_HIGH               (0x20)
#define VLT_CR_LVDTBEN                (0x08)
#define VLT_CR_VM                     (0x07)

#pragma  ioport   VLT_CMP:    0x1E4  // Voltage Monitor Comparators Register
BYTE              VLT_CMP;
#define VLT_CMP_NOWRITE               (0x08)
#define VLT_CMP_PUMP                  (0x04)
#define VLT_CMP_LVD                   (0x02)
#define VLT_CMP_PPOR                  (0x01)

#pragma  ioport   ADC0_TR:    0x1E5  // ADC Column 0 Trim Register
BYTE              ADC0_TR;
#pragma  ioport   ADC1_TR:    0x1E6  // ADC Column 1 Trim Register
BYTE              ADC1_TR;

#pragma  ioport   IMO_TR:     0x1E8  // Internal Main Oscillator Trim Register
BYTE              IMO_TR;
#pragma  ioport   ILO_TR:     0x1E9  // Internal Low-speed Oscillator Trim
BYTE              ILO_TR;
#pragma  ioport   BDG_TR:     0x1EA  // Band Gap Trim Register
BYTE              BDG_TR;
#pragma  ioport   ECO_TR:     0x1EB  // External Oscillator Trim Register
BYTE              ECO_TR;

#pragma  ioport   FLS_PR1:    0x1FA  // Voltage Monitor Comparators Register
BYTE              FLS_PR1;
#define FLS_PR1_BANK                  (0x03)

#pragma  ioport   DAC_CR:     0x1FD   // DAC Control Register
BYTE              DAC_CR;
#define DAC_CR_IRANGE                  (0x08)
#define DAC_CR_OSCMODE                 (0x06)
#define DAC_CR_ENABLE                  (0x01)

//=============================================================================
//=============================================================================
//      M8C System Macros
//=============================================================================
//=============================================================================


//-----------------------------------------------
//  Swapping Register Banks
//-----------------------------------------------
#define  M8C_SetBank0            asm("and F, EFh")
#define  M8C_SetBank1            asm("or  F, 10h")

//-----------------------------------------------
//  Global Interrupt Enable/Disable
//-----------------------------------------------
#define  M8C_EnableGInt          asm("or  F, 01h")
#define  M8C_DisableGInt         asm("and F, FEh")

//---------------------------------------------------
// Enable/Disable Interrupt Mask
//
// Usage:    M8C_DisableIntMask INT_MSKN, MASK
//           M8C_EnableIntMask  INT_MSKN, MASK
//
// where INT_MSKN is INT_MSK0, INT_MSK1 or INT_MSK3
//       and MASK is the bit set to enable or disable
//---------------------------------------------------
// Disable Interrupt Bit Mask(s)
#define M8C_DisableIntMask( INT_MSKN_REG, MASK )   (INT_MSKN_REG &= ~MASK)

// Enable Interrupt Bit Mask(s)
#define M8C_EnableIntMask( INT_MSKN_REG, MASK )    (INT_MSKN_REG |= MASK)

//---------------------------------------------------
// Clear Posted Interrupt Flag
//
// Usage:    M8C_ClearIntFlag   INT_CLRN, MASK
//
// where INT_MSKN is INT_CLR0, INT_CLR1 or INT_CLR3
//       and MASK is the bit set to enable or disable
//---------------------------------------------------
#define M8C_ClearIntFlag( INT_CLRN_REG, MASK )    (INT_CLRN_REG = ~MASK)


//-----------------------------------------------
//  Power-On Reset & WatchDog Timer Functions
//-----------------------------------------------
#define  M8C_EnableWatchDog      (CPU_SCR0 &= ~CPU_SCR0_PORS_MASK)
#define  M8C_ClearWDT            (RES_WDT = 0x00)
#define  M8C_ClearWDTAndSleep    (RES_WDT = 0x38)


//-----------------------------------------------
//  Sleep, CPU Stop & Software Reset
//-----------------------------------------------
#define  M8C_Sleep               (CPU_SCR0 |= CPU_SCR0_SLEEP_MASK)
#define  M8C_Stop                (CPU_SCR0 |= CPU_SCR0_STOP_MASK)

#define  M8C_Reset               asm("mov A, 0"); \
                                 asm("SSC")


//-----------------------------------------------
// ImageCraft Code Compressor Actions
//-----------------------------------------------

    // Suspend Code Compressor
    // Must not span a RET or RETI instruction
    // without resuming code compression

#define Suspend_CodeCompressor  asm("or F, 0")

    // Resume Code Compressor
#define Resume_CodeCompressor   asm("add SP,0")

#endif


// end of file m8c.h
