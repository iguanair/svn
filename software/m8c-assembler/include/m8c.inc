;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  M8C.INC -- CY7C63300 Microcontroller Family System Declarations
;;;
;;;  Copyright (c) 2004, 2005 Cypress Semiconductor, Inc. All rights reserved.
;;;
;;;
;;;  This file provides address constants, bit field masks and a set of macro
;;;  facilities for the Cypress Semiconductor CY7C63300 Microcontroller family.
;;;
;;;  Last Modified:
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;=============================================================================
;; Definition of abbreviations used in the descriptions below
;;  (RW)   The register or bit supports reads and writes
;;  (W)    The register or bit is write-only
;;  (R)    The register or bit is read-only
;;  (#)    Access to the register is bit specific (see the family datasheet)
;;  (RC)   The register or bit can be read, but writing a 0 will clear it,
;;         writing a 1 will have no effect.
;;=============================================================================

;;=============================================================================
;;      System Registers
;;=============================================================================

;----------------------------
;  Flag Register Bit Fields
;----------------------------
FLAG_XIO_MASK:  equ 10h
FLAG_SUPER:     equ 08h
FLAG_CARRY:     equ 04h
FLAG_ZERO:      equ 02h
FLAG_GLOBAL_IE: equ 01h

;;=============================================================================
;;      Register Space, Bank 0
;;=============================================================================

;------------------------------------------------
;  Port Registers
;------------------------------------------------
; Port Data Registers
P0DATA:       equ 00h          ; Port 0 Data Register                     (RW)
P1DATA:       equ 01h          ; Port 1 Data Register                     (RW)
P2DATA:       equ 02h          ; Port 2 Data Register                     (RW)
P3DATA:       equ 03h          ; Port 3 Data Register                     (RW)
P4DATA:       equ 04h          ; Port 4 Data Register                     (RW)
; PSoC Compatability
PRT0DR:       equ 00h          ; Port 0 Data Register                     (RW)(PSoC)
PRT1DR:       equ 01h          ; Port 1 Data Register                     (RW)(PSoC)
PRT2DR:       equ 02h          ; Port 2 Data Register                     (RW)(PSoC)
PRT3DR:       equ 03h          ; Port 3 Data Register                     (RW)(PSoC)
PRT4DR:       equ 04h          ; Port 4 Data Register                     (RW)(PSoC)

; Port/Pin Configuration Registers
P00CR:        equ 05h          ; P0.0 Configuration Register              (RW)
P01CR:        equ 06h          ; P0.1 Configuration Register              (RW)
P02CR:        equ 07h          ; P0.2 Configuration Register              (RW)
P03CR:        equ 08h          ; P0.3 Configuration Register              (RW)
P04CR:        equ 09h          ; P0.4 Configuration Register              (RW)
P05CR:        equ 0Ah          ; P0.5 Configuration Register              (RW)
P06CR:        equ 0Bh          ; P0.6 Configuration Register              (RW)
P07CR:        equ 0Ch          ; P0.7 Configuration Register              (RW)
P10CR:        equ 0Dh          ; P1.0 Configuration Register              (RW)
P11CR:        equ 0Eh          ; P1.1 Configuration Register              (RW)
P12CR:        equ 0Fh          ; P1.2 Configuration Register              (RW)
P13CR:        equ 10h          ; P1.3 Configuration Register              (RW)
P14CR:        equ 11h          ; P1.4 Configuration Register              (RW)
P15CR:        equ 12h          ; P1.5 Configuration Register              (RW)
P16CR:        equ 13h          ; P1.6 Configuration Register              (RW)
P17CR:        equ 14h          ; P1.7 Configuration Register              (RW)
P2CR:         equ 15h          ; P2.0-P2.7 Configuration Register         (RW)
P3CR:         equ 16h          ; P3.0-P3.7 Configuration Register         (RW)
P4CR:         equ 17h          ; P4.0-P4.7 Configuration Register         (RW)

; Timer Registers
FRTMRL:       equ 20h          ; Free Running Timer Low                   (RW)
FRTMRH:       equ 21h          ; Free Running Timer High                  (RW)
TCAP0R:       equ 22h          ; Capture 0 Rising                         (R)             
TCAP1R:       equ 23h          ; Capture 1 Rising                         (R)             
TCAP0F:       equ 24h          ; Capture 0 Falling                        (R)             
TCAP1F:       equ 25h          ; Capture 1 Falling                        (R)             
PITMRL:       equ 26h          ; Programmable Interval Timer Low          (RW)
PITMRH:       equ 27h          ; Programmable Interval Timer High         (RW)
PIRL:         equ 28h          ; Programmable Interval Timer Reload Low   (RW)
PIRH:         equ 29h          ; Programmable Interval Timer Reload High  (RW)
TMRCR:        equ 2Ah          ; Timer Configuration Register             (RW)
TCAPINTE:     equ 2Bh          ; Capture Timer Interrupt Enable           (RW)
TCAPINTS:     equ 2Ch          ; Capture Timer Interrupt Status           (RW)

; Clock Configuration Registers
CPUCLKCR:     equ 30h          ; CPU Clock Configuration Register         (RW)
CPUCLK_SEL_INT:         equ 00h ; CPU Clock Select Internal Oscillator
CPUCLK_SEL_EXT:         equ 01h ; CPU Clock Select External Clock
CPUCLK_USBCLK_SEL_INT:  equ 00h ; USB Clock Select Internal Clock
CPUCLK_USBCLK_SEL_EXT:  equ 40h ; USB Clock Select External Clock
CPUCLK_USBCLK_DIV2_DIS: equ 00h ; USB Clock Divide by 2 disable
CPUCLK_USBCLK_DIV2_ENA: equ 80h ; USB Clock Divide by 2 enable

TMRCLKCR:     equ 31h          ; Timer Clock Configuration Register       (RW)
TMRCLKCR_ITMRCLK_IOSC:     equ 00h ; MASK: ITMRCLK Source--Internal Oscillator
TMRCLKCR_ITMRCLK_XOSC:     equ 01h ; MASK: ITMRCLK Source--External Oscillator or CLKIN
TMRCLKCR_ITMRCLK_LPO:      equ 02h ; MASK: ITMRCLK Source--Low Power Oscillator (32 Khz)
TMRCLKCR_ITMRCLK_TCAPCLK:  equ 03h ; MASK: ITMRCLK Source--TCAPCLK
TMRCLKCR_ITMRCLK_DIV_1:    equ 00h ; MASK: ITMRCLK Divider Value 1
TMRCLKCR_ITMRCLK_DIV_2:    equ 04h ; MASK: ITMRCLK Divider Value 2
TMRCLKCR_ITMRCLK_DIV_3:    equ 08h ; MASK: ITMRCLK Divider Value 3
TMRCLKCR_ITMRCLK_DIV_4:    equ 0Ch ; MASK: ITMRCLK Divider Value 4
TMRCLKCR_ITMRCLK_LPO:      equ 02h ; MASK: ITMRCLK Source--Low Power Oscillator (32 Khz)
TMRCLKCR_TCAPCLK_IOSC:     equ 00h ; MASK: TCAPCLK Source--Internal Oscillator
TMRCLKCR_TCAPCLK_XOSC:     equ 10h ; MASK: TCAPCLK Source--External Oscillator or CLKIN
TMRCLKCR_TCAPCLK_LPO:      equ 20h ; MASK: TCAPCLK Source--Low Power Oscillator (32 Khz)
TMRCLKCR_TCAPCLK_DISABLED: equ 30h ; MASK: TCAPCLK Source--DISABLED
TMRCLKCR_TCAPCLK_DIV_2:    equ 00h ; MASK: TCAPCLK Divider Value 2
TMRCLKCR_TCAPCLK_DIV_4:    equ 40h ; MASK: TCAPCLK Divider Value 4
TMRCLKCR_TCAPCLK_DIV_6:    equ 80h ; MASK: TCAPCLK Divider Value 6
TMRCLKCR_TCAPCLK_DIV_8:    equ C0h ; MASK: TCAPCLK Divider Value 8

CLKIOCR:      equ 32h          ; Clock I/O Configuration Register         (RW)
CLKIOCR_CLKOUT_IOSC:       equ 00h ; MASK: Clock Source--Internal Oscillator
CLKIOCR_CLKOUT_XOSC:       equ 01h ; MASK: Clock Source--External Oscillator or CLKIN
CLKIOCR_CLKOUT_LPO:        equ 02h ; MASK: Clock Source--Low Power Oscillator (32 Khz)
CLKIOCR_CLKOUT_CPUCLK:     equ 03h ; MASK: Clock Source--CPUCLK

; Oscillator Configuration Registers
IOSCTR:       equ 34h          ; Internal Oscillator Trim Register        (R)
XOSCTR:       equ 35h          ; Crystal Oscillator Trim Register         (R)
LPOSCTR:      equ 36h          ; Low Power Oscillator Trim Register       (RW)

; SPI Configuration/Data Registers
SPIDATA:      equ 3Ch          ; SPI Data Register                        (RW)
SPICR:        equ 3Dh          ; SPI Configuration Register               (RW)

; USB SIE Configuration/Data Registers
USBCR:        equ 40h          ; USB Configuration Register               (RW)
EP0CNT:       equ 41h          ; Endpoint 0 Count Register                (RW)
EP1CNT:       equ 42h          ; Endpoint 1 Count Register                (RW)
EP2CNT:       equ 43h          ; Endpoint 2 Count Register                (RW)
EP0MODE:      equ 44h          ; Endpoint 0 Mode  Register                (RW)
EP1MODE:      equ 45h          ; Endpoint 1 Mode  Register                (RW)
EP2MODE:      equ 46h          ; Endpoint 2 Mode  Register                (RW)
EP0DATA:      equ 50h          ; Endpoint 0 Data  Register (50h-57h)      (RW)
EP1DATA:      equ 58h          ; Endpoint 1 Data  Register (58h-5Fh)      (RW)
EP2DATA:      equ 60h          ; Endpoint 2 Data  Register (60h-67h)      (RW)

; Band-gap/TRIMBUF Configuration Registers
BGAPTR:       equ 70h          ; Band-gap Trim Register                   (R)
TRIM0:        equ 71h          ; TRIMBUF Trim Register 0                  (R)
TRIM1:        equ 72h          ; TRIMBUF Trim Register 1                  (R)

; VREG Configuration Register
VREGCR:       equ 73h          ; VREG Configuration Register              (RW)

; USB Transceiver Configuration Registers
USBXCR:       equ 74h          ; USB Transceiver Configuration Register   (RW)
USBXCR_ENABLE:    equ 80h      ; USB Transceiver Enable
USBXCR_FORCE:     equ 01h      ; USB Transceiver Force

; Data Pointer Registers--Listed for compatability with other M8C based parts.
CPPDR:        equ D0h          ; Current Page Pointer Data Register       (RW)
DPRDR:        equ D4h          ; Data Page Read Register                  (RW)
DPWDR:        equ D5h          ; Data Page Write Register                 (RW)

; Watchdog Timer Reset
RESWDT:       equ E3h          ; Watchdog Timer Reset                     (W)
RES_WDT:      equ E3h          ; WatchDog Timer Register                  (W) (PSoC)

;------------------------------------------------
;  System and Global Resource Registers
;------------------------------------------------
INT_CLR0:     equ DAh          ; Interrupt Clear Register 0               (RW)
                               ; Use INT_MSK0 bit field masks
INT_CLR1:     equ DBh          ; Interrupt Clear Register 1               (RW)
                               ; Use INT_MSK1 bit field masks
INT_CLR2:     equ DCh          ; Interrupt Clear Register 2               (RW)
                               ; Use INT_MSK2 bit field masks

INT_MSK3:     equ DEh          ; Interrupt Mask Register                  (RW)
INT_MSK3_ENSWINT:          equ 80h ; MASK: enable/disable SW interrupt

INT_MSK2:     equ DFh          ; Interrupt Mask Register                  (RW)
INT_MSK2_GPIO_PORT4:       equ 40h ; MASK: enable/disable GPIO Port 4 interrupt
INT_MSK2_GPIO_PORT3:       equ 20h ; MASK: enable/disable GPIO Port 3 interrupt
INT_MSK2_GPIO_PORT2:       equ 10h ; MASK: enable/disable GPIO Port 2 interrupt
INT_MSK2_PS2_DATA_LOW:     equ 08h ; MASK: enable/disable PS/2 Data Low
INT_MSK2_GPIO_INT2:        equ 04h ; MASK: enable/disable GPIO INT2 interrupt
INT_MSK2_CTR_16_WRAP:      equ 02h ; MASK: enable/disable 16 bit counter wrap
INT_MSK2_TCAP1:            equ 01h ; MASK: enable/disable Timer/Capture 0 interrupt

INT_MSK0:     equ E0h          ; Interrupt Mask Register                 (RW)
INT_MSK0_GPIO_PORT1:       equ 80h ; MASK: enable/disable GPIO Port 1 interrupt
INT_MSK0_SLEEP:            equ 40h ; MASK: enable/disable sleep interrupt
INT_MSK0_GPIO_INT1:        equ 20h ; MASK: enable/disable GPIO INT1 interrupt
INT_MSK0_GPIO_PORT0:       equ 10h ; MASK: enable/disable GPIO Port 0 interrupt
INT_MSK0_SPI_RX:           equ 08h ; MASK: enable/disable SPI Receive interrupt
INT_MSK0_SPI_TX:           equ 04h ; MASK: enable/disable SPI Transmit interrupt
INT_MSK0_GPIO_INT0:        equ 02h ; MASK: enable/disable GPIO INT0 interrupt
INT_MSK0_POR_LVD:          equ 01h ; MASK: enable/disable POR/LVD interrupt

INT_MSK1:     equ E1h          ; Interrupt Mask Register                 (RW)
INT_MSK1_TCAP0:            equ 80h ; MASK: enable/disable Timer/Capture 0 interrupt
INT_MSK1_PIT:              equ 40h ; MASK: enable/disable Progrmmable Interval Timer
INT_MSK1_MS_TIMER:         equ 20h ; MASK: enable/disable One Millisecond Timer interrupt
INT_MSK1_USB_ACTIVITY:     equ 10h ; MASK: enable/disable USB Bus Activity interrupt
INT_MSK1_USB_BUS_RESET:    equ 08h ; MASK: enable/disable USB Bus Reset interrupt
INT_MSK1_USB_EP2:          equ 04h ; MASK: enable/disable USB Endpoint 2 interrupt
INT_MSK1_USB_EP1:          equ 02h ; MASK: enable/disable USB Endpoint 1 interrupt
INT_MSK1_USB_EP0:          equ 01h ; MASK: enable/disable USB Endpoint 0 interrupt

INT_VC:       equ E2h          ; Interrupt vector register                (RC)

;------------------------------------------------------
;        Register bank 1.
;------------------------------------------------------
OSC_CR0:                   equ E0h  ; System Oscillator Control Register 0     (RW)
OSC_CR0_NO_BUZZ:           equ 20h    ; MASK: Bandgap always powered/BUZZ bandgap
OSC_CR0_SLEEP:             equ 18h    ; MASK: Set Sleep timer freq/period
OSC_CR0_SLEEP_512Hz:       equ 00h    ;     Set sleep bits for 1.95ms period
OSC_CR0_SLEEP_64Hz:        equ 08h    ;     Set sleep bits for 15.6ms period
OSC_CR0_SLEEP_8Hz:         equ 10h    ;     Set sleep bits for 125ms period
OSC_CR0_SLEEP_1Hz:         equ 18h    ;     Set sleep bits for 1 sec period

OSC_CR0_CPU:               equ 07h    ; MASK: Set CPU Frequency
OSC_CR0_CPU_3MHz:          equ 00h    ;     set CPU Freq bits for 3MHz Operation
OSC_CR0_CPU_6MHz:          equ 01h    ;     set CPU Freq bits for 6MHz Operation
OSC_CR0_CPU_12MHz:         equ 02h    ;     set CPU Freq bits for 12MHz Operation
OSC_CR0_CPU_24MHz:         equ 03h    ;     set CPU Freq bits for 24MHz Operation
OSC_CR0_CPU_1d5MHz:        equ 04h    ;     set CPU Freq bits for 1.5MHz Operation
OSC_CR0_CPU_750kHz:        equ 05h    ;     set CPU Freq bits for 750kHz Operation
OSC_CR0_CPU_187d5kHz:      equ 06h    ;     set CPU Freq bits for 187.5kHz Operation

;------------------------------------------------------
;  Note: The following registers are mapped into both
;        register bank 0 AND register bank 1.
;------------------------------------------------------
CPU_F:        equ F7h          ; CPU Flag Register Access                 (RO)
                                   ; Use FLAG_ masks defined at top of file
CPU_SCR:     equ FFh          ; CPU Status and Control Register          (#)
CPU_SCR_GIE_MASK:      equ 80h    ; MASK: Global Interrupt Enable shadow
CPU_SCR_WDRS_MASK:     equ 20h    ; MASK: Watch Dog Timer Reset
CPU_SCR_PORS_MASK:     equ 10h    ; MASK: power-on reset bit PORS
CPU_SCR_SLEEP_MASK:    equ 08h    ; MASK: Enable Sleep
CPU_SCR_STOP_MASK:     equ 01h    ; MASK: Halt CPU bit

;;=============================================================================
;;      Register Space, Bank 1
;;=============================================================================

;------------------------------------------------
;  Clock and System Control Registers
;------------------------------------------------

;;=============================================================================
;;      M8C System Macros
;;  These macros should be used when their functions are needed.
;;=============================================================================

;----------------------------------------------------
;  Swapping Register Banks
;----------------------------------------------------
    macro M8C_SetBank0
    and   F, ~FLAG_XIO_MASK
    endm

    macro M8C_SetBank1
    or    F, FLAG_XIO_MASK
    endm

;----------------------------------------------------
;  Global Interrupt Enable/Disable
;----------------------------------------------------
    macro M8C_EnableGInt
    or    F, FLAG_GLOBAL_IE
    endm

    macro M8C_DisableGInt
    and   F, ~FLAG_GLOBAL_IE
    endm

;----------------------------------------------------
;  Enable/Disable Interrupt Mask
;
;  Use the following macros to enable/disable
;  bits in the Interrupt mask registers,
;  INT_MSK0, INT_MSK1 or INT_MSK3.
;
;  Usage:    M8C_DisableIntMask INT_MSKN, MASK
;            M8C_EnableIntMask  INT_MSKN, MASK
;
;  where INT_MSKN is INT_MSK0, INT_MSK1 or INT_MSK3
;        and MASK is the bit set to enable or disable
;----------------------------------------------------
; Disable Interrupt Bit Mask(s)
    macro M8C_DisableIntMask
    and   reg[@0], ~@1              ; disable specified interrupt enable bit
    endm

; Enable Interrupt Bit Mask(s)
    macro M8C_EnableIntMask
    or    reg[@0], @1               ; enable specified interrupt enable bit
    endm

;----------------------------------------------------
;  Clear Posted Interrupt Flag Mask
;
;  Use the following macros to clear the
;  bits in the Interrupt Clear registers,
;  INT_CLR0, INT_CLR1 or INT_CLR2.
;  Usage:    M8C_ClearIntFlag INT_CLRN, MASK
;
;  where INT_MSKN is INT_CLR0, INT_CLR1 or INT_CLR2
;        and MASK is the bit set to enable or disable
;----------------------------------------------------
    macro M8C_ClearIntFlag
    mov   reg[@0], ~@1              ; clear specified interrupt enable bit
    endm

;----------------------------------------------------
;  Power-On Reset & WatchDog Timer Functions
;----------------------------------------------------
    macro M8C_EnableWatchDog
    and   reg[CPU_SCR], ~CPU_SCR_PORS_MASK
    endm

    macro M8C_ClearWDT
    mov   reg[RES_WDT], 00h
    endm

    macro M8C_ClearWDTAndSleep
    mov   reg[RES_WDT], 38h
    endm

;----------------------------------------------------
;  Sleep, CPU Stop & Software Reset
;----------------------------------------------------
    macro M8C_Sleep
    or    reg[CPU_SCR], CPU_SCR_SLEEP_MASK
    ; The next instruction to be executed depends on the state of the
    ; various interrupt enable bits. If some interrupts are enabled
    ; and the global interrupts are disabled, the next instruction will
    ; be the one that follows the invocation of this macro. If global
    ; interrupts are also enabled then the next instruction will be
    ; from the interrupt vector table. If no interrupts are enabled
    ; then the CPU sleeps forever.
    endm

    macro M8C_Stop
    ; In general, you probably don't want to do this, but here's how:
    or    reg[CPU_SCR], CPU_SCR_STOP_MASK
    ; Next instruction to be executed is located in the interrupt
    ; vector table entry for Power-On Reset.
    endm

    macro M8C_Reset
    ; Restore CPU to the power-on reset state.
    mov A, 0
    SSC
    ; Next non-supervisor instruction will be at interrupt vector 0.
    endm

;----------------------------------------------------
; ImageCraft Code Compressor Actions
;----------------------------------------------------
    ; Suspend Code Compressor
    ; Must not span a RET or RETI instruction
    ; without resuming code compression
    macro Suspend_CodeCompressor
    or   F, 0
    endm

    ; Resume Code Compression
    macro Resume_CodeCompressor
    add  SP, 0
    endm
