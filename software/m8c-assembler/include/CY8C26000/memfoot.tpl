

;  ******* Function Class Definitions *******
;
;  These definitions are provided for forward compatibility

RAM_USE_CLASS_1:               equ 1   ; PUSH, POP & I/O access
RAM_USE_CLASS_2:               equ 2   ; Indexed address mode on stack page
RAM_USE_CLASS_3:               equ 4   ; Indexed address mode to any page
RAM_USE_CLASS_4:               equ 8   ; Direct/Indirect address mode access


;  ******* Page Pointer Manipulation Macros *******
;

   ;-----------------------------------------------
   ;  Set Stack Page Macro
   ;-----------------------------------------------
   ;
   ; USE:   This is an empty macro provided for forward compatibility.

   macro RAM_SETPAGE_STK
   endm

   ;-----------------------------------------------
   ;  Set Current Page Macro
   ;-----------------------------------------------
   ;
   ; USE:   This is an empty macro provided for forward compatibility.

   macro RAM_SETPAGE_CUR
   endm

   ;-----------------------------------------------
   ;  Set Index Page Macro
   ;-----------------------------------------------
   ;
   ; USE:   This is an empty macro provided for forward compatibility.

   macro RAM_SETPAGE_IDX
   endm

   ;-----------------------------------------------
   ;  Set MVI Read Page Macro
   ;-----------------------------------------------
   ;
   ; USE:   This is an empty macro provided for forward compatibility.

   macro RAM_SETPAGE_MVR
   endm

   ;-----------------------------------------------
   ;  Set MVI Write Page Macro
   ;-----------------------------------------------
   ;
   ; USE:   This is an empty macro provided for forward compatibility.

   macro RAM_SETPAGE_MVW
   endm

   ;-----------------------------------------------
   ;  Force Index Page Pointer to Stack Page
   ;-----------------------------------------------
   ;
   ; USE:   This is an empty macro provided for forward compatibility.

   macro RAM_SETPAGE_IDX2STK
   endm

   ;-----------------------------------------------
   ;  Change Memory Mode
   ;-----------------------------------------------
   ;
   ; USE:   This is an empty macro provided for forward compatibility.

   macro RAM_CHANGE_PAGE_MODE
   endm

   ;-----------------------------------------------
   ;  Set Large Memory Model Native Paging Mode
   ;-----------------------------------------------
   ;
   ; USE:   This is an empty macro provided for forward compatibility.

   macro RAM_SET_NATIVE_PAGING
   endm

   ;-----------------------------------------------
   ; Restore Large Memory Model Native Paging Mode
   ;-----------------------------------------------
   ;
   ; USE:   This is an empty macro provided for forward compatibility.

   macro RAM_RESTORE_NATIVE_PAGING
   endm

   ;-----------------------------------------------
   ; Force indexed addr mode operands to Stack Pg
   ;-----------------------------------------------
   ;
   ; USE:   This is an empty macro provided for forward compatibility.

   macro RAM_X_POINTS_TO_STACKPAGE
   endm

   ;-----------------------------------------------
   ;  Force indexed addr mode operands to Index Pg
   ;-----------------------------------------------
   ;
   ; USE:   This is an empty macro provided for forward compatibility.

   macro RAM_X_POINTS_TO_INDEXPAGE
   endm

   ;-----------------------------------------------
   ;  Function Prologue
   ;-----------------------------------------------
   ;
   ;  USE:   This is an empty macro provided for forward compatibility.

   macro RAM_PROLOGUE
   endm

   ;-----------------------------------------------
   ;  Function Epilogue
   ;-----------------------------------------------
   ;
   ;  USE:   This is an empty macro provided for forward compatibility.

   macro RAM_EPILOGUE
   endm

   ;-----------------------------------------------
   ;  Preserve Register
   ;-----------------------------------------------
   ;
   ;  USE:   This is an empty macro provided for forward compatibility.

   macro REG_PRESERVE
   endm

   ;-----------------------------------------------
   ;  Restore Register
   ;-----------------------------------------------
   ;
   ;  USE:   This is an empty macro provided for forward compatibility.

   macro REG_RESTORE
   endm

   ;-----------------------------------------------
   ;  Preserve Volatile Page Pointer Registers
   ;-----------------------------------------------
   ;
   ;  USE:   This is an empty macro provided for forward compatibility.

   macro ISR_PRESERVE_PAGE_POINTERS
   endm

   ;-----------------------------------------------
   ;  Restore Volatile Page Pointer Registers
   ;-----------------------------------------------
   ;
   ;  USE:   This is an empty macro provided for forward compatibility.

   macro ISR_RESTORE_PAGE_POINTERS
   endm

; end of file Memory.inc
