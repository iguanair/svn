;;*****************************************************************************
;;*****************************************************************************
;;
;;       FILENAME: Memory.inc
;;
;;    DESCRIPTION: Memory Model and Stack Parameter Definitions for 
;;                 the 26xxx family of PSoC devices.
;;
;;  LAST MODIFIED: June 17, 2004
;;
;;-----------------------------------------------------------------------------
;;  Copyright (c) Cypress MicroSystems 2004. All Rights Reserved.
;;*****************************************************************************
;;*****************************************************************************
;
;
;  ******** Define Memory Model and Stack parameters ********
;
;  These definitions provide backward compatability for the 
;  CY8C22/24/27/29 Families of PSoC Devices
;
