//=============================================================================
//
//    m8c.h: M8C25000/26000 Microcontroller Family System Declarations
//
//    Copyright: Cypress MicroSystems 2000-2003. All Rights Reserved.
//
//
//    This file provides address constants, bit field masks and a set of macro
//    facilities for the Cypress MicroSystems 25xxx and 26xxx Microcontroller
//    families.
//
//    Last Modified: June 17, 2004
//
//=============================================================================

#ifndef M8C_C_HEADER
#define M8C_C_HEADER

//-----------------------------------------------
// Define System Types
//-----------------------------------------------
typedef  unsigned char  BOOL;
typedef  unsigned char  BYTE;
typedef  signed   char  CHAR;
typedef  unsigned int   WORD;
typedef  signed   int   INT;
typedef  unsigned long  DWORD;
typedef  signed   long  LONG;

//-----------------------------------------------
// Define Boolean TRUE/FALSE
//-----------------------------------------------
#define  TRUE  ((BOOL) 1)
#define  FALSE ((BOOL) 0)


//=============================================================================
//=============================================================================
//      System Registers
//=============================================================================
//=============================================================================

//-----------------------------------------------
//  Flag Register Bit Fields
//-----------------------------------------------
#define  FLAG_XIO_MASK     (0x10)
#define  FLAG_SUPER        (0x08)
#define  FLAG_CARRY        (0x04)
#define  FLAG_ZERO         (0x02)
#define  FLAG_GLOBAL_IE    (0x01)


//=============================================================================
//=============================================================================
//      Register Space, Bank 0
//=============================================================================
//=============================================================================

//-----------------------------------------------
//  Port Registers
//  Note: Also see this address range in Bank 1.
//-----------------------------------------------
// Port 0
#pragma  ioport   PRT0DR:     0x000  // Port 0 Data Register
BYTE              PRT0DR;
#pragma  ioport   PRT0IE:     0x001  // Port 0 Interrupt Enable Register
BYTE              PRT0IE;
#pragma  ioport   PRT0GS:     0x002  // Port 0 Global Select Register
BYTE              PRT0GS;
// Port 1
#pragma  ioport   PRT1DR:     0x004  // Port 1 Data Register
BYTE              PRT1DR;
#pragma  ioport   PRT1IE:     0x005  // Port 1 Interrupt Enable Register
BYTE              PRT1IE;
#pragma  ioport   PRT1GS:     0x006  // Port 1 Global Select Register
BYTE              PRT1GS;
// Port 2
#pragma  ioport   PRT2DR:     0x008  // Port 2 Data Register
BYTE              PRT2DR;
#pragma  ioport   PRT2IE:     0x009  // Port 2 Interrupt Enable Register
BYTE              PRT2IE;
#pragma  ioport   PRT2GS:     0x00A  // Port 2 Global Select Register
BYTE              PRT2GS;
// Port 3
#pragma  ioport   PRT3DR:     0x00C  // Port 3 Data Register
BYTE              PRT3DR;
#pragma  ioport   PRT3IE:     0x00D  // Port 3 Interrupt Enable Register
BYTE              PRT3IE;
#pragma  ioport   PRT3GS:     0x00E  // Port 3 Global Select Register
BYTE              PRT3GS;
// Port 4
#pragma  ioport   PRT4DR:     0x010  // Port 4 Data Register
BYTE              PRT4DR;
#pragma  ioport   PRT4IE:     0x011  // Port 4 Interrupt Enable Register
BYTE              PRT4IE;
#pragma  ioport   PRT4GS:     0x012  // Port 4 Global Select     Register
BYTE              PRT4GS;
// Port 5
#pragma  ioport   PRT5DR:     0x014  // Port 5 Data Register
BYTE              PRT5DR;
#pragma  ioport   PRT5IE:     0x015  // Port 5 Interrupt Enable Register
BYTE              PRT5IE;
#pragma  ioport   PRT5GS:     0x016  // Port 5 Global Select Register
BYTE              PRT5GS;

//-----------------------------------------------
//  Digital PSoC(tm) block Registers
//  Note: Also see this address range in Bank 1.
//-----------------------------------------------

// Digital PSoC block 00, Basic Type A
#pragma  ioport   DBA00DR0:   0x020  // data register 0
BYTE              DBA00DR0;
#pragma  ioport   DBA00DR1:   0x021  // data register 1
BYTE              DBA00DR1;
#pragma  ioport   DBA00DR2:   0x022  // data register 2
BYTE              DBA00DR2;
#pragma  ioport   DBA00CR0:   0x023  // control & status register 0
BYTE              DBA00CR0;

// Digital PSoC block 01, Basic Type A
#pragma  ioport   DBA01DR0:   0x024  // data register 0
BYTE              DBA01DR0;
#pragma  ioport   DBA01DR1:   0x025  // data register 1
BYTE              DBA01DR1;
#pragma  ioport   DBA01DR2:   0x026  // data register 2
BYTE              DBA01DR2;
#pragma  ioport   DBA01CR0:   0x027  // control & status register 0
BYTE              DBA01CR0;

// Digital PSoC block 02, Basic Type A
#pragma  ioport   DBA02DR0:   0x028  // data register 0
BYTE              DBA02DR0;
#pragma  ioport   DBA02DR1:   0x029  // data register 1
BYTE              DBA02DR1;
#pragma  ioport   DBA02DR2:   0x02A  // data register 2
BYTE              DBA02DR2;
#pragma  ioport   DBA02CR0:   0x02B  // control & status register 0
BYTE              DBA02CR0;

// Digital PSoC block 03, Basic Type A
#pragma  ioport   DBA03DR0:   0x02C  // data register 0
BYTE              DBA03DR0;
#pragma  ioport   DBA03DR1:   0x02D  // data register 1
BYTE              DBA03DR1;
#pragma  ioport   DBA03DR2:   0x02E  // data register 2
BYTE              DBA03DR2;
#pragma  ioport   DBA03CR0:   0x02F  // control & status register 0
BYTE              DBA03CR0;

// Digital PSoC block 04, Communications Type A
#pragma  ioport   DCA04DR0:   0x030  // data register 0
BYTE              DCA04DR0;
#pragma  ioport   DCA04DR1:   0x031  // data register 1
BYTE              DCA04DR1;
#pragma  ioport   DCA04DR2:   0x032  // data register 2
BYTE              DCA04DR2;
#pragma  ioport   DCA04CR0:   0x033  // control & status register 0
BYTE              DCA04CR0;

// Digital PSoC block 05, Communications Type A
#pragma  ioport   DCA05DR0:   0x034  // data register 0
BYTE              DCA05DR0;
#pragma  ioport   DCA05DR1:   0x035  // data register 1
BYTE              DCA05DR1;
#pragma  ioport   DCA05DR2:   0x036  // data register 2
BYTE              DCA05DR2;
#pragma  ioport   DCA05CR0:   0x037  // control & status register 0
BYTE              DCA05CR0;

// Digital PSoC block 06, Communications Type A
#pragma  ioport   DCA06DR0:   0x038  // data register 0
BYTE              DCA06DR0;
#pragma  ioport   DCA06DR1:   0x039  // data register 1
BYTE              DCA06DR1;
#pragma  ioport   DCA06DR2:   0x03A  // data register 2
BYTE              DCA06DR2;
#pragma  ioport   DCA06CR0:   0x03B  // control & status register 0
BYTE              DCA06CR0;

// Digital PSoC block 07, Communications Type A
#pragma  ioport   DCA07DR0:   0x03C  // data register 0
BYTE              DCA07DR0;
#pragma  ioport   DCA07DR1:   0x03D  // data register 1
BYTE              DCA07DR1;
#pragma  ioport   DCA07DR2:   0x03E  // data register 2
BYTE              DCA07DR2;
#pragma  ioport   DCA07CR0:   0x03F  // control & status register 0
BYTE              DCA07CR0;

//-----------------------------------------------
//  Analog Resource Control Registers
//-----------------------------------------------

#pragma  ioport   AMX_IN:     0x060  // Analog Input Multiplexor Control
BYTE              AMX_IN;
#define AMX_IN_ACI3     (0xC0)
#define AMX_IN_ACI2     (0x30)
#define AMX_IN_ACI1     (0x0C)
#define AMX_IN_ACI0     (0x03)

#pragma  ioport   ARF_CR:     0x063  // Analog Reference Control Register
BYTE              ARF_CR;
#define ARF_CR_BGT      (0x80)
#define ARF_CR_HBE      (0x40)
#define ARF_CR_REF      (0x38)
#define ARF_CR_REFPWR   (0x07)
#define ARF_CR_SCPWR    (0x03)
// Deprecated:
#define ARF_CR_APWR     (0x04)

#pragma  ioport   CMP_CR:     0x064  // comparator control
BYTE              CMP_CR;
#define CMP_CR_COMP3    (0x80)
#define CMP_CR_COMP2    (0x40)
#define CMP_CR_COMP1    (0x20)
#define CMP_CR_COMP0    (0x10)
#define CMP_CR_AINT3    (0x08)
#define CMP_CR_AINT2    (0x04)
#define CMP_CR_AINT1    (0x02)
#define CMP_CR_AINT0    (0x01)

#pragma  ioport   ASY_CR:     0x065  // Analog Synchronizaton Control Register
BYTE              ASY_CR;
#define ASY_CR_SARCOUNT (0x70)
#define ASY_CR_SARSIGN  (0x08)
#define ASY_CR_SARCOL   (0x06)
#define ASY_CR_SYNCEN   (0x01)

//-----------------------------------------------
//  Analog PSoC block Registers
//
//  Note: the following registers are mapped into
//  both register bank 0 AND register bank 1.
//-----------------------------------------------

// Continuous Time PSoC Block Type A Row 0 Col 0
#pragma  ioport   ACA00CR0:   0x071  // Control register 0
BYTE              ACA00CR0;
#pragma  ioport   ACA00CR1:   0x072  // Control register 1
BYTE              ACA00CR1;
#pragma  ioport   ACA00CR2:   0x073  // Control register 2
BYTE              ACA00CR2;

// Continuous Time PSoC block Type A Row 0 Col 1
#pragma  ioport   ACA01CR0:   0x075  // Control register 0
BYTE              ACA01CR0;
#pragma  ioport   ACA01CR1:   0x076  // Control register 1
BYTE              ACA01CR1;
#pragma  ioport   ACA01CR2:   0x077  // Control register 2
BYTE              ACA01CR2;

// Continuous Time PSoC block Type A Row 0 Col 2
#pragma  ioport   ACA02CR0:   0x079  // Control register 0
BYTE              ACA02CR0;
#pragma  ioport   ACA02CR1:   0x07A  // Control register 1
BYTE              ACA02CR1;
#pragma  ioport   ACA02CR2:   0x07B  // Control register 2
BYTE              ACA02CR2;

// Continuous Time PSoC block Type A Row 0 Col 3
#pragma  ioport   ACA03CR0:   0x07D  // Control register 0
BYTE              ACA03CR0;
#pragma  ioport   ACA03CR1:   0x07E  // Control register 1
BYTE              ACA03CR1;
#pragma  ioport   ACA03CR2:   0x07F  // Control register 2
BYTE              ACA03CR2;

// Switched Cap PSoC block Type A Row 1 Col 0
#pragma  ioport   ASA10CR0:   0x080  // Control register 0
BYTE              ASA10CR0;
#pragma  ioport   ASA10CR1:   0x081  // Control register 1
BYTE              ASA10CR1;
#pragma  ioport   ASA10CR2:   0x082  // Control register 2
BYTE              ASA10CR2;
#pragma  ioport   ASA10CR3:   0x083  // Control register 3
BYTE              ASA10CR3;

// Switched Cap PSoC block Type B Row 1 Col 1
#pragma  ioport   ASB11CR0:   0x084  // Control register 0
BYTE              ASB11CR0;
#pragma  ioport   ASB11CR1:   0x085  // Control register 1
BYTE              ASB11CR1;
#pragma  ioport   ASB11CR2:   0x086  // Control register 2
BYTE              ASB11CR2;
#pragma  ioport   ASB11CR3:   0x087  // Control register 3
BYTE              ASB11CR3;

// Switched Cap PSoC block Type A Row 1 Col 2
#pragma  ioport   ASA12CR0:   0x088  // Control register 0
BYTE              ASA12CR0;
#pragma  ioport   ASA12CR1:   0x089  // Control register 1
BYTE              ASA12CR1;
#pragma  ioport   ASA12CR2:   0x08A  // Control register 2
BYTE              ASA12CR2;
#pragma  ioport   ASA12CR3:   0x08B  // Control register 3
BYTE              ASA12CR3;

// Switched Cap PSoC block Type B Row 1 Col 3
#pragma  ioport   ASB13CR0:   0x08C  // Control register 0
BYTE              ASB13CR0;
#pragma  ioport   ASB13CR1:   0x08D  // Control register 1
BYTE              ASB13CR1;
#pragma  ioport   ASB13CR2:   0x08E  // Control register 2
BYTE              ASB13CR2;
#pragma  ioport   ASB13CR3:   0x08F  // Control register 3
BYTE              ASB13CR3;

// Switched Cap PSoC block Type B Row 2 Col 0
#pragma  ioport   ASB20CR0:   0x090  // Control register 0
BYTE              ASB20CR0;
#pragma  ioport   ASB20CR1:   0x091  // Control register 1
BYTE              ASB20CR1;
#pragma  ioport   ASB20CR2:   0x092  // Control register 2
BYTE              ASB20CR2;
#pragma  ioport   ASB20CR3:   0x093  // Control register 3
BYTE              ASB20CR3;

// Switched Cap PSoC block Type A Row 2 Col 1
#pragma  ioport   ASA21CR0:   0x094  // Control register 0
BYTE              ASA21CR0;
#pragma  ioport   ASA21CR1:   0x095  // Control register 1
BYTE              ASA21CR1;
#pragma  ioport   ASA21CR2:   0x096  // Control register 2
BYTE              ASA21CR2;
#pragma  ioport   ASA21CR3:   0x097  // Control register 3
BYTE              ASA21CR3;

// Switched Cap PSoC block Type B Row 2 Col 2
#pragma  ioport   ASB22CR0:   0x098  // Control register 0
BYTE              ASB22CR0;
#pragma  ioport   ASB22CR1:   0x099  // Control register 1
BYTE              ASB22CR1;
#pragma  ioport   ASB22CR2:   0x09A  // Control register 2
BYTE              ASB22CR2;
#pragma  ioport   ASB22CR3:   0x09B  // Control register 3
BYTE              ASB22CR3;

// Switched Cap PSoC block Type A Row 2 Col 3
#pragma  ioport   ASA23CR0:   0x09C  // Control register 0
BYTE              ASA23CR0;
#pragma  ioport   ASA23CR1:   0x09D  // Control register 1
BYTE              ASA23CR1;
#pragma  ioport   ASA23CR2:   0x09E  // Control register 2
BYTE              ASA23CR2;
#pragma  ioport   ASA23CR3:   0x09F  // Control register 3
BYTE              ASA23CR3;

//-----------------------------------------------
//  System and Global Resource Registers
//-----------------------------------------------
#pragma  ioport   INT_MSK0:   0x0E0  // General Interrupt Mask Register
BYTE              INT_MSK0;
// Recommended:
#define  INT_MSK0_SLEEP             (0x40)
#define  INT_MSK0_GPIO              (0x20)
#define  INT_MSK0_ACOLUMN_3         (0x10)
#define  INT_MSK0_ACOLUMN_2         (0x08)
#define  INT_MSK0_ACOLUMN_1         (0x04)
#define  INT_MSK0_ACOLUMN_0         (0x02)
#define  INT_MSK0_VOLTAGE_MONITOR   (0x01)
// Deprecated:
#define  INT_MSK0_Sleep             (0x40)
#define  INT_MSK0_GPIO              (0x20)
#define  INT_MSK0_AColumn3          (0x10)
#define  INT_MSK0_AColumn2          (0x08)
#define  INT_MSK0_AColumn1          (0x04)
#define  INT_MSK0_AColumn0          (0x02)
#define  INT_MSK0_VoltageMonitor    (0x01)

#pragma  ioport   INT_MSK1:   0x0E1  // Digital PSoC block Mask Register
BYTE              INT_MSK1;
#define  INT_MSK1_DCA07           (0x80)
#define  INT_MSK1_DCA06           (0x40)
#define  INT_MSK1_DBA05           (0x20)
#define  INT_MSK1_DBA04           (0x10)
#define  INT_MSK1_DCA03           (0x08)
#define  INT_MSK1_DCA02           (0x04)
#define  INT_MSK1_DBA01           (0x02)
#define  INT_MSK1_DBA00           (0x01)

#pragma  ioport   INT_VC:     0x0E2  // Interrupt vector register
BYTE              INT_VC;
#pragma  ioport   RES_WDT:    0x0E3  // Watch Dog Timer
BYTE              RES_WDT;

// DECIMATOR Registers
#pragma  ioport   DEC_DH:     0x0E4  // Data Register (high byte)
CHAR              DEC_DH;
#pragma  ioport   DEC_DL:     0x0E5  // Data Register ( low byte)
CHAR              DEC_DL;
#pragma  ioport   DEC_CR:     0x0E6  // Data Control Register
BYTE              DEC_CR;

// Multiplier and MAC (Multiply/Accumulate) Unit
#pragma  ioport   MUL_X:         0x0E8  // Multiplier X Register (write)
CHAR              MUL_X;
#pragma  ioport   MUL_Y:         0x0E9  // Multiplier Y Register (write)
CHAR              MUL_Y;
#pragma  ioport   MUL_DH:        0x0EA  // Multiplier Result Data (high byte read)
CHAR              MUL_DH;
#pragma  ioport   MUL_DL:        0x0EB  // Multiplier Result Data ( low byte read)
CHAR              MUL_DL;
#pragma  ioport   MUL_RESULT:    0x0EA  // Multiplier Result Data - WORD
INT               MUL_RESULT;
#pragma  ioport   MAC_X:         0x0EC  // MAC X register (write) [see ACC_DR1]
CHAR              MAC_X;
#pragma  ioport   MAC_Y:         0x0ED  // MAC Y register (write) [see ACC_DR0]
CHAR              MAC_Y;
#pragma  ioport   MAC_CL0:       0x0EE  // MAC Clear Accum (write)[see ACC_DR3]
BYTE              MAC_CL0;
#pragma  ioport   MAC_CL1:       0x0EF  // MAC Clear Accum (write)[see ACC_DR2]
BYTE              MAC_CL1;
#pragma  ioport   ACC_DR1:       0x0EC  // MAC Accumulator (Read, byte 1)
CHAR              ACC_DR1;
#pragma  ioport   ACC_DR0:       0x0ED  // MAC Accumulator (Read, byte 0)
CHAR              ACC_DR0;
#pragma  ioport   ACC_LOW_WORD:  0x0EC  // MAC Accumulator (Read low word)
INT               ACC_LOW_WORD;
#pragma  ioport   ACC_DR3:       0x0EE  // MAC Accumulator (Read, byte 3)
CHAR              ACC_DR3;
#pragma  ioport   ACC_DR2:       0x0EF  // MAC Accumulator (Read, byte 2)
CHAR              ACC_DR2;
#pragma  ioport   ACC_HI_WORD:   0x0EE  // MAC Accumulator (Read high word)
INT               ACC_HI_WORD;

// THE FOLLOWING HAS BEEN COMMENTED OUT: This register in not accessible
// during normal operation:
// Test Mode mapping of the CPU Flag (F) Register
//#pragma  ioport   CPU_FLAG:      0x0F7  // NOTE: Only mapped when in Test Mode !!!
//BYTE              CPU_FLAG;

//-----------------------------------------------
//  System Status and Control Register
//
//  Note: the following register is mapped into
//  both register bank 0 AND register bank 1.
//-----------------------------------------------
#pragma  ioport   CPU_SCR:    0x0FF  // System Status and Control Register
BYTE              CPU_SCR;
#define  CPUSCR_GIE_MASK      (0x80)
#define  CPUSCR_WDRS_MASK     (0x20)
#define  CPUSCR_PORS_MASK     (0x10)
#define  CPUSCR_SLEEP_MASK    (0x08)
#define  CPUSCR_STOP_MASK     (0x01)

//=============================================================================
//=============================================================================
//      Register Space, Bank 1
//=============================================================================
//=============================================================================


//-----------------------------------------------
//  Port Registers
//  Note: Also see this address range in Bank 0.
//-----------------------------------------------
// Port 0
#pragma  ioport   PRT0DM0:    0x100  // Port 0 Drive Mode 0
BYTE              PRT0DM0;
#pragma  ioport   PRT0DM1:    0x101  // Port 0 Drive Mode 1
BYTE              PRT0DM1;
#pragma  ioport   PRT0IC0:    0x102  // Port 0 Interrupt Control 0
BYTE              PRT0IC0;
#pragma  ioport   PRT0IC1:    0x103  // Port 0 Interrupt Control 1
BYTE              PRT0IC1;
// Port 1
#pragma  ioport   PRT1DM0:    0x104  // Port 1 Drive Mode 0
BYTE              PRT1DM0;
#pragma  ioport   PRT1DM1:    0x105  // Port 1 Drive Mode 1
BYTE              PRT1DM1;
#pragma  ioport   PRT1IC0:    0x106  // Port 1 Interrupt Control 0
BYTE              PRT1IC0;
#pragma  ioport   PRT1IC1:    0x107  // Port 1 Interrupt Control 1
BYTE              PRT1IC1;
// Port 2
#pragma  ioport   PRT2DM0:    0x108  // Port 2 Drive Mode 0
BYTE              PRT2DM0;
#pragma  ioport   PRT2DM1:    0x109  // Port 2 Drive Mode 1
BYTE              PRT2DM1;
#pragma  ioport   PRT2IC0:    0x10A  // Port 2 Interrupt Control 0
BYTE              PRT2IC0;
#pragma  ioport   PRT2IC1:    0x10B  // Port 2 Interrupt Control 1
BYTE              PRT2IC1;
// Port 3
#pragma  ioport   PRT3DM0:    0x10C  // Port 3 Drive Mode 0
BYTE              PRT3DM0;
#pragma  ioport   PRT3DM1:    0x10D  // Port 3 Drive Mode 1
BYTE              PRT3DM1;
#pragma  ioport   PRT3IC0:    0x10E  // Port 3 Interrupt Control 0
BYTE              PRT3IC0;
#pragma  ioport   PRT3IC1:    0x10F  // Port 3 Interrupt Control 1
BYTE              PRT3IC1;
// Port 4
#pragma  ioport   PRT4DM0:    0x110  // Port 4 Drive Mode 0
BYTE              PRT4DM0;
#pragma  ioport   PRT4DM1:    0x111  // Port 4 Drive Mode 1
BYTE              PRT4DM1;
#pragma  ioport   PRT4IC0:    0x112  // Port 4 Interrupt Control 0
BYTE              PRT4IC0;
#pragma  ioport   PRT4IC1:    0x113  // Port 4 Interrupt Control 1
BYTE              PRT4IC1;
// Port 5
#pragma  ioport   PRT5DM0:    0x114  // Port 5 Drive Mode 0
BYTE              PRT5DM0;
#pragma  ioport   PRT5DM1:    0x115  // Port 5 Drive Mode 1
BYTE              PRT5DM1;
#pragma  ioport   PRT5IC0:    0x116  // Port 5 Interrupt Control 0
BYTE              PRT5IC0;
#pragma  ioport   PRT5IC1:    0x117  // Port 5 Interrupt Control 1
BYTE              PRT5IC1;

//-----------------------------------------------
//  Digital PSoC(tm) block Registers
//  Note: Also see this address range in Bank 1.
//-----------------------------------------------

// Digital PSoC block 00, Basic Type A
#pragma  ioport   DBA00FN:    0x120  // Function Register
BYTE              DBA00FN;
#pragma  ioport   DBA00IN:    0x121  //    Input Register
BYTE              DBA00IN;
#pragma  ioport   DBA00OU:    0x122  //   Output Register
BYTE              DBA00OU;

// Digital PSoC block 01, Basic Type A
#pragma  ioport   DBA01FN:    0x124  // Function Register
BYTE              DBA01FN;
#pragma  ioport   DBA01IN:    0x125  //    Input Register
BYTE              DBA01IN;
#pragma  ioport   DBA01OU:    0x126  //   Output Register
BYTE              DBA01OU;

// Digital PSoC block 02, Basic Type A
#pragma  ioport   DBA02FN:    0x128  // Function Register
BYTE              DBA02FN;
#pragma  ioport   DBA02IN:    0x129  //    Input Register
BYTE              DBA02IN;
#pragma  ioport   DBA02OU:    0x12A  //   Output Register
BYTE              DBA02OU;

// Digital PSoC block 03, Basic Type A
#pragma  ioport   DBA03FN:    0x12C  // Function Register
BYTE              DBA03FN;
#pragma  ioport   DBA03IN:    0x12D  //    Input Register
BYTE              DBA03IN;
#pragma  ioport   DBA03OU:    0x12E  //   Output Register
BYTE              DBA03OU;

// Digital PSoC block 04, Communications Type A
#pragma  ioport   DCA04FN:    0x130  // Function Register
BYTE              DCA04FN;
#pragma  ioport   DCA04IN:    0x131  //    Input Register
BYTE              DCA04IN;
#pragma  ioport   DCA04OU:    0x132  //   Output Register
BYTE              DCA04OU;

// Digital PSoC block 05, Communications Type A
#pragma  ioport   DCA05FN:    0x134  // Function Register
BYTE              DCA05FN;
#pragma  ioport   DCA05IN:    0x135  //    Input Register
BYTE              DCA05IN;
#pragma  ioport   DCA05OU:    0x136  //   Output Register
BYTE              DCA05OU;

// Digital PSoC block 06, Communications Type A
#pragma  ioport   DCA06FN:    0x138  // Function Register
BYTE              DCA06FN;
#pragma  ioport   DCA06IN:    0x139  //    Input Register
BYTE              DCA06IN;
#pragma  ioport   DCA06OU:    0x13A  //   Output Register
BYTE              DCA06OU;

// Digital PSoC block 07, Communications Type A
#pragma  ioport   DCA07FN:    0x13C  // Function Register
BYTE              DCA07FN;
#pragma  ioport   DCA07IN:    0x13D  //    Input Register
BYTE              DCA07IN;
#pragma  ioport   DCA07OU:    0x13E  //   Output Register
BYTE              DCA07OU;


//-----------------------------------------------
//  System and Global Resource Registers
//  Note: Also see this address range in Bank 0.
//-----------------------------------------------

#pragma  ioport   CLK_CR0:    0x160  // Analog Column Clock Select Register
BYTE              CLK_CR0;
#define CLK_CR0_ACOLUMN_3             (0xC0)
#define CLK_CR0_ACOLUMN_2             (0x30)
#define CLK_CR0_ACOLUMN_1             (0x0C)
#define CLK_CR0_ACOLUMN_0             (0x03)

#pragma  ioport   CLK_CR1:    0x161  // Analog Clock Source Select Register
BYTE              CLK_CR1;
#define CLK_CR1_SHDIS                 (0x40)
#define CLK_CR1_ACLK1                 (0x38)
#define CLK_CR1_ACLK2                 (0x07)

#pragma  ioport   ABF_CR:     0x162  // Analog Output Buffer Control Register
BYTE              ABF_CR;
#define ABF_CR_ACOL1MUX               (0x80)
#define ABF_CR_ACOL2MUX               (0x40)
#define ABF_CR_ABUF1EN                (0x20)
#define ABF_CR_ABUF2EN                (0x10)
#define ABF_CR_ABUF0EN                (0x08)
#define ABF_CR_ABUF3EN                (0x04)
#define ABF_CR_BYPASS                 (0x02)
#define ABF_CR_PWR                    (0x01)

#pragma  ioport   AMD_CR:     0x163  // Analog Modulator Control Register
BYTE              AMD_CR;
#define AMD_CR_AMOD2                  (0x0C)
#define AMD_CR_AMOD0                  (0x03)

//------------------------------------------------
//  Clock and System Control Registers
//------------------------------------------------

#pragma  ioport   OSC_CR0:    0x1E0  // System Oscillator Control Register
BYTE              OSC_CR0;
#define OSC_CR0_32K_SELECT            (0x80)
#define OSC_CR0_PLL_MODE              (0x40)
#define OSC_CR0_SLEEP                 (0x18)
#define OSC_CR0_SLEEP_512Hz           (0x00)
#define OSC_CR0_SLEEP_64Hz            (0x08)
#define OSC_CR0_SLEEP_8Hz             (0x10)
#define OSC_CR0_SLEEP_1Hz             (0x18)
#define OSC_CR0_CPU                   (0x07)
#define OSC_CR0_CPU_3MHz              (0x00)
#define OSC_CR0_CPU_6MHz              (0x01)
#define OSC_CR0_CPU_12MHz             (0x02)
#define OSC_CR0_CPU_24MHz             (0x03)
#define OSC_CR0_CPU_1d5MHz            (0x04)
#define OSC_CR0_CPU_750kHz            (0x05)
#define OSC_CR0_CPU_187d5kHz          (0x06)
#define OSC_CR0_CPU_93d7kHz           (0x07)

#pragma  ioport   OSC_CR1:    0x1E1  // System V1/V2 Divider Control Register
BYTE              OSC_CR1;
#define OSC_CR1_V1                    (0xF0)
#define OSC_CR1_V2                    (0x0F)

#pragma  ioport   VLT_CR:     0x1E3  // Voltage Monitor Control Register
BYTE              VLT_CR;
#define VLT_CR_SMP                    (0x80)
#define VLT_CR_VM                     (0x07)

#pragma  ioport   IMO_TR:     0x1E8  // Internal Main Oscillator Trim Register
BYTE              IMO_TR;
#pragma  ioport   ILO_TR:     0x1E9  // Internal Low-speed Oscillator Trim
BYTE              ILO_TR;
#pragma  ioport   BDG_TR:     0x1EA  // Band Gap Trim Register
BYTE              BDG_TR;
#pragma  ioport   ECO_TR:     0x1EB  // External Oscillator Trim Register
BYTE              ECO_TR;


//=============================================================================
//=============================================================================
//      M8C System Macros
//=============================================================================
//=============================================================================


//-----------------------------------------------
//  Swapping Register Banks
//-----------------------------------------------
#define  M8C_SetBank0            asm("and F, EFh")
#define  M8C_SetBank1            asm("or  F, 10h")

//-----------------------------------------------
//  Global Interrupt Enable/Disable
//-----------------------------------------------
#define  M8C_EnableGInt          asm("or  F, 01h")
#define  M8C_DisableGInt         asm("and F, FEh")

//-----------------------------------------------
// Enable/Disable Interrupt Mask
// Use the following macros to enable/disable
// either of the two global interrupt mask registers,
// INT_MSK0 or INT_MSK1.
//
// This is a fix to a noted problem in which an
// inadvertant reset can occur if an interrupt occurs
// while clearing an interrupt mask bit.
//
// Usage:    M8C_DisableIntMask INT_MSKN, MASK
//           M8C_EnableIntMask  INT_MSKN, MASK
//
// where INT_MSKN is INT_MSK0 or INT_MSK1 and
//       MASK is the bit set to enable or disable
//------------------------------------------------
#define  DISABLE_INT_FIX   (1)

// Disable Interrupt Bit Mask(s)
#ifdef DISABLE_INT_FIX
#define M8C_DisableIntMask(INT_MSKN_REG,MASK)         \
   {                                                  \
      /* save the flag reg global Int enable bit */   \
      BYTE  bFlagShadow = CPU_SCR & CPUSCR_GIE_MASK;  \
      /* disable global interrupts */                 \
      M8C_DisableGInt;                                \
      /* disable specified int enable bit(s) */       \
      INT_MSKN_REG &= ~MASK;                          \
      /* determine if global ints where enabled */    \
      if (bFlagShadow)                                \
      {                                               \
         /* re-enable global interrups */             \
          M8C_EnableGInt;                             \
      }                                               \
   }
#else
#define M8C_DisableIntMask( INT_MSKN_REG, MASK )   (INT_MSKN_REG &= ~MASK)
#endif

// Enable Interrupt Bit Mask(s)
#define M8C_EnableIntMask( INT_MSKN_REG, MASK )    (INT_MSKN_REG |= MASK)

//---------------------------------------------
//  Power-On Reset & WatchDog Timer Functions
//---------------------------------------------
#define  M8C_EnableWatchDog      (CPU_SCR &= ~CPUSCR_PORS_MASK)
#define  M8C_ClearWDT            (RES_WDT = 0x00)
#define  M8C_ClearWDTAndSleep    (RES_WDT = 0x38)


//------------------------------------------
//  CPU Stall for Analog PSoC Block Writes
//------------------------------------------
#define  M8C_Stall               (ASY_CR |=  ASY_CR_SYNCEN)
#define  M8C_Unstall             (ASY_CR &= ~ASY_CR_SYNCEN)


//------------------------------------------
//  Sleep, CPU Stop & Software Reset
//------------------------------------------
#define  M8C_Sleep               (CPU_SCR |= CPUSCR_SLEEP_MASK)
#define  M8C_Stop                (CPU_SCR |= CPUSCR_STOP_MASK)

#define  M8C_Reset               asm("mov A, 0"); \
                                 asm("SSC")


//------------------------------------------
// ImageCraft Code Compressor Actions
//------------------------------------------

    // Suspend Code Compressor
    // Must not span a RET or RETI instruction
    // without resuming code compression

#define Suspend_CodeCompressor  asm("or F, 0")

    // Resume Code Compressor
#define Resume_CodeCompressor   asm("add SP,0")

#endif


// end of file m8c.h
