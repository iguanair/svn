;------------------------------------------------------------------------------
;   FILENAME:   FlashBlock.inc
;    Version: 4.0.0.7, Updated on 2004/06/29 at 16:10:47
;------------------------------------------------------------------------------
;   DESCRIPTION:
;   FlashBlock library routine include file.
;
;   Copyright (C) Cypress MicroSystems 2000-2004. All rights reserved.
;
;------------------------------------------------------------------------------
;   FUNCTION NAME: bFlashWriteBlock
;
;   DESCRIPTION:
;   Writes 64 bytes of data to the flash at the specified blockId.
;
;   Regardless of the size of the buffer, this routine always writes 64
;   bytes of data. If the buffer is less than 64 bytes, then the next
;   64-N bytes of data will be written to fill the rest of flash block data.
;
;   ARGUMENTS:
;   FLASH_WRITE_STRUCT *  psBlockWriteData
;
;   pointer to a structure that holds the calling arguments and some reserved
;   space for temporary local variables.
;
;   A = MSB of pointer
;   X = LSB of pointer
;
;   1) ALLOCATE the structure and set the X pointer to the first location
;   in the structure.  Note that this structure can be allocated on the stack.
;   2) Set the first three stucture elements with the required data: block ID,
;   flash buffer pointer, and temperature.
;   3) call this routine.
;   4) Restore stack if necessary
;
;   RETURNS: in Accumuator
;   BYTE - successful if NON-Zero returned.
;
;   SIDE EFFECTS:
;   1) CPU clock temporarily set to 12MHz during call.
;
;-----------------------------------------------------------------------------
; Definition of FLASH_WRITE_STRUCT:

bARG_BlockId:              equ      0        ; block ID
pARG_FlashBuffer:          equ      1        ; flash buffer pointer - 2 bytes
cARG_Temperature:          equ      3        ; flash Tempurature
bDATA_PWErase:             equ      4        ; temporary storage (reserved)
bDATA_PWProgram:           equ      5        ; temporary storage (reserved)
bDATA_PWMultiplier:        equ      6        ; temporary storage (reserved)

FLASH_WRITE_STRUCT_SIZE:   equ      7        ; Calling frame size

; Old definitions, now deprecated, not supported
;
; ARG_SIZE:                equ      4        ; argument size
; CALLING_FRAME_SIZE:      equ      7        ; Calling frame size

;------------------------------------------------------------------------------
;   FUNCTION NAME: FlashReadBlock
;
;   DESCRIPTION:
;   Reads a specified flash block to a buffer in RAM.
;
;   ARGUMENTS:
;   FLASH_READ_STRUCT *  psBlockReadData
;
;   pointer to a structure that holds the calling arguments and some reserved
;   space for temporary local variables.
;
;   A = MSB of pointer
;   X = LSB of pointer
;
;   1) ALLOCATE the structure and set the X pointer to the first location
;   in the structure.  Note that this structure can be allocated on the stack.
;   2) Set the first three stucture elements with the required data: block ID,
;   flash buffer pointer, and count.
;   3) call this routine.
;   4) Restore stack if necessary
;
;
;  RETURNS: void
;     Data read is returned at specified pFlashBuffer.
;
;  SIDE EFFECTS:
;     none.
;
;------------------------------------------------------------------------------
; Definition of FLASH_READ_STRUCT:

bARG_BlockId:              equ      0        ; block ID
pARG_FlashBuffer:          equ      1        ; flash buffer pointer - 2 bytes
bARG_ReadCount:            equ      3        ; Read count

FLASH_READ_STRUCT_SIZE:    equ      4        ; Calling frame size

;---------------------
;  End of File
;---------------------

