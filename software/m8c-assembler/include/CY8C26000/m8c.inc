;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  M8C.INC -- M8C25000/26000 Microcontroller Family System Declarations
;;;
;;;  Copyright (c) 2000-2003 Cypress MicroSystems, Inc. All rights reserved.
;;;
;;;
;;;  This file provides address constants, bit field masks and a set of macro
;;;  facilities for the Cypress MicroSystems 25xxx and 26xxx Microcontroller
;;;  families.
;;;
;;;  Last Modified: June 17, 2004
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;=============================================================================
;; Definition of abbreviations used in the descriptions below
;;  (RW)   The register or bit supports reads and writes
;;  (WO)   The register or bit is write-only
;;  (R)    The register or bit is read-only
;;  (#)    Access to the register is bit specific (see the family datasheet)
;;=============================================================================

;;=============================================================================
;;      System Registers
;;=============================================================================

;----------------------------
;  Flag Register Bit Fields
;----------------------------
; Recommended:
FLAG_XIO_MASK:  equ 10h
FLAG_SUPER:     equ 08h
FLAG_CARRY:     equ 04h
FLAG_ZERO:      equ 02h
FLAG_GLOBAL_IE: equ 01h

; Deprecated:
FlagXIOMask:    equ 10h
FlagSuper:      equ 08h
FlagCarry:      equ 04h
FlagZero:       equ 02h
FlagGlobalIE:   equ 01h


;;=============================================================================
;;      Register Space, Bank 0
;;=============================================================================

;------------------------------------------------
;  Port Registers
;  Note: Also see this address range in Bank 1.
;------------------------------------------------
; Port 0
PRT0DR:       equ 00h          ; Port 0 Data Register              (RW)
PRT0IE:       equ 01h          ; Port 0 Interrupt Enable Register  (WO)
PRT0GS:       equ 02h          ; Port 0 Global Select Register     (WO)
; (Reserved)  equ 03h
; Port 1
PRT1DR:       equ 04h          ; Port 1 Data Register              (RW)
PRT1IE:       equ 05h          ; Port 1 Interrupt Enable Register  (WO)
PRT1GS:       equ 06h          ; Port 1 Global Select Register     (WO)
; (Reserved)  equ 07h
; Port 2
PRT2DR:       equ 08h          ; Port 2 Data Register              (RW)
PRT2IE:       equ 09h          ; Port 2 Interrupt Enable Register  (WO)
PRT2GS:       equ 0Ah          ; Port 2 Global Select Register     (WO)
; (Reserved)  equ 0Bh
; Port 3
PRT3DR:       equ 0Ch          ; Port 3 Data Register              (RW)
PRT3IE:       equ 0Dh          ; Port 3 Interrupt Enable Register  (WO)
PRT3GS:       equ 0Eh          ; Port 3 Global Select Register     (WO)
; (Reserved)  equ 0Fh
; Port 4
PRT4DR:       equ 10h          ; Port 4 Data Register              (RW)
PRT4IE:       equ 11h          ; Port 4 Interrupt Enable Register  (WO)
PRT4GS:       equ 12h          ; Port 4 Global Select Register     (WO)
; (Reserved)  equ 13h
; Port 5
PRT5DR:       equ 14h          ; Port 5 Data Register              (RW)
PRT5IE:       equ 15h          ; Port 5 Interrupt Enable Register  (WO)
PRT5GS:       equ 16h          ; Port 5 Global Select Register     (WO)
; (Reserved)  equ 17h

;------------------------------------------------
;  Digital PSoC(tm) block Registers
;  Note: Also see this address range in Bank 1.
;------------------------------------------------
; Digital PSoC block 00, Basic Type A
DBA00DR0:     equ 20h          ; data register 0                   (RO)
DBA00DR1:     equ 21h          ; data register 1                   (WO)
DBA00DR2:     equ 22h          ; data register 2                   (RW)
DBA00CR0:     equ 23h          ; control & status register 0       (RW)

; Digital PSoC block 01, Basic Type A
DBA01DR0:     equ 24h          ; data register 0                   (RO)
DBA01DR1:     equ 25h          ; data register 1                   (WO)
DBA01DR2:     equ 26h          ; data register 2                   (RW)
DBA01CR0:     equ 27h          ; control & status register 0       (RW)

; Digital PSoC block 02, Basic Type A
DBA02DR0:     equ 28h          ; data register 0                   (RO)
DBA02DR1:     equ 29h          ; data register 1                   (WO)
DBA02DR2:     equ 2Ah          ; data register 2                   (RW)
DBA02CR0:     equ 2Bh          ; control & status register 0       (RW)

; Digital PSoC block 03, Basic Type A
DBA03DR0:     equ 2Ch          ; data register 0                   (RO)
DBA03DR1:     equ 2Dh          ; data register 1                   (WO)
DBA03DR2:     equ 2Eh          ; data register 2                   (RW)
DBA03CR0:     equ 2Fh          ; control & status register 0       (RW)

; Digital PSoC block 04, Communications Type A
DCA04DR0:     equ 30h          ; data register 0                   (RO)
DCA04DR1:     equ 31h          ; data register 1                   (WO)
DCA04DR2:     equ 32h          ; data register 2                   (RW)
DCA04CR0:     equ 33h          ; control & status register 0       (RW)

; Digital PSoC block 05, Communications Type A
DCA05DR0:     equ 34h          ; data register 0                   (RO)
DCA05DR1:     equ 35h          ; data register 1                   (WO)
DCA05DR2:     equ 36h          ; data register 2                   (RW)
DCA05CR0:     equ 37h          ; control & status register 0       (RW)

; Digital PSoC block 06, Communications Type A
DCA06DR0:     equ 38h          ; data register 0                   (RO)
DCA06DR1:     equ 39h          ; data register 1                   (WO)
DCA06DR2:     equ 3Ah          ; data register 2                   (RW)
DCA06CR0:     equ 3Bh          ; control & status register 0       (RW)

; Digital PSoC block 07, Communications Type A
DCA07DR0:     equ 3Ch          ; data register 0                   (RO)
DCA07DR1:     equ 3Dh          ; data register 1                   (WO)
DCA07DR2:     equ 3Eh          ; data register 2                   (RW)
DCA07CR0:     equ 3Fh          ; control & status register 0       (RW)


;-------------------------------------
;  Analog Resource Control Registers
;-------------------------------------
AMX_IN:       equ 60h          ; Analog Input Multiplexor Control      (RW)
AMX_IN_ACI3:          equ C0h          ; column 3 input mux
AMX_IN_ACI2:          equ 30h          ; column 2 input mux
AMX_IN_ACI1:          equ 0Ch          ; column 1 input mux
AMX_IN_ACI0:          equ 03h          ; column 0 input mux

ARF_CR:       equ 63h          ; Analog Reference Control Register     (RW)
ARF_CR_BGT:           equ 80h    ; MASK: Bandgap Test
ARF_CR_HBE:           equ 40h    ; MASK: Bias level control
ARF_CR_REF:           equ 38h    ; MASK: Analog Reference controls
ARF_CR_REFPWR:        equ 07h    ; MASK: Analog Reference power
ARF_CR_APWR:          equ 04h    ; MASK: use deprecated; see datasheet
ARF_CR_SCPWR:         equ 03h    ; MASK: Switched Cap block power

CMP_CR:       equ 64h          ; comparator control                    (#)
CMP_CR_COMP3:         equ 80h          ; Column 3 comparator state     (R)
CMP_CR_COMP2:         equ 40h          ; Column 2 comparator state     (R)
CMP_CR_COMP1:         equ 20h          ; Column 1 comparator state     (R)
CMP_CR_COMP0:         equ 10h          ; Column 0 comparator state     (R)
CMP_CR_AINT3:         equ 08h          ; Column 3 interrupt source     (RW)
CMP_CR_AINT2:         equ 04h          ; Column 2 interrupt source     (RW)
CMP_CR_AINT1:         equ 02h          ; Column 1 interrupt source     (RW)
CMP_CR_AINT0:         equ 01h          ; Column 0 interrupt source     (RW)

ASY_CR:       equ 65h          ; Analog Synchronizaton Control         (#)
ASY_CR_SARCOUNT:      equ 70h          ; SAR support: resolution count (W0)
ASY_CR_SARSIGN:       equ 08h          ; SAR support: sign             (RW)
ASY_CR_SARCOL:        equ 06h          ; SAR support: column spec      (RW)
ASY_CR_SYNCEN:        equ 01h          ; Stall bit                     (RW)

;---------------------------------------------------
;  Analog PSoC block Registers
;
;  Note: the following registers are mapped into
;  both register bank 0 AND register bank 1.
;---------------------------------------------------

; Continuous Time PSoC block Type A Row 0 Col 0
; (Reserved)  equ 70h
ACA00CR0:     equ 71h          ; Control register 0                    (RW)
ACA00CR1:     equ 72h          ; Control register 1                    (RW)
ACA00CR2:     equ 73h          ; Control register 2                    (RW)

; Continuous Time PSoC block Type A Row 0 Col 1
; (Reserved)  equ 74h
ACA01CR0:     equ 75h          ; Control register 0                    (RW)
ACA01CR1:     equ 76h          ; Control register 1                    (RW)
ACA01CR2:     equ 77h          ; Control register 2                    (RW)

; Continuous Time PSoC block Type A Row 0 Col 2
; (Reserved)  equ 78h
ACA02CR0:     equ 79h          ; Control register 0                    (RW)
ACA02CR1:     equ 7Ah          ; Control register 1                    (RW)
ACA02CR2:     equ 7Bh          ; Control register 2                    (RW)

; Continuous Time PSoC block Type A Row 0 Col 3
; (Reserved)  equ 7Ch
ACA03CR0:     equ 7Dh          ; Control register 0                    (RW)
ACA03CR1:     equ 7Eh          ; Control register 1                    (RW)
ACA03CR2:     equ 7Fh          ; Control register 2                    (RW)

; Switched Cap PSoC blockType A Row 1 Col 0
ASA10CR0:     equ 80h          ; Control register 0                    (RW)
ASA10CR1:     equ 81h          ; Control register 1                    (RW)
ASA10CR2:     equ 82h          ; Control register 2                    (RW)
ASA10CR3:     equ 83h          ; Control register 3                    (RW)

; Switched Cap PSoC blockType B Row 1 Col 1
ASB11CR0:     equ 84h          ; Control register 0                    (RW)
ASB11CR1:     equ 85h          ; Control register 1                    (RW)
ASB11CR2:     equ 86h          ; Control register 2                    (RW)
ASB11CR3:     equ 87h          ; Control register 3                    (RW)

; Switched Cap PSoC blockType A Row 1 Col 2
ASA12CR0:     equ 88h          ; Control register 0                    (RW)
ASA12CR1:     equ 89h          ; Control register 1                    (RW)
ASA12CR2:     equ 8Ah          ; Control register 2                    (RW)
ASA12CR3:     equ 8Bh          ; Control register 3                    (RW)

; Switched Cap PSoC blockType B Row 1 Col 3
ASB13CR0:     equ 8Ch          ; Control register 0                    (RW)
ASB13CR1:     equ 8Dh          ; Control register 1                    (RW)
ASB13CR2:     equ 8Eh          ; Control register 2                    (RW)
ASB13CR3:     equ 8Fh          ; Control register 3                    (RW)

; Switched Cap PSoC blockType B Row 2 Col 0
ASB20CR0:     equ 90h          ; Control register 0                    (RW)
ASB20CR1:     equ 91h          ; Control register 1                    (RW)
ASB20CR2:     equ 92h          ; Control register 2                    (RW)
ASB20CR3:     equ 93h          ; Control register 3                    (RW)

; Switched Cap PSoC blockType A Row 2 Col 1
ASA21CR0:     equ 94h          ; Control register 0                    (RW)
ASA21CR1:     equ 95h          ; Control register 1                    (RW)
ASA21CR2:     equ 96h          ; Control register 2                    (RW)
ASA21CR3:     equ 97h          ; Control register 3                    (RW)

; Switched Cap PSoC blockType B Row 2 Col 2
ASB22CR0:     equ 98h          ; Control register 0                    (RW)
ASB22CR1:     equ 99h          ; Control register 1                    (RW)
ASB22CR2:     equ 9Ah          ; Control register 2                    (RW)
ASB22CR3:     equ 9Bh          ; Control register 3                    (RW)

; Switched Cap PSoC blockType A Row 2 Col 3
ASA23CR0:     equ 9Ch          ; Control register 0                    (RW)
ASA23CR1:     equ 9Dh          ; Control register 1                    (RW)
ASA23CR2:     equ 9Eh          ; Control register 2                    (RW)
ASA23CR3:     equ 9Fh          ; Control register 3                    (RW)

;------------------------------------------------
;  System and Global Resource Registers
;------------------------------------------------
INT_MSK0:     equ E0h          ; General Interrupt Mask Register       (RW)
; Recommended:
INT_MSK0_SLEEP:            equ 40h ; MASK: enable/disable sleep interrupt
INT_MSK0_GPIO:             equ 20h ; MASK: enable/disable GPIO  interrupt
INT_MSK0_ACOLUMN_3:        equ 10h ; MASK: enable/disable Analog col 3 interrupt
INT_MSK0_ACOLUMN_2:        equ 08h ; MASK: enable/disable Analog col 2 interrupt
INT_MSK0_ACOLUMN_1:        equ 04h ; MASK: enable/disable Analog col 1 interrupt
INT_MSK0_ACOLUMN_0:        equ 02h ; MASK: enable/disable Analog col 0 interrupt
INT_MSK0_VOLTAGE_MONITOR:  equ 01h ; MASK: enable/disable Volts interrupt
; Deprecated:
INT_MSK0_Sleep:            equ 40h  ; MASK: enable/disable sleep interrupt
INT_MSK0_GPIO:             equ 20h  ; MASK: enable/disable GPIO  interrupt
INT_MSK0_AColumn3:         equ 10h  ; MASK: enable/disable Analog col 3 interrupt
INT_MSK0_AColumn2:         equ 08h  ; MASK: enable/disable Analog col 2 interrupt
INT_MSK0_AColumn1:         equ 04h  ; MASK: enable/disable Analog col 1 interrupt
INT_MSK0_AColumn0:         equ 02h  ; MASK: enable/disable Analog col 0 interrupt
INT_MSK0_VoltageMonitor:   equ 01h  ; MASK: enable/disable Volts interrupt

INT_MSK1:     equ E1h          ; Digital PSoC block Mask Register      (RW)
INT_MSK1_DCA07:            equ 80h  ; MASK: enable/disable DCA07 block interrupt
INT_MSK1_DCA06:            equ 40h  ; MASK: enable/disable DCA06 block interrupt
INT_MSK1_DCA05:            equ 20h  ; MASK: enable/disable DCA05 block interrupt
INT_MSK1_DCA04:            equ 10h  ; MASK: enable/disable DCA04 block interrupt
INT_MSK1_DBA03:            equ 08h  ; MASK: enable/disable DBA03 block interrupt
INT_MSK1_DBA02:            equ 04h  ; MASK: enable/disable DBA02 block interrupt
INT_MSK1_DBA01:            equ 02h  ; MASK: enable/disable DBA01 block interrupt
INT_MSK1_DBA00:            equ 01h  ; MASK: enable/disable DBA00 block interrupt

INT_VC:       equ E2h          ; Interrupt vector register             (RW)
RES_WDT:      equ E3h          ; Watch Dog Timer Register              (RW)

; DECIMATOR Registers
DEC_DH:       equ E4h          ; Data Register (high byte)             (RW)
DEC_DL:       equ E5h          ; Data Register ( low byte)             (RO)
DEC_CR:       equ E6h          ; Data Control Register                 (RW)

; Multiplier and MAC (Multiply/Accumulate) Unit
MUL_X:        equ E8h          ; Multiplier X Register (write)            (WO)
MUL_Y:        equ E9h          ; Multiplier Y Register (write)            (WO)
MUL_DH:       equ EAh          ; Multiplier Result Data (high byte read)  (RO)
MUL_DL:       equ EBh          ; Multiplier Result Data ( low byte read)  (RO)
MAC_X:        equ ECh          ; MAC X register (write) [also see ACC_DR1](WO)
MAC_Y:        equ EDh          ; MAC Y register (write) [also see ACC_DR0](WO)
MAC_CL0:      equ EEh          ; MAC Clear Accum (write)[also see ACC_DR3](WO)
MAC_CL1:      equ EFh          ; MAC Clear Accum (write)[also see ACC_DR2](WO)
ACC_DR1:      equ ECh          ; MAC Accumulator (Read, byte 1)           (RO)
ACC_DR0:      equ EDh          ; MAC Accumulator (Read, byte 0)           (RO)
ACC_DR3:      equ EEh          ; MAC Accumulator (Read, byte 3)           (RO)
ACC_DR2:      equ EFh          ; MAC Accumulator (Read, byte 2)           (RO)

; THE FOLLOWING HAS BEEN COMMENTED OUT: This register in not accessible
; during normal operation:
; Test Mode mapping of the CPU Flag (F) Register
; CPU_FLAG:     equ F7h          ; NOTE: Only mapped when in Test Mode !!!


;------------------------------------------------------
;  System Status and Control Registers
;
;  Note: The following registers are mapped into both
;        register bank 0 AND register bank 1.
;------------------------------------------------------
CPU_SCR:      equ FFh   ;                                                 (#)
; Recommended:
CPUSCR_GIE_MASK:        equ 80h   ; MASK: Global Interrupt Enable shadow
CPUSCR_WDRS_MASK:       equ 20h   ; MASK: Watch Dog Timer Reset
CPUSCR_PORS_MASK:       equ 10h   ; MASK: power-on reset bit PORS
CPUSCR_SLEEP_MASK:      equ 08h   ; MASK: Enable Sleep
CPUSCR_STOP_MASK:       equ 01h   ; MASK: Halt CPU bit
; Deprecated:
CPUSCR_GIEMask:         equ 80h   ; MASK: Global Interrupt Enable shadow
CPUSCR_WDRSMask:        equ 20h   ; MASK: Watch Dog Timer Reset
CPUSCR_PORSMask:        equ 10h   ; MASK: power-on reset bit PORS
CPUSCR_SleepMask:       equ 08h   ; MASK: Enable Sleep
CPUSCR_StopMask:        equ 01h   ; MASK: Halt CPU bit


;;=============================================================================
;;      Register Space, Bank 1
;;=============================================================================

;------------------------------------------------
;  Port Registers
;  Note: Also see this address range in Bank 0.
;------------------------------------------------
; Port 0
PRT0DM0:      equ 00h          ; Port 0 Drive Mode 0               (WO)
PRT0DM1:      equ 01h          ; Port 0 Drive Mode 1               (WO)
PRT0IC0:      equ 02h          ; Port 0 Interrupt Control 0        (WO)
PRT0IC1:      equ 03h          ; Port 0 Interrupt Control 1        (WO)

; Port 1
PRT1DM0:      equ 04h          ; Port 1 Drive Mode 0               (WO)
PRT1DM1:      equ 05h          ; Port 1 Drive Mode 1               (WO)
PRT1IC0:      equ 06h          ; Port 1 Interrupt Control 0        (WO)
PRT1IC1:      equ 07h          ; Port 1 Interrupt Control 1        (WO)

; Port 2
PRT2DM0:      equ 08h          ; Port 2 Drive Mode 0               (WO)
PRT2DM1:      equ 09h          ; Port 2 Drive Mode 1               (WO)
PRT2IC0:      equ 0Ah          ; Port 2 Interrupt Control 0        (WO)
PRT2IC1:      equ 0Bh          ; Port 2 Interrupt Control 1        (WO)

; Port 3
PRT3DM0:      equ 0Ch          ; Port 3 Drive Mode 0               (WO)
PRT3DM1:      equ 0Dh          ; Port 3 Drive Mode 1               (WO)
PRT3IC0:      equ 0Eh          ; Port 3 Interrupt Control 0        (WO)
PRT3IC1:      equ 0Fh          ; Port 3 Interrupt Control 1        (WO)

; Port 4
PRT4DM0:      equ 10h          ; Port 4 Drive Mode 0               (WO)
PRT4DM1:      equ 11h          ; Port 4 Drive Mode 1               (WO)
PRT4IC0:      equ 12h          ; Port 4 Interrupt Control 0        (WO)
PRT4IC1:      equ 13h          ; Port 4 Interrupt Control 1        (WO)

; Port 5
PRT5DM0:      equ 14h          ; Port 5 Drive Mode 0               (WO)
PRT5DM1:      equ 15h          ; Port 5 Drive Mode 1               (WO)
PRT5IC0:      equ 16h          ; Port 5 Interrupt Control 0        (WO)
PRT5IC1:      equ 17h          ; Port 5 Interrupt Control 1        (WO)


;------------------------------------------------
;  Digital PSoC(tm) block Registers
;  Note: Also see this address range in Bank 0.
;------------------------------------------------

; Digital PSoC block 00, Basic Type A
DBA00FN:      equ 20h          ; Function Register                 (RW)
DBA00IN:      equ 21h          ;    Input Register                 (RW)
DBA00OU:      equ 22h          ;   Output Register                 (RW)
; (Reserved)  equ 23h

; Digital PSoC block 01, Basic Type A
DBA01FN:      equ 24h          ; Function Register                 (RW)
DBA01IN:      equ 25h          ;    Input Register                 (RW)
DBA01OU:      equ 26h          ;   Output Register                 (RW)
; (Reserved)  equ 27h

; Digital PSoC block 02, Basic Type A
DBA02FN:      equ 28h          ; Function Register                 (RW)
DBA02IN:      equ 29h          ;    Input Register                 (RW)
DBA02OU:      equ 2Ah          ;   Output Register                 (RW)
; (Reserved)  equ 2Bh

; Digital PSoC block 03, Basic Type A
DBA03FN:      equ 2Ch          ; Function Register                 (RW)
DBA03IN:      equ 2Dh          ;    Input Register                 (RW)
DBA03OU:      equ 2Eh          ;   Output Register                 (RW)
; (Reserved)  equ 2Fh

; Digital PSoC block 04, Communications Type A
DCA04FN:      equ 30h          ; Function Register                 (RW)
DCA04IN:      equ 31h          ;    Input Register                 (RW)
DCA04OU:      equ 32h          ;   Output Register                 (RW)
; (Reserved)  equ 33h

; Digital PSoC block 05, Communications Type A
DCA05FN:      equ 34h          ; Function Register                 (RW)
DCA05IN:      equ 35h          ;    Input Register                 (RW)
DCA05OU:      equ 36h          ;   Output Register                 (RW)
; (Reserved)  equ 37h

; Digital PSoC block 06, Communications Type A
DCA06FN:      equ 38h          ; Function Register                 (RW)
DCA06IN:      equ 39h          ;    Input Register                 (RW)
DCA06OU:      equ 3Ah          ;   Output Register                 (RW)
; (Reserved)  equ 3Bh

; Digital PSoC block 07, Communications Type A
DCA07FN:      equ 3Ch          ; Function Register                 (RW)
DCA07IN:      equ 3Dh          ;    Input Register                 (RW)
DCA07OU:      equ 3Eh          ;   Output Register                 (RW)
; (Reserved)  equ 3Fh

;------------------------------------------------
;  System and Global Resource Registers
;  Note: Also see this address range in Bank 0.
;------------------------------------------------

CLK_CR0:      equ 60h          ; Analog Column Clock Select Register 0    (RW)
; Recommended:
CLK_CR0_ACOLUMN_3:    equ C0h  ; MASK: Specify clock for analog cloumn
CLK_CR0_ACOLUMN_2:    equ 30h  ; MASK: Specify clock for analog cloumn
CLK_CR0_ACOLUMN_1:    equ 0Ch  ; MASK: Specify clock for analog cloumn
CLK_CR0_ACOLUMN_0:    equ 03h  ; MASK: Specify clock for analog cloumn
; Deprecated:
CLK_CR0_AColumn3:     equ C0h  ; MASK: Specify clock for analog cloumn
CLK_CR0_AColumn2:     equ 30h  ; MASK: Specify clock for analog cloumn
CLK_CR0_AColumn1:     equ 0Ch  ; MASK: Specify clock for analog cloumn
CLK_CR0_AColumn0:     equ 03h  ; MASK: Specify clock for analog cloumn

CLK_CR1:      equ 61h          ; Analog Clock Source Select Register 1    (RW)
CLK_CR1_SHDIS:        equ 40h  ; MASK: Sample and Hold Disable (all Columns)
CLK_CR1_ACLK1:        equ 38h  ; MASK: Digital PSoC block for analog source
CLK_CR1_ACLK2:        equ 07h  ; MASK: Digital PSoC block for analog source

ABF_CR:       equ 62h          ; Analog Output Buffer Control Register   (RW)
ABF_CR_ACOL1MUX:      equ 80h  ; MASK: Analog Column 1 Mux control
ABF_CR_ACOL2MUX:      equ 40h  ; MASK: Analog Column 2 Mux control
ABF_CR_ABUF1EN:       equ 20h  ; MASK: Enable ACol 1 analog buffer (P0[5])
ABF_CR_ABUF2EN:       equ 10h  ; MASK: Enable ACol 2 analog buffer (P0[4])
ABF_CR_ABUF0EN:       equ 08h  ; MASK: Enable ACol 0 analog buffer (P0[3])
ABF_CR_ABUF3EN:       equ 04h  ; MASK: Enable ACol 3 analog buffer (P0[2])
ABF_CR_BYPASS:        equ 02h  ; MASK: Bypass the analog buffers
ABF_CR_PWR:           equ 01h  ; MASK: High power mode on all analog buffers

AMD_CR:       equ 63h          ; Analog Modulator Control Register       (RW)
AMD_CR_AMOD2:         equ 0Ch  ; MASK: Modulation source for analog column 2
AMD_CR_AMOD0:         equ 03h  ; MASK: Modulation source for analog column 1

;------------------------------------------------
;  Clock and System Control Registers
;------------------------------------------------

OSC_CR0:      equ E0h          ; System Oscillator Control Register 0     (RW)
; Recommended:
OSC_CR0_32K_SELECT:   equ 80h  ; MASK: Enable/Disable External XTAL Oscillator
OSC_CR0_PLL_MODE:     equ 40h  ; MASK: Enable/Disable PLL
OSC_CR0_SLEEP:        equ 18h  ; MASK: Set Sleep timer freq/period
OSC_CR0_SLEEP_512Hz:  equ 00h  ;     Set sleep bits for 1.95ms period
OSC_CR0_SLEEP_64Hz:   equ 08h  ;     Set sleep bits for 15.6ms period
OSC_CR0_SLEEP_8Hz:    equ 10h  ;     Set sleep bits for 125ms period
OSC_CR0_SLEEP_1Hz:    equ 18h  ;     Set sleep bits for 1 sec period
OSC_CR0_CPU:          equ 07h  ; MASK: Set CPU Frequency
OSC_CR0_CPU_3MHz:     equ 00h  ;     set CPU Freq bits for 3MHz Operation
OSC_CR0_CPU_6MHz:     equ 01h  ;     set CPU Freq bits for 6MHz Operation
OSC_CR0_CPU_12MHz:    equ 02h  ;     set CPU Freq bits for 12MHz Operation
OSC_CR0_CPU_24MHz:    equ 03h  ;     set CPU Freq bits for 24MHz Operation
OSC_CR0_CPU_1d5MHz:   equ 04h  ;     set CPU Freq bits for 1.5MHz Operation
OSC_CR0_CPU_750kHz:   equ 05h  ;     set CPU Freq bits for 750kHz Operation
OSC_CR0_CPU_187d5kHz: equ 06h  ;     set CPU Freq bits for 187.5kHz Operation
OSC_CR0_CPU_93d7kHz:  equ 07h  ;     set CPU Freq bits for 93.7kHz Operation
; Deprecated
OSC_CR0_32K_Select:   equ 80h  ; MASK: Enable/Disable External XTAL Oscillator
OSC_CR0_PLL_Mode:     equ 40h  ; MASK: Enable/Disable PLL
OSC_CR0_Sleep:        equ 18h  ; MASK: Set Sleep timer freq/period
OSC_CR0_Sleep_512Hz:  equ 00h  ;     Set sleep bits for 1.95ms period
OSC_CR0_Sleep_64Hz:   equ 08h  ;     Set sleep bits for 15.6ms period
OSC_CR0_Sleep_8Hz:    equ 10h  ;     Set sleep bits for 125ms period
OSC_CR0_Sleep_1Hz:    equ 18h  ;     Set sleep bits for 1 sec period

OSC_CR1:      equ E1h          ; System V1/V2 Divider Control Register   (RW)
OSC_CR1_V1:           equ F0h  ; MASK System V1 24MHz divider
OSC_CR1_V2:           equ 0Fh  ; MASK System V2 24MHz divider

;Reserved     equ E2h
VLT_CR:       equ E3h          ; Voltage Monitor Control Register        (RW)
VLT_CR_SMP:           equ 80h  ; MASK: Switch-mode Pump Enable
VLT_CR_VM:            equ 07h  ; MASK: Voltage Monitor Threshhold Select

IMO_TR:       equ E8h          ; Internal Main Oscillator Trim Register  (WO)
ILO_TR:       equ E9h          ; Internal Low-speed Oscillator Trim      (WO)
BDG_TR:       equ EAh          ; Band Gap Trim Register                  (WO)
ECO_TR:       equ EBh          ; External Oscillator Trim Register       (WO)

;;=============================================================================
;;      M8C System Macros
;;  These macros should be used when their functions are needed.
;;=============================================================================

;----------------------------------------------------
;  Swapping Register Banks
;----------------------------------------------------
    macro M8C_SetBank0
    and   F, ~FLAG_XIO_MASK
    endm

    macro M8C_SetBank1
    or    F, FLAG_XIO_MASK
    endm

;----------------------------------------------------
;  Global Interrupt Enable/Disable
;----------------------------------------------------
    macro M8C_EnableGInt
    or    F, FLAG_GLOBAL_IE
    endm

    macro M8C_DisableGInt
    and   F, ~FLAG_GLOBAL_IE
    endm

;----------------------------------------------------
;  Enable/Disable Interrupt Mask
;
;  Use the following macros to enable/disable
;  bits in the Interrupt mask registers,
;  INT_MSK0 or INT_MSK1.
;
;  This is a fix to a noted problem in which an
;  inadvertant reset can occur if an interrupt occurs
;  while clearing an interrupt mask bit.
;
;  Usage:    M8C_DisableIntMask INT_MSKN, MASK
;            M8C_EnableIntMask  INT_MSKN, MASK
;
;  where INT_MSKN is INT_MSK0 or INT_MSK1
;        and MASK is the bit set to enable or disable
;----------------------------------------------------
; Disable Interrupt Bit Mask(s)
	
DISABLE_INT_FIX:   equ   1

    macro M8C_DisableIntMask
if DISABLE_INT_FIX
    mov   A, reg[CPU_SCR]           ; save the current Global interrupt state
    M8C_DisableGInt                 ; disable global interrupts
endif
    and   reg[@0], ~@1              ; disable specified interrupt enable bit
if DISABLE_INT_FIX
    and   A, CPUSCR_GIEMask         ; determine if global interrupt was set
    jz    . + 4                     ; jump if global interrupt disabled
    M8C_EnableGInt                  ; set global interrupt
endif
    endm

; Enable Interrupt Bit Mask(s)
    macro M8C_EnableIntMask
    or    reg[@0], @1
    endm

;----------------------------------------------------
;  Power-On Reset & WatchDog Timer Functions
;----------------------------------------------------
    macro M8C_EnableWatchDog
    and   reg[CPU_SCR], ~CPUSCR_PORS_MASK
    endm

    macro M8C_ClearWDT
    mov   reg[RES_WDT], 00h
    endm

    macro M8C_ClearWDTAndSleep
    mov   reg[RES_WDT], 38h
    endm

;----------------------------------------------------
;  CPU Stall for Analog PSoC Block Writes
;----------------------------------------------------
    macro M8C_Stall
    or    reg[ASY_CR], ASY_CR_SYNCEN
    endm

    macro M8C_Unstall
    and   reg[ASY_CR], ~ASY_CR_SYNCEN
    endm

;----------------------------------------------------
;  Sleep, CPU Stop & Software Reset
;----------------------------------------------------
    macro M8C_Sleep
    or    reg[CPU_SCR], CPUSCR_SLEEP_MASK
    ; The next instruction to be executed depends on the state of the
    ; various interrupt enable bits. If some interrupts are enabled
    ; and the global interrupts are disabled, the next instruction will
    ; be the one that follows the invocation of this macro. If global
    ; interrupts are also enabled then the next instruction will be
    ; from the interrupt vector table. If no interrupts are enabled
    ; then the CPU sleeps forever.
    endm

    macro M8C_Stop
    ; In general, you probably don't want to do this, but here's how:
    or    reg[CPU_SCR], CPUSCR_STOP_MASK
    ; Next instruction to be executed is located in the interrupt
    ; vector table entry for Power-On Reset.
    endm

    macro M8C_Reset
    ; Restore CPU to the power-on reset state.
    mov A, 0
    SSC
    ; Next non-supervisor instruction will be at interrupt vector 0.
    endm

;----------------------------------------------------
; ImageCraft Code Compressor Actions
;----------------------------------------------------
    ; Suspend Code Compressor
    ; Must not span a RET or RETI instruction
    ; without resuming code compression
    macro Suspend_CodeCompressor
    or   F, 0
    endm

    ; Resume Code Compression
    macro Resume_CodeCompressor
    add  SP, 0
    endm

; end of file m8c.inc
