/******************************************************************************
*  FILENAME:      E2PROMLIB.h for PSoC devices:  CY7C603xx 
*   @Version@
*******************************************************************************
*  DESCRIPTION:
*     Header file for E2PROM Library routines.  These routines use the
*     FlashBlock functions to emulate an EEPROM device.  These algorithms
*     translate flash block oriented operations into byte-wise operations.
*
*     See E2PROMLIB.INC and E2PROMLIB.ASM for specific detail of operation.
*******************************************************************************
*   Copyright (c) Cypress Semiconductor 2000-2005. All Rights Reserved.
******************************************************************************/

/* include the global header file */
#include <m8c.h>

/* Create pragmas to support proper argument and return value passing */
#pragma  fastcall16 bE2Write                 // class 4 function
#pragma  fastcall16 E2Read                   // class 4 function

BYTE bE2Write( WORD wAddr, BYTE * pbData, WORD wByteCount, CHAR cTermperature );
void E2Read( WORD wAddr, BYTE * pbDataDest, WORD wByteCount );

/**************************
*  bE2Write Return Values
***************************/
#define  NOERROR           0        // Successfull completion
#define  FAILURE          -1        // Error condition
#define  STACKOVERFLOW    -2        // Error Stack Overflow

//  End of File
