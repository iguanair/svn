//=============================================================================
//
//    m8c.h: CY7C63300 Microcontroller Family System Declarations
//
//    Copyright: Cypress Semiconductor 2004, 2005. All Rights Reserved.
//
//
//    This file provides address constants, bit field masks and a set of macro
//    facilities for the Cypress Semiconductor 633xx Microcontroller families.
//
//    Last Modified: 
//
//=============================================================================

#ifndef M8C_C_HEADER
#define M8C_C_HEADER

//-----------------------------------------------
// Define System Types
//-----------------------------------------------
typedef  unsigned char  BOOL;
typedef  unsigned char  BYTE;
typedef  signed   char  CHAR;
typedef  unsigned int   WORD;
typedef  signed   int   INT;
typedef  unsigned long  DWORD;
typedef  signed   long  LONG;

//-----------------------------------------------
// Define Boolean TRUE/FALSE
//-----------------------------------------------
#define  TRUE  ((BOOL) 1)
#define  FALSE ((BOOL) 0)


//=============================================================================
//=============================================================================
//      System Registers
//=============================================================================
//=============================================================================

//-----------------------------------------------
//  Flag Register Bit Fields
//-----------------------------------------------
#define  FLAG_XIO_MASK     (0x10)
#define  FLAG_SUPER        (0x08)
#define  FLAG_CARRY        (0x04)
#define  FLAG_ZERO         (0x02)
#define  FLAG_GLOBAL_IE    (0x01)


//=============================================================================
//=============================================================================
//      Register Space, Bank 0
//=============================================================================
//=============================================================================

//-----------------------------------------------
//  Port Data Registers
//  Note: Also see this address range in Bank 1.
//-----------------------------------------------
#pragma  ioport   P0DATA:     0x000  // Port 0 Data Register
BYTE              P0DATA;
#pragma  ioport   P1DATA:     0x001  // Port 1 Data Register
BYTE              P1DATA;
#pragma  ioport   P2DATA:     0x002  // Port 2 Data Register
BYTE              P2DATA;
#pragma  ioport   P3DATA:     0x003  // Port 3 Data Register
BYTE              P3DATA;
#pragma  ioport   P4DATA:     0x004  // Port 4 Data Register
BYTE              P4DATA;
//  PSoC Compatability
#pragma  ioport   PRT0DR:     0x000  // Port 0 Data Register
BYTE              PRT0DR;
#pragma  ioport   PRT1DR:     0x001  // Port 1 Data Register
BYTE              PRT1DR;
#pragma  ioport   PRT2DR:     0x002  // Port 2 Data Register
BYTE              PRT2DR;
#pragma  ioport   PRT3DR:     0x003  // Port 3 Data Register
BYTE              PRT3DR;
#pragma  ioport   PRT4DR:     0x004  // Port 4 Data Register
BYTE              PRT4DR;

//-----------------------------------------------
//  Port/Pin Configuration Registers
//-----------------------------------------------

#pragma  ioport   P00CR:      0x005  // P0.0 Configuration Register
BYTE              P00CR;
#pragma  ioport   P01CR:      0x006  // P0.1 Configuration Register
BYTE              P01CR;
#pragma  ioport   P02CR:      0x007  // P0.2 Configuration Register
BYTE              P02CR;
#pragma  ioport   P03CR:      0x008  // P0.3 Configuration Register
BYTE              P03CR;
#pragma  ioport   P04CR:      0x009  // P0.4 Configuration Register
BYTE              P04CR;
#pragma  ioport   P05CR:      0x00A  // P0.5 Configuration Register
BYTE              P05CR;
#pragma  ioport   P06CR:      0x00B  // P0.6 Configuration Register
BYTE              P06CR;
#pragma  ioport   P07CR:      0x00C  // P0.7 Configuration Register
BYTE              P07CR;
#pragma  ioport   P10CR:      0x00D  // P1.0 Configuration Register
BYTE              P10CR;
#pragma  ioport   P11CR:      0x00E  // P1.1 Configuration Register
BYTE              P11CR;
#pragma  ioport   P12CR:      0x00F  // P1.2 Configuration Register
BYTE              P12CR;
#pragma  ioport   P13CR:      0x010  // P1.3 Configuration Register
BYTE              P13CR;
#pragma  ioport   P14CR:      0x011  // P1.4 Configuration Register
BYTE              P14CR;
#pragma  ioport   P15CR:      0x012  // P1.5 Configuration Register
BYTE              P15CR;
#pragma  ioport   P16CR:      0x013  // P1.6 Configuration Register
BYTE              P16CR;
#pragma  ioport   P17CR:      0x014  // P1.7 Configuration Register
BYTE              P17CR;
#pragma  ioport   P2CR:       0x015  // P2.0-P2.7 Configuration Register
BYTE              P2CR;
#pragma  ioport   P3CR:       0x016  // P3.0-P3.7 Configuration Register
BYTE              P3CR;
#pragma  ioport   P4CR:       0x017  // P4.0-P4.7 Configuration Register
BYTE              P4CR;

//-----------------------------------------------
//  Timer Registers
//-----------------------------------------------
#pragma  ioport   FRTMRL:     0x020  // Free Running Timer Low
BYTE              FRTMRL;
#pragma  ioport   FRTMRH:     0x021  // Free Running Timer High
BYTE              FRTMRH;
#pragma  ioport   TCAP0R:     0x022  // Capture 0 Rising
BYTE              TCAP0R;
#pragma  ioport   TCAP1R:     0x023  // Capture 1 Rising
BYTE              TCAP1R;
#pragma  ioport   TCAP0F:     0x024  // Capture 0 Falling
BYTE              TCAP0F;
#pragma  ioport   TCAP1F:     0x025  // Capture 1 Falling
BYTE              TCAP1F;
#pragma  ioport   PITMRL:     0x026  // Programmable Interval Timer Low
BYTE              PITMRL;
#pragma  ioport   PITMRH:     0x027  // Programmable Interval Timer High
BYTE              PITMRH;
#pragma  ioport   PIRL:       0x028  // Programmable Interval Timer Reload Low
BYTE              PIRL;
#pragma  ioport   PIRH:       0x029  // Programmable Interval Timer Reload High
BYTE              PIRH;
#pragma  ioport   TMRCR:      0x02A  // Timer Configuration Register
BYTE              TMRCR;
#pragma  ioport   TCAPINTE:   0x02B  // Capture Timer Interrupt Enable
BYTE              TCAPINTE;
#pragma  ioport   TCAPINTS:   0x02C  // Capture Timer Interrupt Status
BYTE              TCAPINTS;

//-----------------------------------------------
//  Clock Configuration Registers
//-----------------------------------------------
#pragma  ioport   CPUCLKCR:   0x030  // CPU Clock Configuration Register
BYTE              CPUCLKCR;
#pragma  ioport   TMRCLKCR:   0x031  // Timer Clock Configuration Register
BYTE              TMRCLKCR;
#pragma  ioport   CLKIOCR:    0x032  // Clock I/O Configuration Register
BYTE              CLKIOCR;

//-----------------------------------------------
//  Oscillator Configuration Registers
//-----------------------------------------------
#pragma  ioport   IOSCTR:     0x034  // Internal Oscillator Trim Register
BYTE              IOSCTR;
#pragma  ioport   XOSCTR:     0x035  // Crystal Oscillator Trim Register
BYTE              XOSCTR;
#pragma  ioport   LPOSCTR:    0x036  // Low Power Oscillator Trim Register
BYTE              LPOSCTR;

//-----------------------------------------------
//  SPI Configuration/Data Registers
//-----------------------------------------------
#pragma  ioport   SPIDATA:    0x03C  // SPI Data Register
BYTE              SPIDATA;
#pragma  ioport   SPICR:      0x03D  // SPI Configuration Register
BYTE              SPICR;
//-----------------------------------------------
//  USB SIE Configuration/Data Registers
//-----------------------------------------------
#pragma  ioport   USBCR:      0x040  // USB Configuration Register
BYTE              USBCR;
#pragma  ioport   EP0CNT:     0x041  // Endpoint 0 Count Register
BYTE              EP0CNT;
#pragma  ioport   EP1CNT:     0x042  // Endpoint 1 Count Register
BYTE              EP1CNT;
#pragma  ioport   EP2CNT:     0x043  // Endpoint 1 Count Register
BYTE              EP2CNT;
#pragma  ioport   EP0MODE:    0x044  // Endpoint 0 Mode  Register
BYTE              EP0MODE;
#pragma  ioport   EP1MODE:    0x045  // Endpoint 1 Mode  Register
BYTE              EP1MODE;
#pragma  ioport   EP2MODE:    0x046  // Endpoint 1 Mode  Register
BYTE              EP2MODE;
#pragma  ioport   EP0DATA:    0x050  // Endpoint 0 Data  Register (50h-57h)
BYTE              EP0DATA;
#pragma  ioport   EP1DATA:    0x058  // Endpoint 1 Data  Register (58h-5Fh)
BYTE              EP1DATA;
#pragma  ioport   EP2DATA:    0x060  // Endpoint 1 Data  Register (60h-67h)
BYTE              EP2DATA;

//-----------------------------------------------
//  Band-gap/TRIMBUF Configuration Registers
//-----------------------------------------------
#pragma  ioport   BGAPTR:     0x070  // Band-gap Trim Register
BYTE              BGAPTR;
#pragma  ioport   TRIM0:      0x071  // TRIMBUF Trim Register 0
BYTE              TRIM0;
#pragma  ioport   TRIM1:      0x072  // TRIMBUF Trim Register 1
BYTE              TRIM1;

//-----------------------------------------------
//  VREG Configuration Register
//-----------------------------------------------
#pragma  ioport   VREGCR:     0x073  // VREG Configuration Register
BYTE              VREGCR;
#define  VREGCR_KEEP_ALIVE      (0x02)
#define  VREGCR_ENABLE          (0x01)

//-----------------------------------------------
//  USB Transceiver Configuration Registers
//-----------------------------------------------
#pragma  ioport   USBXCR:     0x074  // USB Transceiver Configuration Register
BYTE              USBXCR;
#define  USBXCR_ENABLE          (0x80)
#define  USBXCR_FORCE           (0x01)

//-----------------------------------------------
//  Watchdog Timer Reset
//-----------------------------------------------
#pragma  ioport   RESWDT:     0x0E3  // Watchdog Timer Reset
BYTE              RESWDT;
#pragma  ioport   RES_WDT:    0x0E3  // Watchdog Timer Reset (PSoC)
BYTE              RES_WDT;

//-----------------------------------------------
//  System and Global Resource Registers
//-----------------------------------------------
#pragma  ioport   INT_CLR0:   0x0DA  // Interrupt Clear Register 0
BYTE              INT_CLR0;
#pragma  ioport   INT_CLR1:   0x0DB  // Interrupt Clear Register 1
BYTE              INT_CLR1;
#pragma  ioport   INT_CLR2:   0x0DC  // Interrupt Clear Register 2
BYTE              INT_CLR2;

#pragma  ioport   INT_MSK3:   0x0DE  // Software Mask Register
BYTE              INT_MSK3;
#define INT_MSK3_ENSWINT          (0x80)

#pragma  ioport   INT_MSK2:   0x0DF  // General Interrupt Mask Register
BYTE              INT_MSK2;
#define  INT_MSK2_GPIO_PORT4      (0x40)
#define  INT_MSK2_GPIO_PORT3      (0x20)
#define  INT_MSK2_GPIO_PORT2      (0x10)
#define  INT_MSK2_PS2_DATA_LOW    (0x08)
#define  INT_MSK2_GPIO_INT2       (0x04)
#define  INT_MSK2_CTR_16_WRAP     (0x02)
#define  INT_MSK2_TCAP1           (0x01)

#pragma  ioport   INT_MSK0:   0x0E0  // General Interrupt Mask Register
BYTE              INT_MSK0;
#define  INT_MSK0_GPIO_PORT1      (0x80)
#define  INT_MSK0_SLEEP           (0x40)
#define  INT_MSK0_GPIO_INT1       (0x20)
#define  INT_MSK0_GPIO_PORT0      (0x10)
#define  INT_MSK0_SPI_RX          (0x08)
#define  INT_MSK0_SPI_TX          (0x04)
#define  INT_MSK0_GPIO_INT0       (0x02)
#define  INT_MSK0_POR_LVD         (0x01)

#pragma  ioport   INT_MSK1:   0x0E1  // General Interrupt Mask Register
BYTE              INT_MSK1;
#define  INT_MSK1_TCAP0           (0x80)
#define  INT_MSK1_PIT             (0x40)
#define  INT_MSK1_MS_TIMER        (0x20)
#define  INT_MSK1_USB_ACTIVITY    (0x10)
#define  INT_MSK1_USB_BUS_RESET   (0x08)
#define  INT_MSK1_USB_EP2         (0x04)
#define  INT_MSK1_USB_EP1         (0x02)
#define  INT_MSK1_USB_EP0         (0x01)

#pragma  ioport   INT_VC:     0x0E2  // Pending Interrupt
BYTE              INT_VC;

//-----------------------------------------------
//  System Status and Control Register
//
//        Register bank 1.
//-----------------------------------------------
#pragma  ioport   OSC_CR0:    0x1E0  // System Oscillator Control Register 0
BYTE              OSC_CR0;
#define OSC_CR0_NO_BUZZ               (0x20)
#define OSC_CR0_SLEEP                 (0x18)
#define OSC_CR0_SLEEP_512Hz           (0x00)
#define OSC_CR0_SLEEP_64Hz            (0x08)
#define OSC_CR0_SLEEP_8Hz             (0x10)
#define OSC_CR0_SLEEP_1Hz             (0x18)
#define OSC_CR0_CPU                   (0x07)
#define OSC_CR0_CPU_3MHz              (0x00)
#define OSC_CR0_CPU_6MHz              (0x01)
#define OSC_CR0_CPU_12MHz             (0x02)
#define OSC_CR0_CPU_24MHz             (0x03)
#define OSC_CR0_CPU_1d5MHz            (0x04)
#define OSC_CR0_CPU_750kHz            (0x05)
#define OSC_CR0_CPU_187d5kHz          (0x06)

//-----------------------------------------------
//  Note: the following register is mapped into
//  both register bank 0 AND register bank 1.
//-----------------------------------------------
#pragma  ioport   CPU_SCR:   0x0FF  // System Status and Control Register 0
BYTE              CPU_SCR;
#define  CPU_SCR_GIE_MASK     (0x80)
#define  CPU_SCR_WDRS_MASK    (0x20)
#define  CPU_SCR_PORS_MASK    (0x10)
#define  CPU_SCR_SLEEP_MASK   (0x08)
#define  CPU_SCR_STOP_MASK    (0x01)

//=============================================================================
//=============================================================================
//      M8C System Macros
//=============================================================================
//=============================================================================


//-----------------------------------------------
//  Swapping Register Banks
//-----------------------------------------------
#define  M8C_SetBank0            asm("and F, EFh")
#define  M8C_SetBank1            asm("or  F, 10h")

//-----------------------------------------------
//  Global Interrupt Enable/Disable
//-----------------------------------------------
#define  M8C_EnableGInt          asm("or  F, 01h")
#define  M8C_DisableGInt         asm("and F, FEh")

//---------------------------------------------------
// Enable/Disable Interrupt Mask
//
// Usage:    M8C_DisableIntMask INT_MSKN, MASK
//           M8C_EnableIntMask  INT_MSKN, MASK
//
// where INT_MSKN is INT_MSK0, INT_MSK1 or INT_MSK3
//       and MASK is the bit set to enable or disable
//---------------------------------------------------
// Disable Interrupt Bit Mask(s)
#define M8C_DisableIntMask( INT_MSKN_REG, MASK )   (INT_MSKN_REG &= ~MASK)

// Enable Interrupt Bit Mask(s)
#define M8C_EnableIntMask( INT_MSKN_REG, MASK )    (INT_MSKN_REG |= MASK)

//---------------------------------------------------
// Clear Posted Interrupt Flag
//
// Usage:    M8C_ClearIntFlag   INT_CLRN, MASK
//
// where INT_MSKN is INT_CLR0, INT_CLR1 or INT_CLR2
//       and MASK is the bit set to enable or disable
//---------------------------------------------------
#define M8C_ClearIntFlag( INT_CLRN_REG, MASK )    (INT_CLRN_REG = ~MASK)


//-----------------------------------------------
//  Power-On Reset & WatchDog Timer Functions
//-----------------------------------------------
#define  M8C_EnableWatchDog      (CPU_SCR &= ~CPU_SCR_PORS_MASK)
#define  M8C_ClearWDT            (RES_WDT = 0x00)
#define  M8C_ClearWDTAndSleep    (RES_WDT = 0x38)


//-----------------------------------------------
//  Sleep, CPU Stop & Software Reset
//-----------------------------------------------
#define  M8C_Sleep               (CPU_SCR |= CPU_SCR_SLEEP_MASK)
#define  M8C_Stop                (CPU_SCR |= CPU_SCR_STOP_MASK)

#define  M8C_Reset               asm("mov A, 0\nSSC");


//-----------------------------------------------
// ImageCraft Code Compressor Actions
//-----------------------------------------------

    // Suspend Code Compressor
    // Must not span a RET or RETI instruction
    // without resuming code compression

#define Suspend_CodeCompressor  asm("or F, 0")

    // Resume Code Compressor
#define Resume_CodeCompressor   asm("add SP,0")

#endif
