;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  M8SSC.INC -- M8C21x3x Microcontroller Device SSC Declarations
;;;
;;;  Copyright (c) 2004, Cypress MicroSystems, Inc. All rights reserved.
;;;
;;;
;;;  This file provides address constants, bit field masks and a set of macro
;;;  facilities for the Cypress MicroSystems 24x9x Microcontroller family.
;;;
;;;  Last Modified: April 20, 2005
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;----------------------------------
; Common Supervisory Code Variables
;----------------------------------
bSSC_KEY1:                          equ      F8h   ; supervisory key
bSSC_KEYSP:                         equ      F9h   ; supervisory stack ptr key
bSSC_TABLE_TableId:                 equ      FAh   ; table ID

OPER_KEY:                           equ      3Ah   ; operation key

;----------------------------------
; SSC_Action macro command codes
;----------------------------------
FLASH_READ:                         equ      1     ; flash read command
FLASH_WRITE:                        equ      2     ; flash write command
FLASH_ERASE:                        equ      3     ; flash erase command
PROTECT_BLOCK:                      equ      4     ; flash protect block command
TABLE_READ:                         equ      6     ; table read command
FLASH_CHECKSUM:                     equ      7     ; flash checksum calculation command
CALIBRATE0:                         equ      8     ; Calibrate without checksum
CALIBRATE1:                         equ      9     ; Calibrate with checksum

;----------------------------------
; SSC_Action Flash table addresses
;----------------------------------
; Table 0 Values
SILICON_ID_1:                       equ      F8h   ; Table 0 first byte of silicon ID
SILICON_ID_0:                       equ      F9h   ; Table 0 second byte of silicon ID

; Table 1 Values
SSCTBL1_TRIM_BGR_3V:                equ      F8h   ; 3.3V bandgap ref voltage trim
SSCTBL1_TRIM_IMO_3V_24MHZ:          equ      F9h   ; 3.3V internal main oscillator trim (24MHz)
SSCTBL1_CAL_ROOM_3V:                equ      FAh   ; 3.3V Room Temp Calibration
SSCTBL1_CAL_HOT_3V:                 equ      FBh   ; 3.3V Hot  Temp Calibration
SSCTBL1_TRIM_BGR_5V:                equ      FCh   ; 5.0V bandgap ref voltage trim
SSCTBL1_TRIM_IMO_5V_24MHZ:          equ      FDh   ; 5.0V internal main oscillator trim (24MHz)
SSCTBL1_CAL_ROOM_5V:                equ      FEh   ; 5.0V Room Temp Calibration
SSCTBL1_CAL_HOT_5V:                 equ      FFh   ; 5.0V Hot  Temp Calibration
    ; legacy names:
    VOLTAGE_TRIM_3V:                equ      F8h   ; Table 1 3.3V bandgap ref voltage trim value
    OSCILLATOR_TRIM_3V:             equ      F9h   ; Table 1 3.3V internal main oscillator trim value
    VOLTAGE_TRIM_5V:                equ      FCh   ; Table 1 5.0V bandgap ref voltage trim value
    OSCILLATOR_TRIM_5V:             equ      FDh   ; Table 1 5.0V internal main oscillator trim value

; Table 2 Values
SSCTBL2_TRIM_BGR_2V:                equ      F8h   ; 2.7V bandgap ref voltage trim
SSCTBL2_TRIM_IMO_2V_12MHZ:          equ      F9h   ; 2.7V internal main oscillator trim (12MHz)
SSCTBL2_TRIM_IMO_GAIN_3V:           equ      FAh   ; 3.3V IMO Gain trim value
SSCTBL2_TRIM_IMO_GAIN_5V:           equ      FBh   ; 5.0V IMO Gain trim value
SSCTBL2_TRIM_IMO_3V_6MHZ:           equ      FCh   ; 3.3V IMO Trim for SLOWIMO 6MHz operation
SSCTBL2_TRIM_IMO_2V_6MHZ:           equ      FDh   ; 2.7V IMO Trim for SLOWIMO 6MHz operation
SSCTBL2_TRIM_IMO_5V_6MHZ:           equ      FEh   ; 5.0V IMO Trim for SLOWIMO 6MHz operation


;-----------------------------------------------------------------------------
;  MACRO SSC_Action( OpCode )
;
;  DESCRIPTION:
;     Performs supervisory operations defined in Supervisory ROM (SROM)
;     section of Technical Reference Manual and/or Datasheet.
;-----------------------------------------------------------------------------
;
;  ARGUMENTS:
;     BYTE  OpCode   - specified supervisory operation - defined operations
;                      are:  FLASH_WRITE, FLASH_ERASE, FLASH_READ, TABLE_READ,
;                            FLASH_CHECKSUM, PROTECT_BLOCK
;  RETURNS:
;     Nothing
;
;  SIDE EFFECTS:
;     The values of the A and X registers are modified
;
;  PROCEDURE:
;     1) specify a 3 byte stack frame.  Save in [KEYSP]
;     2) insert the flash Supervisory key in [KEY1]
;     3) store function code in A
;     4) call the supervisory code
;
    macro SSC_Action( OpCode )
;   !!! DO NOT CHANGE THIS CODE !!!
;       This sequence of opcodes provides a
;       signature for the debugger and ICE.
    mov   X, SP                            ; copy SP into X
    mov   A, X                             ; mov to A
    add   A, 3                             ; create 3 byte stack frame
    mov   [bSSC_KEYSP], A                  ; save stack frame for supervisory code
    mov   [bSSC_KEY1], OPER_KEY            ; load the code for supervisory operations
    mov   A, @OpCode                       ; load A with specific Flash operation
    SSC                                    ; SSC call the supervisory code
;   !!! DO NOT CHANGE THIS CODE !!!
    endm


;-----------------------------------------------------------------------------
;  MACRO M8SSC_SetTableTrims( Table, IMO_Trim, Volt_Trim )
;
;  DESCRIPTION:
;     Loads Bandgap Voltage and Internal Main Oscillator (IMO) Trim
;     registers with values from from the specified flash System Parameter
;     table entries. Note that both trim values must come from the same table.
;     Also, see M8SSC_Set2TableTrims below.
;-----------------------------------------------------------------------------
;
;  ARGUMENTS:
;     BYTE Table     - 0..3 for the desired system parameter table
;     BYTE Volt_Trim - Address (F8 - FF) of the voltage trim value desired
;     BYTE IMP_Trim  - Address (F8 - FF) of the IMO trim value desired
;
;  RETURNS:
;     Nothing
;
;  SIDE EFFECTS:
;     The values of the A and X registers are modified
;
    macro M8SSC_SetTableTrims( Table, IMO_Trim, Volt_Trim )
    mov   [bSSC_TABLE_TableId], @Table     ; Point to requested Flash Table
    SSC_Action TABLE_READ                  ; Perform a table read supervisor call
    M8C_SetBank1
    mov   A, [@IMO_Trim]
    mov   reg[IMO_TR], A                   ; Load the 3V trim oscillator setting
    mov   A, [@Volt_Trim]
    mov   reg[BDG_TR], A                   ; Load the bandgap trim setting for 3V
    M8C_SetBank0
    endm


;-----------------------------------------------------------------------------
;  MACRO M8SSC_Set2TableTrims( TableA, IMO_Trim, TableB, Volt_Trim )
;
;  DESCRIPTION:
;     Loads Bandgap Voltage and Internal Main Oscillator (IMO) Trim
;     registers with values from from the specified flash System Parameter
;     table entries. Each trim value comes from a different table.
;     Also, see M8SSC_SetTableTrims above.
;-----------------------------------------------------------------------------
;
;  ARGUMENTS:
;     BYTE TableA    - 0..3 for the parameter table holding the Volt Trim
;     BYTE Volt_Trim - Address (F8 - FF) of the voltage trim value desired
;     BYTE TableB    - 0..3 for the parameter table holding the IMO Trim
;     BYTE IMP_Trim  - Address (F8 - FF) of the IMO trim value desired
;
;  RETURNS:
;     Nothing
;
;  SIDE EFFECTS:
;     The values of the A and X registers are modified
;
    macro M8SSC_Set2TableTrims( TableA, IMO_Trim, TableB, Volt_Trim )
    mov   [bSSC_TABLE_TableId], @TableA    ; Point to Flash Table for IMO Trim
    SSC_Action TABLE_READ                  ; Copy table data to RAM F8-FF
    M8C_SetBank1                           ; (Note, preserved across next SSC!)
    mov   A, [@IMO_Trim]                   ; Set the main oscillator trim
    mov   reg[IMO_TR], A
    mov   [bSSC_TABLE_TableId], @TableB    ; Point to Flash Table for Volt Trim
    SSC_Action TABLE_READ                  ; Copy table data to RAM F8-FF
    mov   A, [@Volt_Trim]                  ; Set the bandgap voltage trim
    mov   reg[BDG_TR], A                   ; Load the bandgap trim setting for 3V
    M8C_SetBank0
    endm


;-----------------------------------------------------------------------------
;  MACRO M8SSC_SetTableVoltageTrim( Table, Volt_Trim )
;
;  DESCRIPTION:
;     Loads Bandgap Voltage Trim register with values from from the
;     specified flash System Parameter table entry.
;-----------------------------------------------------------------------------
;
;  ARGUMENTS:
;     BYTE Table     - 0..3 for the parameter table holding the Volt Trim
;     BYTE Volt_Trim - Address (F8 - FF) of the voltage trim value desired
;
;  RETURNS:
;     Nothing
;
;  SIDE EFFECTS:
;     The values of the A and X registers are modified
;
    macro M8SSC_SetTableVoltageTrim( Table, Volt_Trim )
    mov   [bSSC_TABLE_TableId], @Table     ; Point to Flash Table
    SSC_Action TABLE_READ                  ; Perform a table read supervisor call
    M8C_SetBank1
    mov   A, [@Volt_Trim]                  ; Get the bandgap trim seting
    mov   reg[BDG_TR], A                   ; Update the register value
    M8C_SetBank0
    endm


;-----------------------------------------------------------------------------
;  MACRO M8SSC_SetTableIMOTrim( Table, IMO_Trim )
;
;  DESCRIPTION:
;     Loads Internal Main Oscillator (IMO) Trim register with values from
;     from the specified flash System Parameter table entry.
;-----------------------------------------------------------------------------
;
;  ARGUMENTS:
;     BYTE Table     - 0..3 for the parameter table holding the IMO Trim
;     BYTE IMO_Trim  - Address (F8 - FF) of the voltage trim value desired
;
;  RETURNS:
;     Nothing
;
;  SIDE EFFECTS:
;     The values of the A and X registers are modified
;
    macro M8SSC_SetTableIMOTrim( Table, IMO_Trim )
    mov   [bSSC_TABLE_TableId], @Table ; Point to Flash Table 1
    SSC_Action TABLE_READ              ; Perform a table read supervisor call
    M8C_SetBank1
    mov   A, [@IMO_Trim]               ; Get the IMO trim seting
    mov   reg[IMO_TR], A               ; Update the register value
    M8C_SetBank0
    endm


;-----------------------------------------------------------------------------
;  MACRO M8SSC_SetTableIMOGainTrim( Table, IMO_Gain_Trim )
;
;  DESCRIPTION:
;     Loads Internal Main Oscillator (IMO) Gain Trim register with values from
;     from the specified flash System Parameter table entry.
;-----------------------------------------------------------------------------
;
;  ARGUMENTS:
;     BYTE Table     - 0..3 for the parameter table holding the IMO Trim
;     BYTE IMO_Trim  - Address (F8 - FF) of the voltage trim value desired
;
;  RETURNS:
;     Nothing
;
;  SIDE EFFECTS:
;     The values of the A and X registers are modified
;
    macro M8SSC_SetTableIMOGainTrim( Table, IMO_Gain_Trim )
    mov   [bSSC_TABLE_TableId], @Table ; Point to Flash Table 1
    SSC_Action TABLE_READ              ; Perform a table read supervisor call
    M8C_SetBank1
    mov   A, [@IMO_Gain_Trim]          ; Get the IMO Gain trim setting
    jz    .NoGainTrim                  ; if Gain trim is zero, then don't set it
    mov   reg[IMO_TR2], A              ; Update the register value
.NoGainTrim:
    M8C_SetBank0
    endm

; end of file m8ssc.inc
